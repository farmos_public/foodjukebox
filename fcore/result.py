#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from builtins import zip
from builtins import str
from builtins import object
from enum import IntEnum

from code import RetCode
from variable import Variable

class ProcResult:
    def __init__(self, retcode = RetCode.OK, proc = None, values = None, alerts = [], period = None):
        if proc:
            self.setresult(proc, retcode, values)
        else:
            self._retcode = retcode
            self._values = None
            self._outputs = {}
        self._alerts = alerts
        self._period = period

    def __repr__(self):
        return "{}({}, {}, {}, {})".format(self.__class__.__name__, str(self._retcode), str(self._values), str(self._outputs), str(self._alerts))

    def getretcode(self):
        return self._retcode

    def setretcode(self, retcode):
        self._retcode = retcode

    def getvalues(self):
        return self._values

    def setresult(self, proc, retcode, values):
        self._retcode = retcode
        self._values = values
        if retcode == RetCode.OK and "outputs" in proc:
            tmp = {}
            for item in zip(proc["outputs"], values):
                if isinstance(item[1], Variable):
                    tmp[item[0]] = item[1]
                else:
                    tmp[item[0]] = Variable(item[1])
            self._outputs = tmp
            print("##### proc _outputs", tmp)
        else:
            self._outputs = {}
     
    def getoutputs(self):
        return self._outputs

    def getalerts(self):
        return self._alerts

    def getperiod(self):
        return self._period

class AlertResult:
    def __init__(self, code, grade, info, fldid = None, hwid = None, devid = None, farmid = None):
        self._code = code
        self._grade = grade
        self._info = info
        self._fldid = fldid
        self._hwid = hwid
        self._devid = devid
        self._farmid = farmid

    def setfarmid(self, farmid):
        self._farmid = farmid

    def getwparams(self):
        return [self._farmid, self._code.value, self._grade.value, self._info, self._hwid, self._devid, self._fldid]

class RuleResult:
    def __init__(self, procrets, requests={}, data={}, alerts=[]):
        self._prets = procrets
        self._reqs = requests
        self._data = data
        self._alerts = alerts

    def __repr__(self):
        return "ProcResults {}, Requests {}, DataDic {}, Alerts {}".format(len(self._prets), len(self._reqs), len(self._data), len(self._alerts))

    def updaterequests(self, requests):
        self._reqs.update(requests)

    def getrequests(self):
        return self._reqs

    def getdatadic(self):
        return self._data

    def getalerts(self):
        return self._alerts

    def getprocresults(self):
        return self._prets

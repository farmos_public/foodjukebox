import tailer
import datetime

line = tailer.tail(open('/var/log/cpmng.log'), 1)
temp = line[0].split()

datestr = temp[1] + " " + temp[2]
print (datestr)
dateobj = datetime.datetime.strptime(datestr, '%Y-%m-%d %H:%M:%S,%f')

diff = datetime.datetime.now() - dateobj
if diff.total_seconds() > 120:
    print ("error", diff, dateobj)
    exit (1)
else:
    print ("no error", diff, dateobj)
    exit (0)

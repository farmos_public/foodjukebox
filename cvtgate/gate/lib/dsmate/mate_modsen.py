#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
import itertools
from enum import IntEnum
from threading import Thread, Lock
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.exceptions import ConnectionException

from .. import *

class ModSingleSensor(object):
    def __init__(self, connmng, devid, dk, logger):
        """ dk : (connidx, unit, addr, (a, b)) : a, b is for linear calibration """
        self._conn = connmng.getconn(dk[0])
        self._devid = devid
        self._params = dk[1:3] + [1]
        self._cali = dk[3]
        self._logger = logger
        self._logger.info("New Device " + str([devid, dk]))

    def getvalue(self):
        res = self._conn.readinfo(self._params)
        if res is None:
            return (StatCode.ERROR, None) 
        else:
            return (StatCode.READY, res[0] * self._cali[0] + self._cali[1])


class ModConn(object):
    def __init__(self, option, logger):
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._lock = Lock()
        self._logger = logger
        self._method = None
        self._conn = self.connect(option)

    def getmethod(self):
        return self._method

    def close(self):
        self._conn.close()

    def connect(self, opt):
        ret = False
        conn = None

        try:
            if opt['method'] == 'rtu':
                conn = ModbusSerialClient(method='rtu', port=opt['port'],
                        timeout=self._timeout, baudrate=opt['baudrate'])
                ret = conn.connect()
                msg = "failed to connect with rtu"
                code = NotiCode.RTU_CONNECTED if ret else NotiCode.RTU_FAILED_CONNECTION
                self._method = opt['method']
            elif opt['method'] == 'tcp':
                conn =  ModbusTcpClient(opt['host'], port=opt['port'], timeout=self._timeout)
                ret = conn.connect()
                msg = "failed to connect with tcp"
                code = NotiCode.TCP_CONNECTED if ret else NotiCode.RTU_FAILED_CONNECTION
                self._method = opt['method']
            else:
                msg = "It's a wrong connection method. " + str(opt['method'])
        except Exception as ex:
            self._logger.warn("Fail to connect MODBUS [" + str(opt) + "] : " + str(ex))
            return None

        if ret == False:
            self._logger.warn(msg)

        return conn

    def readinfo(self, params):
        """ params : (unit, addr, length) """
        print ("readinfo", params)
        try:
            res = self._conn.read_holding_registers(params[1], params[2], unit=params[0])
        except Exception as ex:
            self._logger.warn("fail to read holding registers. : " + str(ex))
            return None

        if res is None or res.isError():
            self._logger.warn("Fail to read register." + str(params) + ":" + str(res))
            return None

        if len(res.registers) != params[2]:
            self._logger.warn("Number of read registers is not matched." + str(res.registers))
            return None

        print ("registers : ", res.registers)
        return res.registers

    def close(self):
        self._conn.close()

class ModConnManager:
    def __init__(self, option, logger):
        self._conns = []
        self._logger = logger
        self._option = option

        for copt in self._option["conn"]:
            self._conns.append(ModConn(copt, logger))

    def getconn(self, idx):
        return self._conns[idx]

    def close(self):
        for conn in self._conns:
            conn.close()

class MODSenMate(DSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(MODSenMate, self).__init__(option, devinfo, coupleid, quelist)
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._connmng = None
        self._devices = {}  # connections by node ids - for performance
        self._logger.info("MODSenMate Started.")

    def connect(self):
        self._connmng = ModConnManager(self._option, self._logger) 
        self._logger.info("Mate connected. ")

        self._devices = {}  # connections by node ids - for performance
        gw = self._devinfo.getgw()
        for nd in gw["children"]:
            self._devices[nd["id"]] = {}
            for dev in nd["children"]:
                self._devices[nd["id"]][dev["id"]] = ModSingleSensor(self._connmng, dev["id"], json.loads(dev["dk"]), self._logger)

        print("connect", self._devices)
        super(MODSenMate, self).connect()

    def close(self):
        self._connmng.close()
        super(MODSenMate, self).close()

    def process(self):
        self.processobservations()

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE):
            for nid, devs in self._devices.items():
                obs = Observation(nid)

                for did, dev in devs.items():
                    val = dev.getvalue()
                    obs.setobservation(did, val[1], val[0])

                self._writeobs(obs)
            self._updatetime(Mate.OBSTYPE)

if __name__ == "__main__":
    from multiprocessing import Queue

    quelist = [Queue(), Queue(), Queue(), Queue()]

    opt = {
        'conn' : [{
            'method': 'rtu',
            'name' : 'USB0',
            'port' : '/dev/ttyUSB1',
            'baudrate' : 19200,
            'timeout': 5
        }]
    }
    
    devinfo = [
        {"id" : "1", "dk" : "", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "", "dt": "nd", "children" : [
                {"id" : "3", "dk" : "[0, 1, 40002, [0.02, -273.15]]", "dt": "sen"},
                {"id" : "4", "dk" : "[0, 1, 40002, [0.02, -273.15]]", "dt": "sen"}
            ]}
        ]}
    ]

    msmate = MODSenMate(opt, devinfo, "1", quelist)
    ssmate = SSMate ({}, [], "1", quelist)

    ssmate.start ()
    msmate.start ()

    print("mate started. waiting 10 seconds")
    time.sleep(10)

    for q in quelist[1:]:
        q.close()
        q.join_thread()
    msmate.stop()
    ssmate.stop()

    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()



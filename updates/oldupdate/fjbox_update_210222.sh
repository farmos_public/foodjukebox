#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

echo -e '\n\n 1. Get Serial Number'

cd $(find /home -type d -name "fjbox")


statustxt=$(awk '/FJDevice Serial/' status.txt)

fjboxserial=${statustxt:18}

echo -e $fjboxserial

echo -e '\n\n 2. Update & Cvtgate Git pull'

echo -e '\n\n Update Start'
#pip3 install scikit-learn
pip3 install --upgrade numpy
#pip3 install --upgrade psutil

echo -e '\n\n Update Complete'

cd /home/pi/farmosv2-script/cvtgate; git pull origin master

echo -e '\n\n 3. Set Up Crontab \n'

(crontab -l 2>/dev/null | sed 's/python fcore.py/python3 fcore.py/' ) | crontab -
cat <(crontab -l) <(echo '* * * * * cd /home/pi/farmosv2-script/cvtgate/util; python3 fmon.py') | crontab -

crontab -l

echo -e '\n\n 4. Set Up DataBase \n'

mysql -u farmos -pfarmosv2@ farmos -e "update farm set name = '푸드 쥬크박스 $fjboxserial'";
mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) values(51, NULL, 'CPU 1분부하', '%', 0, NULL, 0, 0),(52, NULL, 'CPU 5분부하', '%', 0, NULL, 0, 0),(53, NULL, 'CPU 15분부하', '%', 0, NULL, 0, 0),(54, NULL, '메모리사용율', '%', 0, NULL, 0, 0),(55, NULL, '스왑사용율', '%', 0, NULL, 0, 0),(56, NULL, '디스크사용율', '%', 0, NULL, 0, 0),(57, NULL, '디스크읽기', 'MB', 0, NULL, 0, 0),(58, NULL, '디스크쓰기', 'MB', 0, NULL, 0, 0),(59, NULL, '네트웍전송', 'MB', 0, NULL, 0, 0),(60, NULL, '네트웍수신', 'MB', 0, NULL, 0, 0)";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) values(51, now(), 0, now(), NULL),(52, now(), 0, now(), NULL),(53, now(), 0, now(), NULL),(54, now(), 0, now(), NULL),(55, now(), 0, now(), NULL),(56, now(), 0, now(), NULL),(57, now(), 0, now(), NULL),(58, now(), 0, now(), NULL),(59, now(), 0, now(), NULL),(60, now(), 0, now(), NULL)";


echo -e '\n\n 5. Set Up MonitFcore \n'

sed -i "s/cpmng/fcore/g" /etc/monit/conf.d/monitfcore

'use strict';

var express = require('express');
var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  app.use(express.static('public'));
  // install middleware
  swaggerExpress.register(app);

  var port = swaggerExpress.runner.swagger.host.split(":")[1] || 8880;
  app.listen(port);
});

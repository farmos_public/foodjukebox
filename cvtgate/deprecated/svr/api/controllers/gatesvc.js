/**
 * @fileoverview gatesvc api for cvtgate
 * @author joonyong.jinong@gmail.com
 * @version 1.0.0
 * @since 2019.01.30
 */

/*jshint esversion: 6 */

/**
 * notice
 * 코드는 일단 다른거 복사만 해서 가져다 둔거임.
 **/

var mysql = require('mysql');
var uuid = require('uuid/v4');

var gatesvcapi = function () {
    "use strict";

    var _initialized = false;
    var _pool = null;
    var _session = "";
    var _config = {
        "host" : "localhost",
        "user" : "farmos",
        "password" : "farmosv2@",
        "database" : "cvtgate",
        "timezone" : "Asia/Seoul"
    };

    /**
     * @method initialize
     * @description 모듈을 초기화 한다.
     */
    var initialize = function () {
        return new Promise (function (resolve, reject){
            if (_initialized) {
                resolve();
            } else {
                _pool = mysql.createPool(_config);
                _initialized = true;
                resolve();
            }
        });
    };

    /**
     * @method finalize
     * @description 모듈사용을 종료한다.
     */
    var finalize = function () {
        _pool.end (function (err) {
            if (err)
                console.log ("error : " + err);
            else
                _initialized = false;
        });
    };

    /**
     * @method _execute
     * @param {String} query - database query
     * @param {Object} param - parameter for query
     * @return Promise 성공시 true
     * @description query와 param을 받아서 실행한다.
     */
    var _execute = function (query, param) {
        console.log("_execute", query, param);
        return new Promise(function (resolve, reject) {
            _pool.getConnection(function(err, conn) {
                if (err) {
                    console.log ("fail to get connection", err);
                    reject(new Error("fail to get db connection"));

                } else {
                    conn.query (query, param, function (error, results, fields) {
                        if (error) {
                            console.log ("error : " + query);
                            console.log (error);
                            conn.release();
                            reject(new Error("fail to execute query : " + query));
                        } else {
                            conn.release();
                            resolve (true);
                        }
                    });
                }
            });
        });
    };

    /**
     * @method _executeandresult
     * @param {String} query - database query
     * @param {Object} param - parameter for query
     * @return Promise 성공시 조회결과
     * @description query와 param을 받아서 실행한다.
     */
    var _executeandresult = function (query, param) {
        console.log("_execute&result", query, param);
        return new Promise(function (resolve, reject) {
            _pool.getConnection(function(err, conn) {
                if (err) {
                    console.log ("fail to get connection", err);
                    reject(new Error("fail to get db connection"));

                } else {
                    conn.query(query, param, function (error, results, fields) {
                        if (error) {
                            console.log ("error : " + query);
                            console.log (error);
                            conn.release();
                            reject(new Error("fail to execute query : " + query));
                        } else {
                            console.log (results);
                            conn.release();
                            resolve(results);
                        }
                    });
                }
            });
        });
    };

    var newmate = function (req, res) {
        var params = req.swagger.params;

        if (('body' in params) && (params.body.value)) {
            var newid = uuid();
            var mate = params.body.value;
            mate.id = newid;

            initialize()
            .then(function () {
                return _execute("insert into mate(id, side, config) values (?, ?, ?)", 
                                [mate.id, mate.side, JSON.stringify(mate)]);
            })
            .then(function(ret) {
                res.json();
            })
            .catch (function(err) {
                console.log ("new mate error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no body.");
        }
    };

    var delmate = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value)) {
            initialize()
            .then(function () {
            	return _execute("update mate set deleted = true, updated = now() where id = ?", [params.id.value]);
            })
            .then(function(ret) {
                res.json();
            })
            .catch (function(err) {
                console.log ("delete mate error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var setmate = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value) &&
            ('body' in params) && (params.body.value)) {
            var mate = params.body.value;
            if (mate.id == params.id.value) {
                initialize()
                .then(function () {
                	return _execute("update mate set side = ?, config = ?, updated = now() where id = ?", 
                                        [mate.side, JSON.stringify(mate), mate.id]);
                })
                .then(function(ret) {
                    res.json();
                })
                .catch (function(err) {
                    console.log ("update mate error : " + err);
                    res.status(405).send(err);
                });
            } else {
                res.status(400).send("Mate IDs are not matched.");
            }
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var getmates = function (req, res) {
        initialize()
        .then(function () {
            return _executeandresult("select * from mate where deleted = false order by side", []);
        })
        .then(function (data) {
            var mates = {"ds": [], "ss": []};
            data.map(function(one) {
                mates[one.side].push(JSON.parse(one.config));
            });
            console.log(mates);
            res.json(mates);
        })
        .catch(function (err) {
            res.status(405).send(err);
        });
    };

    var _setfjboxcouple = function (gateid, target) {
        return new Promise (function (resolve, reject){
            _executeandresult("select name, config from couple where gateid = 'default' and deleted = false and id = ?", [target])
            .then(function (data) {
                var couple = data[0];
                var config = JSON.parse(couple.config);
                var coupleid = uuid();

                couple["id"] = coupleid;
                config["id"] = coupleid;
                config["ssmate"]["opt"]["mqtt"]["id"] = coupleid;

                couple.config = JSON.stringify(config);
                return couple;
            })
            .then(function (couple) {
                return _execute("insert into couple(id, gateid, name, config) values (?, ?, ?, ?)", [couple["id"], gateid, couple["name"], couple["config"]]);
            })
            .then(function (ret) {
                resolve(target);
            })
            .catch (function(err) {
                console.log("set fjbox couples error : " + err);
                reject(new Error("fail to set fjbox couples"));
            });
        });
    };

    var newfjbox = function (req, res) {
        var params = req.swagger.params;
        var name;

        console.log ("new fj box")
        if (('body' in params) && (params.body.value)) {
            var newid = params.body.value.id;
            name = params.body.value.name;

            console.log ("new fj box " + newid + " " + name);
            initialize()
            .then(function () {
                return _execute("insert into gate(id, name) values (?, ?)", [newid, name]);
            })
            .then(function(ret) {
                return _setfjboxcouple(newid, 'kdboard');
            })
            .then(function(ret) {
                return _setfjboxcouple(newid, 'rpicam');
            })
            .then(function(ret) {
                return _getcouples(newid);
            })
            .then(function (couples) {
                res.json({
                    "id": newid,
                    "name" : name,
                    "children" : couples
                });
            })
            .catch (function(err) {
                console.log ("new gate error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no body.");
        }
    };

    var newgate = function (req, res) {
        var params = req.swagger.params;
        var name;

        if (('body' in params) && (params.body.value)) {
            var newid = uuid();
            name = params.body.value.name;

            initialize()
            .then(function () {
                return _execute("insert into gate(id, name) values (?, ?)", [newid, name]);
            })
            .then(function(ret) {
                res.json({
                    "id": newid,
                    "name" : name,
                    "children" : []
                });
            })
            .catch (function(err) {
                console.log ("new gate error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no body.");
        }
    };

    var delgate = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value)) {
            initialize()
            .then(function () {
                return _execute("update gate set deleted = true, updated = now() where id = ?", [params.id.value]);
 	    })
            .then(function(ret) {
                res.json();
            })
            .catch (function(err) {
                console.log ("delete mate error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var setgate = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value) && 
            ('body' in params) && (params.body.value)) {
            var gate = params.body.value;

            if (gate.id == params.id.value) {
                initialize()
                .then(function () {
                    return _execute("update gate set name = ?, updated = now() where id = ?", [gate.name, gate.id]);
                })
                .then(function(ret) {
                    res.json(gate);
                })
                .catch (function(err) {
                    console.log ("delete mate error : " + err);
                    res.status(405).send(err);
                });
            } else {
                res.status(400).send("Gate IDs are not matched.");
            }
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var newcouple = function (req, res) {
        var params = req.swagger.params;

        if (('gateid' in params) && (params.gateid.value) && 
            ('body' in params) && (params.body.value)) {
            var couple = params.body.value;
            var id = couple.id;
            var name = couple.name;

            initialize()
            .then(function () {
                return _execute("insert into couple(id, gateid, name, config) values (?, ?, ?, ?)", 
                                [id, params.gateid.value, name, JSON.stringify(couple)]);
            })
            .then(function(ret) {
                res.json(couple);
            })
            .catch (function(err) {
                console.log ("new couple error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no body.");
        }
    };

    var delcouple = function (req, res) {
        var params = req.swagger.params;

        if (('gateid' in params) && (params.gateid.value) && 
            ('id' in params) && (params.id.value)) {

            initialize()
            .then(function () {
                return _execute("update couple set deleted = true, updated = now() where gateid = ? and id = ?", 
                                [params.gateid.value, params.id.value]);
 	    })
            .then(function(ret) {
                res.json();
            })
            .catch (function(err) {
                console.log ("delete couple error : " + err);
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var setcouple = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value) && 
            ('gateid' in params) && (params.gateid.value) && 
            ('body' in params) && (params.body.value)) {
            var couple = params.body.value;

            if (couple.id == params.id.value) {
                initialize()
                .then(function () {
                    return _execute("update couple set config = ?, name = ?, updated = now() where id = ? and gateid = ?", 
                                   [JSON.stringify(couple), couple.name, couple.id, params.gateid.value]);
                })
                .then(function(ret) {
                    res.json(couple);
                })
                .catch (function(err) {
                    console.log ("update couple error : " + err);
                    res.status(405).send(err);
                });
            } else {
                res.status(400).send("Couple IDs are not matched.");
            }
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var setcouplechildren = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value) && 
            ('gateid' in params) && (params.gateid.value) && 
            ('body' in params) && (params.body.value)) {

            var couple = params.body.value;
            var config = null;
            var gateid = params.gateid.value;
            var id = params.id.value;

            initialize()
            .then(function () {
                return _executeandresult("select config from couple where gateid = ? and id = ? and deleted = false", [gateid, id])
            })
            .then(function (data) {
        console.log (data);
                config = JSON.parse(data[0].config);
        console.log (couple);
                config.children = couple;
        console.log (config);
                return _execute("update couple set config = ?, updated = now() where id = ? and gateid = ?", 
                               [JSON.stringify(config), id, gateid]);
            })
            .then(function(ret) {
        console.log (config);
                res.json(config);
            })
            .catch (function(err) {
                console.log ("update children of couple error : " + err);
                res.status(405).send(err);
            });
        } else {
        console.log ("5");
            res.status(400).send("There is no id.");
        }
    };

    var _getcouples = function (gateid) {
        return new Promise (function (resolve, reject){
            _executeandresult("select * from couple where gateid = ? and deleted = false", [gateid])
            .then(function (data) {
                var couples = data.map(function(one) {
                    console.log(JSON.parse(one.config));
                    return JSON.parse(one.config);
                });
                console.log(couples);
                resolve(couples);
            })
            .catch (function(err) {
                console.log("get couples error : " + err);
                reject(new Error("fail to get couples"));
            });
        });
    };

    var getcouples = function (req, res) {
        var params = req.swagger.params;

        if (('gateid' in params) && (params.gateid.value)) {
            initialize()
            .then(function () {
                return _getcouples(params.gateid.value);
            })
            .then(function (couples) {
                res.json(couples);
            })
            .catch(function (err) {
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var _getcouple = function (gateid,coupleid) {
        return new Promise (function (resolve, reject){
            _executeandresult("select * from couple where gateid = ? and id = ? and deleted = false", [gateid,coupleid])
            .then(function (data) {
                var couple = data.map(function(one) {
                    console.log(JSON.parse(one.config));
                    return JSON.parse(one.config);
                });
                console.log(couple);
                resolve(couple);
            })
            .catch (function(err) {
                console.log("get couple error : " + err);
                reject(new Error("fail to get couple"));
            });
        });
    };

    var getcouple = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value) && 
            ('gateid' in params) && (params.gateid.value)) {
            initialize()
            .then(function () {
                return _getcouple(params.gateid.value,params.id.value);
            })
            .then(function (couple) {
                if(couple.length > 0) {
                    res.json(couple[0]);
                } else {
                    res.json(couple);
                }
            })
            .catch(function (err) {
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no id.");
        }
    };   

    var getgate = function (req, res) {
        var params = req.swagger.params;

        if (('id' in params) && (params.id.value)) {
            var gate = {};
            initialize()
            .then(function () {
                return _executeandresult("select id, name from gate where deleted = false and id = ?", [params.id.value]);
            })
            .then(function (data) {
                gate = data[0];
            })
            .then(function () {
                return _getcouples(params.id.value);
            })
            .then(function (couples) {
                gate.children = couples;
                res.json(gate);
            })
            .catch(function (err) {
                res.status(405).send(err);
            });
        } else {
            res.status(400).send("There is no id.");
        }
    };

    var getgates = function (req, res) {
        initialize()
        .then(function () {
            return _executeandresult("select id, name from gate where deleted = false", []);
        })
        .then(function (data) {
            //res.setHeader('Content-Type', 'application/json');
            console.log(data);
            res.json(data);
        })
        .catch(function (err) {
            res.status(405).send(err);
        });
    };

    var login = function (req, res) {
        var ip = (req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress).split(",")[0];
        console.log ("login ip : " + ip);

        var params = req.swagger.params;

        if (('body' in params) && (params.body.value)) {
            initialize()
            .then(function () {
                var info = params.body.value;
                if (info.id == 'admin' && info.password == '1234%') {
                   _session = uuid();
                   res.json({"session": _session});
                } else {
                   res.status(405).send("id or password are not matched");
                }
            })
            .catch(function (err) {
                res.status(405).send(err);
            });
        } else {
            res.status(405).send("no parameter");
        }
    };

    var logout = function (req, res) {
        var params = req.swagger.params;

        if (('body' in params) && (params.body.value)) {
            if (params.body.value.session == _session) {
                _session = "";
                res.json();
            } else {
                res.status(405).send("fail to logout");
            }
        } else {
            res.status(405).send("no parameter");
        }
    };

    return {
        login: login,
        logout: logout,
        delmate: delmate,
        newmate: newmate,
        setmate: setmate,
        getmates: getmates,
        newfjbox: newfjbox,
        newgate: newgate,
        delgate: delgate,
        getgates: getgates,
        getgate: getgate,
        setgate: setgate,
        newcouple: newcouple,
        delcouple: delcouple,
        getcouples: getcouples,
        setcouple: setcouple,
        setcouplechildren: setcouplechildren,
        getcouple: getcouple
    };
};

module.exports = gatesvcapi();

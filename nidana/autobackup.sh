#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

echo -e $SHELL_PATH

cd $SHELL_PATH

echo $1

startday=$(mysql -ufarmos -pfarmosv2@ farmos -N -s -e "select date_format(obs_time, '%Y-%m-%d') from observations where obs_time like '2022-05%' order by obs_time limit 1;")
endday=$(mysql -ufarmos -pfarmosv2@ farmos -N -s -e  "select date_format(obs_time, '%Y-%m-%d') from observations where obs_time like '2022-05%' order by obs_time desc limit 1;")

echo -e $startday, $endday

START=$(date +"%Y-%m-%d" -d $startday)

END=$(date +"%Y-%m-%d" -d "$endday 1 day")

CURRENT="$START"

echo -e `date -d "$CURRENT 1 day" +"%Y-%m-%d"`


while [ "$CURRENT" != "$END" ]; do
    echo -e $CURRENT
    python3 boxmng.py $CURRENT
    CURRENT=$(date -d "$CURRENT 1 day" +"%Y-%m-%d")
done



# FoodJukeBox Update

푸드쥬크박스 업데이트와 관련된 내용들을 정리한다.

## 업데이트 자동화

푸드 쥬크박스에 주기적으로 업데이트할 필요가 있다. 업데이트는 서버별로 특정 주기에 따라 자동으로 업데이트를 진행하는데, 이는 업데이트 자동화 프로그램으로 autoupdate.sh 이다. 업데이트 날짜에 따라 fjbox_update_000000 식으로 업데이트 프로그램이 업로드된다. 그럼 가장 최근에 업데이트를 실행한

## 슬랙
```
오후 6:36
fjbox72 : fjbox_update_201223.sh success
6:36
fjbox72 : fjbox_update_201228.sh success
6:36
178a7f1..75aed1e  master     -> origin/master
fjbox72 : fjbox_update_210122.sh fail
```

업데이트 프로그램이 실행되면 푸드 쥬크박스 이름과 업데이트 프로그램 이름, 성공 여부에 대해서 슬랙으로 발송됩니다. 실패 시에는 실패 로그와 함께

#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc. All right reserved.
#
# SEWOON API
#


from collections import defaultdict
import requests
import json
import struct
import time
import traceback
from datetime import datetime
from decimal import *
import pymysql
from lxml import etree
from io import StringIO
import requests
import re
from .. import *

class ISUETRIMate(DSMate):
    _MAXOPID = 65535   
    """
    "userId":"farmos","password":"farmos","_csrf":csrf
    option = {
        "loginkey" : {
            "zone_id" : ["1","2","3"],
            "user_id" : "farmos",
            "password" : "farmos"
        }
    }
    """
    def __init__(self, option, devinfo, coupleid, quelist):
        super(ISUETRIMate, self).__init__(option, devinfo, coupleid, quelist)
        self._timeout = 5 if "timeout" not in option else option["timeout"]
        copt = self._option["loginkey"]
        self._s = self.login(copt)
        self.pastdata = {}
        for zone in self._option["loginkey"]['zone_id']:
            self.pastdata[zone] = self.loaddata(zone, self._s)


    def loaddata(self, z, s):
        copt = self._option["loginkey"]
        #r = s.get("http://cfarm.iptime.org:9090/environment/summary/data?controller_id=100&zone_id=1")
        url = "http://cfarm.iptime.org:9090/environment/summary/data?controller_id=100&zone_id={}".format(z)
        r = s.get(url)
        data = json.loads(r.text) if r.status_code == 200 else self._logger.error("error loaddata" + r.status_code)
        return data

    def login(self, opt):
        id = opt["user_id"]
        pw = opt["password"]
        parser = etree.HTMLParser()
        s = requests.Session()
        #fe54cc8d-f247-4c97-be21-7586ba13f303
        r = s.get('http://cfarm.iptime.org:9090')
        html = r.content.decode("utf-8")
        tree = etree.parse(StringIO(html), parser=parser)
        csrf = tree.xpath('/html/head/meta[@name="_csrf"]/@content')
        #print (csrf)

        info = {"userId":id,"password":pw,"_csrf":csrf}
        r = s.post('http://cfarm.iptime.org:9090/postlogin',info)
        print (r, "login")
        return s

    def process(self):
        for gw in self._devinfo:
            nd = gw["children"][0]
            data = self.loaddata(gw["dk"], self._s)
            self.senddatafornode(data, nd, gw)
            self.pastdata[gw["dk"]] = data


    def senddatafornode(self, data, nd, gw):
        obsblk = Observation(nd["id"])
        notiblk = Notice(nd["id"], NotiCode.ACTUATOR_STATUS)
        for dev in nd["children"]:
            if DevType.issensor(dev["dt"]):
                obsblk.setobservation(dev["id"], self.getvalue(dev["id"], self.getsensorinfo(dev, data)), StatCode.READY)
            elif DevType.isswitch(dev["dt"]):
                notiblk.setcontent(dev["id"], {"opid" : 0, "status" : self.getswitchinfo(dev, data)})
            elif DevType.isretractable(dev["dt"]):               
                (stat , value) = self.getretractableinfo(dev, data, gw)
                notiblk.setcontent(dev["id"], {"opid" : 0, "status" : stat, "position" : value})
        self._writeobs(obsblk)
        self._writenoti(notiblk)
    
    def getdata(self, dev, data):
        for row in data["data"]["list"]:
            for ro in row["list"]:
                for r in ro["list"]:
                    if dev["dk"] == "{}-{}".format(r["controller_node_id"], r["device_id"]):
                        v = r['value']
                        return v
                    
    def getsensorinfo(self, dev, data):
        if self.getdata(dev, data) == "NO":
            v = 0
        elif self.getdata(dev, data) == "YES":
            v = 1
        elif "도" in self.getdata(dev, data):
            v = float(re.sub(r'[^0-9]', '', self.getdata(dev, data)))
        elif self.getdata(dev, data) == "":
            v = None
        else:
            v = float(self.getdata(dev, data))
        return v

    def getswitchinfo(self, dev, data):
        if self.getdata(dev, data) == "ON":
            return StatCode.WORKING.value
        elif self.getdata(dev, data) == "OFF":
            return StatCode.READY.value
        else:
            self._logger.error("check switch value " + dev["dk"])
        
    def getretractableinfo(self, dev, data, gw):
        v = self.getdata(dev, data)
        past_v = self.getdata(dev, self.pastdata[gw["dk"]])
        if v > past_v:
            return StatCode.OPENING.value, v
        elif v < past_v:
            return StatCode.CLOSING.value, v
        elif v == past_v:
            return StatCode.READY.value, v


if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "zone_id" : "1",
        "user_id" : "farmos",
        "password" : "farmos"
    }

    devinfo = [
        {"id": "1", "dk": "1", "dt": "gw", "children": [{
            "id": "2", "dk": "1", "dt": "nd", "children": [
                {"id": "3", "dk": "XINTEMP", "dt": "sen"},
                {"id": "4", "dk": "XINTEMP1", "dt": "sen"},
                {"id": "5", "dk": "XINTEMP2", "dt": "sen"},
                {"id": "6", "dk": "XINTEMP3", "dt": "sen"},
                {"id": "7", "dk": "XINHUM", "dt": "sen"},
                {"id": "8", "dk": "XINHUM1", "dt": "sen"},
                {"id": "9", "dk": "XINHUM2", "dt": "sen"},
                {"id": "10", "dk": "XSKYVOL1,XSKYAUTO", "dt": "act/retractable/level0"}
            ]
        }]
        },
        {"id": "11", "dk": "2", "dt": "gw", "children": [{
            "id": "12", "dk": "2", "dt": "nd", "children": [
                {"id": "13", "dk": "XINTEMP", "dt": "sen"},
                {"id": "14", "dk": "XINTEMP1", "dt": "sen"},
                {"id": "15", "dk": "XINTEMP2", "dt": "sen"},
                {"id": "16", "dk": "XINTEMP3", "dt": "sen"},
                {"id": "17", "dk": "XINHUM", "dt": "sen"},
                {"id": "18", "dk": "XINHUM1", "dt": "sen"},
                {"id": "19", "dk": "XINHUM2", "dt": "sen"},
                {"id": "20", "dk": "XSKYVOL2,XSKYAUTO", "dt": "act/retractable/level0"}
            ]
        }]
        }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = ISUETRIMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

import traceback
from lib.mate import Logger
import logging
import time
import struct
from pymodbus.exceptions import ConnectionException, ModbusException
from pymodbus.other_message import ReadExceptionStatusRequest
from pymodbus.pdu import ExceptionResponse, ModbusResponse

from pymodbus.register_read_message import ReadHoldingRegistersResponse

from pymodbus.client.sync import ModbusSerialClient, ModbusTcpClient, BaseModbusClient
from pymodbus.constants import Endian

from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder

from pymodbus.transaction import (ModbusRtuFramer,
                                  ModbusAsciiFramer,
                                  ModbusBinaryFramer,
                                  ModbusSocketFramer)

from lib.mblock import CmdCode, ResCode
from .address import StandardAddress

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

INITIAL_CLIENT = {
    "status": "Not Connected",
    "buffer": {},
    "builder_infos": {}
}

UNIT = 0x01

def setlogger (log):
    global logger
    logger = log
    


def connect(host="127.0.0.1", port=2088, method="rtu", **kwargs):
    """
    양액기와 Modbus를 통하여 통신할 수 있도록 연결을 시도한다.
    인자값으로 host, port, method 등을 받는다. 
    
    Args:
        host : 접속할 주소, RTU의 경우 host는 사용하지 않음
        port (string|int): example): 예를 들면, rtu의 경우 '/tmp/ptype0', tcp의 경우, 2333
        baudrate (int): optional
        stopbits (int): 1로 기본적으로 설정
        bytesize (int): 8 로 기본적으로 설정
        timeout (int): 3으로 절정

    Return:
    연결이 성공했을 때: MobusSerialClient|ModbusTCPClient|None
    """

    client = None
    logger.debug("connect to modbus " + method + "/" + host +":"+ str(port))
    if method == "serial":
        client = ModbusSerialClient(
            method='rtu', port=port, framer=ModbusRtuFramer, **kwargs)
    elif method == "tcp":
        client = ModbusTcpClient(host=host, port=port,
                                 framer=ModbusSocketFramer, **kwargs)

    try:
        client.connect()
    except Exception as e:
        logger.error("Connection Error")
        logger.error(traceback.format_exc())


    return client

def reconnect(client):
    """
    재접속 시도

    Args:
        client (ModbusClient): 재접속을 수행한다. 

    Returns:
         : 
    """
    if client is None:
        return None
    
    try:
        client.close()
        client.connect()
    except Exception as e:
        logger.error("Connection Error")
        logger.error(e)
        logger.error(traceback.format_exc())


    return client


def is_open(conn: BaseModbusClient) -> bool:
    """
    connector가 연결되어 있는지 확인합니다.

    Args:
        conn (BaseModbusClient): 연결 객체입니다.

    Returns:
        bool: 연결 여부입니다.
    """    
    return conn is not None and conn.is_socket_open()


def disconnect(conn: BaseModbusClient):
    """
    양액기의 연결을 닫습니다.

    Args: conn : 연결 정보
    """
    try:
        conn.close()
    except:
        logger.error("Closing Error")
        logger.error(traceback.format_exc())


def read(client: BaseModbusClient, unit_id, address, size):
    """
    Register, 주소값 그리고 크기로부터 레지스러를 읽어옵니다.

    Args:
        client_info (client_info): 클라이언트 객체 
        address (int): 주소, string이라면 integer로 변환된다.
        size (int): 읽어들일 메모리 크기

    Returns:
        any: 읽은 값의 레지스터 결과
    """
    try:
        if type(address) is str and address.isdigit():
            address = int(address)
    
        response: ReadHoldingRegistersResponse = client.read_holding_registers(
            address, size, unit=unit_id)
        
        if response.isError():
            logger.error("Error code : " + str(response.function_code))
            
        return response.registers
    except Exception as e:
        logger.error(e)
        logger.error(traceback.format_exc())
        logger.error("read fail.")
        logger.error("address :  " + str(address))
        logger.error("unit_id :  " + str(unit_id))
        logger.error("size :  " + str(size))        
    return None


def create_decoder(registers):
    """
    Modbus레지스터를 해석할 디코더 객체를 생성한다.
    
      Args:
        registers : Register list

    Returns:
        BinaryPayloadDecoder: 이진수로 구성된 페이로드를 디코딩함.
    """
    decoder = None
    try:
        decoder = BinaryPayloadDecoder.fromRegisters(
            registers, byteorder=Endian.Big, wordorder=Endian.Little)
    except Exception as e:
        logger.error(e)
        logger.error(traceback.format_exc())
        logger.error("Registers : ")
        logger.error(registers)
        
    return decoder


INPUT_REGISTER_EXAMPLE = {"start": 1,
                          "values": [{"value": 15, "type": int, "size": 100}]}


def create_payload_builder():
    return BinaryPayloadBuilder(
        byteorder=Endian.Big, wordorder=Endian.Little)
    
def build_payload(builder:BinaryPayloadBuilder, input):
    value_list = input["values"]

    for value_dict in value_list:
        value = value_dict["value"]
        size = value_dict["size"]

        if type(value) is int and size == 2:
            builder.add_32bit_uint(value)
        elif type(value) is float and size == 1:
            builder.add_16bit_float(value)
        elif type(value) is float and size == 2:
            builder.add_32bit_float(value)
        else:
            builder.add_16bit_uint(value)
    return builder.to_registers()

def parse_register(register:list, data_map):
    decoder:BinaryPayloadDecoder = BinaryPayloadDecoder(register)
    value_map = {}
    for data_unit in data_map:
        name = data_unit["name"]
        type = data_unit["type"]
        size = data_unit["size"]
        if type is int and size is 2:
            value = decoder.decode_32bit_uint()
        elif type is int and size is 1:
            value = decoder.decode_16bit_uint()
        elif type is float and size is 2:
            value = decoder.decode_32bit_float()
        elif type is float and size is 1:
            value = decoder.decode_16bit_float()
        value_map[name] = value
    return value_map



def write(conn:BaseModbusClient, unit, start_address, registers):
    """
    레지스터 정보를 Modbus slave에 쓴다. 

    Args:
        client (BaseModbusClient): 클라이언트 객체
        unit (int) : 유닛 id
        start_address (int): 데이터가 시작하는 시작 주소
        registers (list): 보낼 바이너리 데이터가 저장된 리스트
    """
    #print("write address : ", start_address)

    try:
        response = conn.write_registers(start_address, registers, unit=unit)
        if response.isError():
            logger.error("Write Error code : " + response.exception_code)
            return ResCode.FAIL_TO_WRITE
    except Exception as e:
        logger.error(traceback.format_exc())
        logger.error("Write Error : " + str(e))
        logger.error("unit : " + str(unit))
        logger.error("start address : " + str(start_address))
        logger.error("registers : " + str(registers))
        return ResCode.FAIL_TO_WRITE
    return ResCode.OK



def parse_node_info(decoder: BinaryPayloadDecoder):
    """
    Modbus로 연결을 수행하였다면, 다음에는 양액기를 인식해야 한다. 
    양액기를 인식하기 위해서는 레지스터를 읽어야 한다. 
    레지스터는 ModbusSerialClient 객체의 read_holding_register를 호출해야 한다.
    
    Args:
        decoder: 페이로드 디코딩을 수행할 개체

    Args:
        client_info (Dictionary): 클라이언트 정보
    """

    return {
        "certification": decoder.decode_16bit_uint(),
        "company_code": decoder.decode_16bit_uint(),
        "product_type": decoder.decode_16bit_uint(),
        "product_code": decoder.decode_16bit_uint(),
        "protocol_version": decoder.decode_16bit_uint(),
        "channel_number": decoder.decode_16bit_uint(),
        "serial_number": decoder.decode_32bit_uint()
    }

def parse_device_code_info(decoder:BinaryPayloadDecoder):
    max_device_account = 20
    device_list = []
    
    for i in range(max_device_account):
        device_list.append(decoder.decode_16bit_uint())
    
    return device_list

    

def parse_node_status_info(decoder: BinaryPayloadDecoder):
    """
    현재 레지스터에 등록된 양액기 노드의 상태를 확인한다. 
    양액기 노드 상태는 레지스터 주소인 201, 202, 203에 있다. 양액기 노드에 연결이 되어 있어야 확인할 수 있다.

    Args:
        client_info (dict): 양액기 노드의 상태
    """
    if decoder is None:
        return None

    return {"status": decoder.decode_16bit_uint(),  # 0~6, 900~
            "opid": decoder.decode_16bit_uint(),
            "control": decoder.decode_16bit_uint()}  # 1 Local, 2 Remote, 3  manual


def parse_sensor_status_info(decoder: BinaryPayloadDecoder):
    """
    현재 레지스터에 등록된 양액기 센서의 상태를 확인한다. 레지스터 정보는 204로 시작해서 센서 수 X 3(상태, 값) * (16/8) 바이트만큼 존재한다.
    양액기 노드에 연결이 되어 있어야 확인할 수 있다.

    Args:
        client_info (dict): 양액기 노드 정보
        register_number (int): 양액기 센서 ID
    """
    
    return {"sensor_value": decoder.decode_32bit_float(),
            "sensor_status": decoder.decode_16bit_uint()}


def parse_nutrient_status(decoder: BinaryPayloadDecoder):
    """
    레지스터에 등록된 양액기의 상태를 확인한다. 레지스터의 주소는 401~404까지 있다. 양액기 노드에 연결이 되어 있어야 확인할 수 있다.

    Args:
        client, unit (dict): 양액기 노드 연결 정보
    """
    if decoder is None:
        return None
    
    return {
        # 0~99, 401~403, 900~999 401 준비중, 402 제공중,  403 정지중
        "status": decoder.decode_16bit_uint(),
        "area": decoder.decode_16bit_uint(),
        "alert": decoder.decode_16bit_uint(),  # 0~8
        "opid": decoder.decode_16bit_uint()
    }
    
def get_nutrient_controlv3_map():
    return [{"name" : "cmd", "type": int, "size":1},
            {"name" : "opid", "type": int, "size":1},
            {"name" : "start-area", "type": int, "size":1},
            {"name" : "stop-area", "type": int, "size":1},
            {"name" : "on-sec", "type": int, "size":1},
            {"name" : "EC", "type": float, "size":2},
            {"name" : "pH", "type": float, "size":2}]
            

def parse_nutrient_controlv3(decoder:BinaryPayloadDecoder):
    """
    레지스터에 등록된 양액기의 관수 정보를 확인한다. 레지스터의 주소는 504~513까지 있다. 양액기 노드에 연결이 되어 있어야 확인할 수 있다.
    Args:
        client, unit (dict): 양액기 노드 연결 정보
    """
    if decoder is None:
        return None
    
    return {
        # 2 : control, 401 : ON, 0 : OFF, 402 : AREA_ON, 403 : PARAM_ON : 403
        "cmd": decoder.decode_16bit_uint(),
        "opid": decoder.decode_16bit_uint(),
        "start-area": decoder.decode_16bit_uint(),
        "stop-area": decoder.decode_16bit_uint(),
        "on-sec": decoder.decode_32bit_uint(),
        "EC": decoder.decode_32bit_float(),
        "pH": decoder.decode_32bit_float()
    }


def node_control_input(cmd, opid, control):
    return {"start": StandardAddress.NODE_CONTROL_COMMAND.value,
            "values": [{"value": cmd, "type": int, "size": 1},
                       {"value": opid, "type": int, "size": 1},
                       {"value": control, "type": int, "size": 1}]}
    
def get_off_input(opid):
    """
    레지스터에 양액기 제어를 요청한다. get_nutrient_control()의 반대역할을 수행한다. write 함수를 사용하여 레지스터에 값을 설정하면, Device가 이를 수신한다.

    Args:
        client, unit (dict): 연결 정보
    """
    return {"start": StandardAddress.NUTRIENT_CONTROL_COMMAND.value,
            "values": [{"value": 0, "type": int, "size": 1},
                       {"value": opid, "type": int, "size": 1}, 
                       {"value": 0, "type": int, "size": 1},
                       {"value": 0, "type": int, "size": 1},
                       {"value": 0.0, "type": float, "size": 2}, 
                       {"value": 0.0, "type": float, "size": 2},
                       {"value": 0.0, "type": float, "size": 2}]}

    
def get_on_input(opid):
    """
    레지스터에 양액기 제어를 요청한다. get_nutrient_control()의 반대역할을 수행한다. write 함수를 사용하여 레지스터에 값을 설정하면, Device가 이를 수신한다.

    Args:
        client, unit (dict): 연결 정보
    """
    return {"start": StandardAddress.NUTRIENT_CONTROL_COMMAND.value,
            "values": [{"value": 401, "type": int, "size": 1},
                       {"value": opid, "type": int, "size": 1}, 
                       {"value": 0, "type": int, "size": 1},
                       {"value": 0, "type": int, "size": 1},
                       {"value": 0.0, "type": float, "size": 2}, 
                       {"value": 0.0, "type": float, "size": 2},
                       {"value": 0.0, "type": float, "size": 2}]}

    
    
def get_area_on_input(opid, start_area, stop_area, on_sec):
    """
    레지스터에 양액기 제어를 요청한다. get_nutrient_control()의 반대역할을 수행한다. write 함수를 사용하여 레지스터에 값을 설정하면, Device가 이를 수신한다.

    Args:
        client, unit (dict): 연결 정보
    """
    return {"start": StandardAddress.NUTRIENT_CONTROL_COMMAND.value,
            "values": [{"value": 402, "type": int, "size": 1},
                       {"value": opid, "type": int, "size": 1},
                       {"value": start_area, "type": int, "size": 1},
                       {"value": stop_area, "type": int, "size": 1},
                       {"value": on_sec, "type": int, "size": 2}, 
                       {"value": float(0), "type": float, "size": 2},
                       {"value": float(0), "type": float, "size": 2}]}


def get_param_on_input(opid, start_area, stop_area, on_sec, EC, pH):
    """
    레지스터에 양액기 제어를 요청한다. get_nutrient_control()의 반대역할을 수행한다. write 함수를 사용하여 레지스터에 값을 설정하면, Device가 이를 수신한다.

    Args:
        client, unit (dict): 연결 정보
    """
    return {"start": StandardAddress.NUTRIENT_CONTROL_COMMAND.value,
            "values": [{"value": 403, "type": int, "size": 1},
                       {"value": opid, "type": int, "size": 1},
                       {"value": start_area, "type": int, "size": 1},
                       {"value": stop_area, "type": int, "size": 1},
                       {"value": int(on_sec), "type": int, "size": 2},
                       {"value": float(EC), "type": float, "size": 2},
                       {"value": float(pH), "type": float, "size": 2}]}
    
def get_change_control_input(opid, command, control):
    """

    Args:
        opid (int): [description]
        command (int): [description]
        control (int): [description]

    Returns:
        dict : write할 값 목록입니다.
    """
    return {"start":StandardAddress.NODE_CONTROL_COMMAND.value,
            "values": [{"value": command, "type":int, "size":1},
                       {"value": opid, "type":int, "size":1},
                       {"value": control, "type":int, "size":1}]}
    

if __name__ == "__main__":
    client = connect(host="127.0.0.1", port=2882, method="tcp")
    

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import sys
import time
import importlib
import json
#import paho.mqtt.subscribe as subscribe
from queue import Queue
from daemon import Daemon, Runner
from mate import Mate
from mblock import MBlock, BlkType

class CoupleManager(Runner):
    _couples = []

    def __init__(self, configfile):
        fp = open(configfile, 'r')
        self._option = json.loads(fp.read())
        fp.close()

    def loadcandidates(self):
        couples = [{
            'id': '1',
            'ssmate': {
                'mod':'mate_hspestapi', 
                'class':'HSPestAPIMate', 
                'opt':{
                    "conn" : {"base" : "http://hs.jinong.co.kr:8080/hsict/pest/"},
                    "conn_old" : {"base" : "http://49.247.212.106:8080/hsict/pest/"}
                }
            },
            'dsmate': {
                'mod':'mate_hscdtp',
                'class':'HSCDTPMate', 
                'opt':{
                    'conn':{'port': 10010},
                    'sleeptime': 53,
                    'gatewaykey' : 'JN-01'
                }
            },
            'devinfo': None
        }]

        return couples

    def popmate(self, candy):
        for idx in range(len(self._couples)):
            if self._couples[idx]['id'] == candy['id']:
                return self._couples.pop(idx)
        return None

    def execute(self, candy):
        ssmate = candy['ssmate']
        dsmate = candy['dsmate']
        devinfo = candy['devinfo']
        couple = {}
        couple['id'] = candy['id']
        couple['ssmate'] = self.loadmate(ssmate, devinfo)
        couple['dsmate'] = self.loadmate(dsmate, devinfo)
        couple['ssmate'].start(couple['dsmate'].writeblk)
        couple['dsmate'].start(couple['ssmate'].writeblk)
        return couple

    def loadmate(self, conf, devinfo):
        module = importlib.import_module(conf['mod'])
        class_ = getattr(module, conf['class'])
        mate = class_(conf['opt'], devinfo, self.logger)
        return mate

    def stopold(self):
        for couple in self._couples:
            couple['ssmate'].stop()
            couple['dsmate'].stop()
            print(couple['id'], "stopped")

    def stop(self):
        print("Couple Manager tries to stop")
        self._isrunning = False

    def run(self):
        self._isrunning = True
        i = 0
        while self._isrunning:
            if i % 10 == 0:
                newcouples = []
                candidates = self.loadcandidates()
                for candy in candidates:
                    couple = self.popmate(candy)
                    if couple is None:
                        couple = self.execute(candy)
                    newcouples.append(couple)

                self.stopold()
                self._couples = newcouples
                i = 0
            i = i + 1
            time.sleep(self._option['sleep'])
        self.stopold()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python couplemanager.py [start|stop|restart|run]")
        sys.exit(2)

    mode = sys.argv[1]
    runner = CoupleManager('cpmng.conf')
    adaemon = Daemon('cpmng', runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.stop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)

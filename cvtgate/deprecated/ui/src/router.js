import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
Vue.use(Router)

const requireAuth = () => (from, to, next) => {
  if (store.getters.getIsAuth) return next()
  next('/login')
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: () => import(/* webpackChunkName: "about" */ '@/views/Main'),
      beforeEnter: requireAuth()
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ '@/views/Login')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

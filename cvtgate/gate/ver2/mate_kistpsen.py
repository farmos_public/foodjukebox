#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
from enum import IntEnum
from threading import Thread, Lock

from imports import *
from mate import Mate, ThreadMate, DevType

class KISTPlantSensorMate(ThreadMate):
    _DIR = "/home/pi/Datalogging/"
    _FILES = ["ave_CO2", "ave_HD", "ave_IR_temp", "ave_PYR", "ave_env_humi", "ave_env_temp", "ETc_amount", "ETc_rate"]

    def __init__(self, option, devinfo, coupleid, logger):
        super(KISTPlantSensorMate, self).__init__(option, devinfo, coupleid, logger)

    def readvaluefromfile(self, fname):
        try:
            with open(fname) as f:
                return float(f.readline())
        except Exception as ex:
            self._logger.warn("exception : " + str(ex))
            return None

    def readmsg(self):
        pass

    def writeblk(self, blk):
        print("received message", blk.getdevid(), self._coupleid)
        if BlkType.isrequest(blk.gettype()) is False:
            self._logger.warn("The message is not request. " + str(blk.gettype()))
            return False

        response = Response(blk)
        cmd = blk.getcommand()
        nd = self._devinfo.finddevbyid(blk.getnodeid())
        dev = self._devinfo.finddevbyid(blk.getdevid())

        if dev is None:
            self._logger.warn("There is no device. " + str(blk.getdevid()))
            code = ResCode.FAIL_NO_DEVICE

        elif DevType.ispropercommand(dev['dt'], cmd) is False:
            self._logger.warn("The request is not proper. " + str(cmd) + " " + str(dev['dt']))
            code = ResCode.FAIL_NOT_PROPER_COMMAND

        elif DevType.isactuator(dev['dt']) or DevType.isnode(dev['dt']):
            # modbus
            code = self.processrequest(dev, blk, nd)
            self._logger.info("Actuator processed : " + str(code))

        elif DevType.isgateway(dev['dt']):
            self._logger.info("Gateway does not receive a request")
            code = ResCode.FAIL

        else:
            self._logger.warn("Unknown Error. " + str(blk) + ", " + str(dev))
            code = ResCode.FAIL

        response.setresult(code)
        self._logger.info("write response: " + str(response))
        self.writecb(response)
        return True #if code == ResCode.OK else False

    def sendobs(self):
        for gw in self._devinfo:
            for nd in gw["children"]:
                obsblk = Observation(nd["id"])
                obsblk.setobservation(nd["id"], 0, StatCode.READY)
                for dev in nd["children"]:
                    if DevType.issensor(dev["dt"]):
                        idx = int(dev["dk"])
                        fname = KISTPlantSensorMate._DIR + KISTPlantSensorMate._FILES[idx]
                        value = self.readvaluefromfile(fname)
                        if value is not None:
                            obsblk.setobservation(dev["id"], value, StatCode.READY)
                        else:
                            obsblk.setobservation(dev["id"], 0, StatCode.ERROR)
                self.writecb(obsblk)

    def sendnoti(self):
        pass

    def start(self, writecb):
        super(KISTPlantSensorMate, self).start(writecb)
        return True

    def stop(self):
        super(KISTPlantSensorMate, self).stop()
        return True

if __name__ == "__main__":
    opt = {
    }
    
    devinfo = [{
        "id" : "1", "dk" : "JND2", "dt": "gw", "children" : [{
            "id" : "101", "dk" : '', "dt": "nd", "children" : [
                {"id" : "102", "dk" : '0', "dt": "sen"},
                {"id" : "103", "dk" : '1', "dt": "sen"},
                {"id" : "104", "dk" : '2', "dt": "sen"}
             ]
        }]
    }]

    kmate = KISTPlantSensorMate(opt, devinfo, "1", None)

    mate = Mate ({}, [], "1", None)
    kmate.start (mate.writeblk)
    print("mate started")

    time.sleep(30)
    kdmate.stop()
    print("mate stopped") 

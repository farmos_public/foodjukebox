SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- 데이터베이스 생성
CREATE database farmos;
-- 사용자 생성
CREATE USER 'farmos'@'localhost' IDENTIFIED BY 'farmosv2@';
GRANT ALL PRIVILEGES ON farmos.* TO 'farmos'@'localhost';
FLUSH PRIVILEGES;
USE farmos;

-- ----------------------------
-- Table structure for configuration
-- ----------------------------
CREATE TABLE `configuration` (
  `type` varchar(255) NOT NULL,
  `configuration` text NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT into configuration(type, configuration) values('initialize','false'); 

-- ----------------------------
-- Table structure for core_rule_applied
-- ----------------------------
CREATE TABLE `core_rule_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `used` int(1) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `constraints` text,
  `configurations` text,
  `inputs` text,
  `controllers` text,
  `outputs` text,
  `autoapplying` int(1) NOT NULL DEFAULT '0',
  `sched` int(1) NOT NULL,
  `advanced` int(1) NOT NULL DEFAULT '0',
  `groupname` varchar(255) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_core_rule_applied_fields_1` (`field_id`),
  CONSTRAINT `fk_core_rule_applied_fields_1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for core_rule_template
-- ----------------------------
CREATE TABLE `core_rule_template` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `autoapplying` int(1) DEFAULT NULL,
  `constraints` text,
  `configurations` text,
  `controllers` text,
  `outputs` text,
  `groupname` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `desc` text,
  `sched` int(1) NOT NULL,
  `advanced` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idx`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for core_timespan
-- ----------------------------
CREATE TABLE `core_timespan` (
  `id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `timespan` text,
  `name` varchar(50) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`field_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for current_observations
-- ----------------------------
CREATE TABLE `current_observations` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` double DEFAULT NULL,
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`) USING BTREE,
  CONSTRAINT `fk_current_observations_dataindexes` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dataindexes
-- ----------------------------
CREATE TABLE `dataindexes` (
  `id` int(11) NOT NULL,
  `rule_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `sigdigit` int(11) DEFAULT '0',
  `device_id` int(11) DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for device_field
-- ----------------------------
CREATE TABLE `device_field` (
  `device_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `sort_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`,`field_id`),
  KEY `FK_device_field_field_id_fields_id` (`field_id`),
  CONSTRAINT `FK_device_field_device_id_devices_id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `FK_device_field_field_id_fields_id` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for devices
-- ----------------------------
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `spec` text NOT NULL,
  `gateid` varchar(255) NOT NULL,
  `coupleid` varchar(255) NOT NULL,
  `nodeid` int(11) NOT NULL,
  `compcode` int(11) NOT NULL,
  `devcode` int(11) DEFAULT NULL,
  `devindex` int(11) DEFAULT NULL,
  `nodetype` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment
-- ----------------------------
CREATE TABLE `experiment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `memo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `lastupdated` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_comments
-- ----------------------------
CREATE TABLE `experiment_comments` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` text,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`,`obs_time`),
  CONSTRAINT `fk_observations_etc_dataindexes_1` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_items
-- ----------------------------
CREATE TABLE `experiment_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_experiment_type_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_sample
-- ----------------------------
CREATE TABLE `experiment_sample` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_experiment_sample_fields_1` (`field_id`),
  CONSTRAINT `fk_experiment_sample_fields_1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_sample_map
-- ----------------------------
CREATE TABLE `experiment_sample_map` (
  `experiment_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  `deleted` int(1) DEFAULT '0',
  `sample_item` text,
  PRIMARY KEY (`experiment_id`,`sample_id`),
  KEY `fk_experiment_sample_map_experiment_1` (`experiment_id`) USING BTREE,
  KEY `fk_experiment_sample_map_experiment_sample_1` (`sample_id`),
  CONSTRAINT `fk_experiment_sample_map_experiment_1` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`),
  CONSTRAINT `fk_experiment_sample_map_experiment_sample_1` FOREIGN KEY (`sample_id`) REFERENCES `experiment_sample` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_user_map
-- ----------------------------
CREATE TABLE `experiment_user_map` (
  `experiment_id` int(11) NOT NULL,
  `farmos_user_id` int(11) NOT NULL,
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`experiment_id`,`farmos_user_id`) USING BTREE,
  KEY `fk_farmos_user_id_1` (`farmos_user_id`),
  KEY `fk_experiment_id_1` (`experiment_id`) USING BTREE,
  CONSTRAINT `fk_experiment_user_experiment_1` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`),
  CONSTRAINT `fk_farmos_user_id_1` FOREIGN KEY (`farmos_user_id`) REFERENCES `farmos_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for farm
-- ----------------------------
CREATE TABLE `farm` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `info` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for farmos_user
-- ----------------------------
CREATE TABLE `farmos_user` (
  `userid` varchar(50) NOT NULL,
  `passwd` varchar(50) NOT NULL,
  `privilege` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `basicinfo` text,
  `appinfo` text,
  `loginip` varchar(50) DEFAULT NULL,
  `lastupdated` datetime NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uq_farmos_user_userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for fields
-- ----------------------------
CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `fieldtype` text NOT NULL,
  `uiinfo` text,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_fields_farm_1` (`farm_id`),
  CONSTRAINT `fk_fields_farm_1` FOREIGN KEY (`farm_id`) REFERENCES `farm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `path` varchar(255) NOT NULL,
  `meta` text,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_gallery_dataindexes` (`data_id`),
  CONSTRAINT `fk_gallery_dataindexes` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gate_info
-- ----------------------------
CREATE TABLE `gate_info` (
  `uuid` varchar(255) NOT NULL,
  `couple` varchar(255) NOT NULL,
  `detect` text,
  PRIMARY KEY (`uuid`,`couple`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for observations
-- ----------------------------
CREATE TABLE `observations` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` double DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`,`obs_time`),
  KEY `obs_index_dataid` (`data_id`),
  CONSTRAINT `fk_observations_dataindexes_1` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for process
-- ----------------------------
CREATE TABLE `process` (
  `idx` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cmd_start` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cmd_stop` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `log` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `deleted` int(1) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idx`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for requests
-- ----------------------------
CREATE TABLE `requests` (
  `opid` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `command` int(11) NOT NULL,
  `params` text NOT NULL,
  `sentcnt` int(11) NOT NULL DEFAULT '1',
  `senttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exectime` timestamp NULL DEFAULT NULL,
  `finishtime` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for units
-- ----------------------------
CREATE TABLE `units` (
  `unit` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nvalue` int(11) NOT NULL,
  `converted` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`unit`,`nvalue`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

BEGIN;
INSERT INTO `units` VALUES ('crop', 1, '토마토');
INSERT INTO `units` VALUES ('crop', 2, '딸기');
INSERT INTO `units` VALUES ('crop', 3, '파프리카');
INSERT INTO `units` VALUES ('crop', 4, '포도');
INSERT INTO `units` VALUES ('growth_period', 0, '휴지기');
INSERT INTO `units` VALUES ('growth_period', 1, '개화기');
INSERT INTO `units` VALUES ('growth_period', 2, '비대기');
INSERT INTO `units` VALUES ('growth_period', 3, '수확기');
INSERT INTO `units` VALUES ('growth_period', 11, '초기');
INSERT INTO `units` VALUES ('growth_period', 12, '중기');
INSERT INTO `units` VALUES ('growth_period', 13, '중기-1');
INSERT INTO `units` VALUES ('growth_period', 14, '중기-2');
INSERT INTO `units` VALUES ('growth_period', 15, '중기-3');
INSERT INTO `units` VALUES ('growth_period', 16, '중기-4');
INSERT INTO `units` VALUES ('growth_period', 17, '말기');
INSERT INTO `units` VALUES ('isday', 0, '야간');
INSERT INTO `units` VALUES ('isday', 1, '주간');

insert into units (unit, nvalue, converted) values ('growth_period', 21, '휴면기');
insert into units (unit, nvalue, converted) values ('growth_period', 22, '발아기');
insert into units (unit, nvalue, converted) values ('growth_period', 23, '개화기');
insert into units (unit, nvalue, converted) values ('growth_period', 24, '과립비대기');
insert into units (unit, nvalue, converted) values ('growth_period', 25, '착색기');
insert into units (unit, nvalue, converted) values ('growth_period', 26, '수확기');
insert into units (unit, nvalue, converted) values ('growth_period', 27, '양분축적기');


insert into units (unit, nvalue, converted) values ('soil', 1, '사토');
insert into units (unit, nvalue, converted) values ('soil', 2, '양질 사토');
insert into units (unit, nvalue, converted) values ('soil', 3, '사양토');
insert into units (unit, nvalue, converted) values ('soil', 4, '양토');
insert into units (unit, nvalue, converted) values ('soil', 5, '미사질 양토');
insert into units (unit, nvalue, converted) values ('soil', 6, '사질 식양토');
insert into units (unit, nvalue, converted) values ('soil', 7, '식양토');
insert into units (unit, nvalue, converted) values ('soil', 8, '미사질 식양토');
insert into units (unit, nvalue, converted) values ('soil', 9, '미사질 식토');
insert into units (unit, nvalue, converted) values ('soil', 10, '식토');
COMMIT;

-- ----------------------------
-- Table structure for uploadhistory
-- ----------------------------
CREATE TABLE `uploadhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uptype` varchar(100) NOT NULL,
  `originalfile` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  `meta` text,
  `processed` int(1) NOT NULL,
  `uptime` datetime NOT NULL,
  `pctime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- Table structure for nutrient_supply
-- ----------------------------
CREATE TABLE `nutrient_supply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obstime` datetime NOT NULL,
  `nutinfo` text,
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`,`obstime`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


/*
 * EEPROM Clear
 *
 * Sets all of the bytes of the EEPROM to 0.
 * Please see eeprom_iteration for a more in depth
 * look at how to traverse the EEPROM.
 *
 * This example code is in the public domain.
 */

#include <EEPROM.h>

void setup() {
  Serial.begin(9600);
}

void display() {
  Serial.println ("Current Board Setting : ");
  Serial.print ("Number of Toggle board : ");
  Serial.println (EEPROM.read(0));
  Serial.print ("Number of Switch board : ");
  Serial.println (EEPROM.read(1));
}

void loop() {
  if (Serial.available() > 0) {
    String instr = Serial.readStringUntil('\n');
    if (instr[0] == 'C') {
      EEPROM.write(0, 0);
      EEPROM.write(1, 0);
      Serial.println ("EEPROM is clean");
    } else if (instr[0] == 'L') {
      ;
    } else if (instr[0] == 'T' && isDigit(instr[1])) {
      EEPROM.write(0, instr[1] - '0');

    } else if (instr[0] == 'S' && isDigit(instr[1])) {
      EEPROM.write(1, instr[1] - '0');
    } else {
      Serial.print ("Unknown command : ");
      Serial.println (instr);
      return;
    }
    display();
  }
  delay(500);
}

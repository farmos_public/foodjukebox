#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

cd $SHELL_PATH

#sudo pip3 install deepdiff

fjboxname=$(cat /etc/hostname)

fjboxserial=${fjboxname:5}

echo -e $fjboxserial

if [ ${fjboxserial} -lt 1000 ]; then
    connServer=fjboxtest.farmos.co.kr
    temp=$fjboxserial
else
    temp=$(echo $fjboxserial | cut -c 2-)
    serverNum=`expr $temp / 15 + 1`
    if [ ${serverNum} -lt 10 ]; then
        connServer=fjbox00$serverNum.farmos.co.kr
    else
        connServer=fjbox0$serverNum.farmos.co.kr
    fi
fi

echo -e "test"


cat << "EOF" > "/home/pi/farmosv2-script/nidana/conf/dp.conf"
{
  "masternode" : {
    "host" : "fjbox.farmos.co.kr",
    "user" : "farmos",
    "password" : "farmosv2@",
    "db" : "master_node"
  },
  "datanode" : {
EOF

echo "    \"host\" : \"${connServer}\"," >> "/home/pi/farmosv2-script/nidana/conf/dp.conf"
echo "    \"farms\" : [${fjboxserial}]">> "/home/pi/farmosv2-script/nidana/conf/dp.conf"

cat << "EOF" >> "/home/pi/farmosv2-script/nidana/conf/dp.conf"
  },
  "path" : {
    "backup" : "backup",
    "image" : "../backup/image",
    "upload" : "data",
    "trash" : "trash",
    "dataset" : "../analysis"
  }
}
EOF

su - root -c "
cat <(crontab -l) <(echo '45 0 * * * cd /home/pi/farmosv2-script/nidana; python3 boxsync.py sync $fjboxserial') | crontab
"

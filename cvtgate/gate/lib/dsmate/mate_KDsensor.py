#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2018 FarmOS, Inc. All right reserved.
#
# Online AWS
#


import requests
import json
import time

from .. import *
class KDSENSORMate(DSMate):
    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)

        copt = self._option["urlkey"]
        
        with open('/root/api.json', 'r') as file:
            data = json.load(file)

        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])

        for i in range(len(data["datas"])):
            if data["datas"][i]["device_id"]=="{farm}".format(farm=copt["farm"]):
                for dev in nd["children"] :
                    for row in data["datas"][i]["sensors"]:
                        if dev["dk"]== "{}-{}-{}-{}".format(row["h_idx"],row["n_idx"],row["channel"],row["type"]):
                        
                            obsblk.setobservation(dev["id"], self.getvalue(dev["id"], float(row["value"])), StatCode.READY)
                            break
        self._writeobs(obsblk)

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "urlkey" : {
            "key" : "xxx",
            "farm" : "BE3148"}
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : "h_idx", "dt": "sen"},
                {"id" : "4", "dk" : "n_idx", "dt": "sen"},
                {"id" : "5", "dk" : "type", "dt": "sen"},
                {"id" : "6", "dk" : "channel", "dt": "sen"},
                {"id" : "7", "dk" : "value", "dt": "sen"},
            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = KDSENSORMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
import requests
from enum import IntEnum
from threading import Thread, Lock

from imports import *
from mate import Mate, ThreadMate, DevType

class SmartKDAWSMate(ThreadMate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(SmartKDAWSMate, self).__init__(option, devinfo, coupleid, logger)
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._url = option["conn"]["url"]

    def connect(self):
        return super(SmartKDAWSMate, self).connect()

    def close(self):
        super(SmartKDAWSMate, self).close()

    def readmsg(self):
        pass

    def processrequest(self, dev, request, node):
        return ResCode.OK

    def writeblk(self, blk):
        print("received message", blk.getdevid(), self._coupleid)
        if BlkType.isrequest(blk.gettype()) is False:
            self._logger.warn("The message is not request. " + str(blk.gettype()))
            return False

        response = Response(blk)
        self._logger.info("This mate does not receive a request")
        code = ResCode.FAIL

        response.setresult(code)
        self.writecb(response)
        return True #if code == ResCode.OK else False

    def sendobs(self):
        res = requests.get(self._url)
        if res.status_code == 200:
            ret = res.json()
            if ret["status"] == "success":
                for msg in ret["datas"]:
                    self.sendobservation(msg)

    def sendnoti(self):
        pass

    def sendobservation(self, ndinfo):
        obsblk = Observation(ndinfo["device_id"])
        obsblk.setobservation(ndinfo["device_id"], 0, StatCode.READY)

        for info in ndinfo["sensors"]:
            devid = int(info["h_idx"]) * 1000000 + int(info["n_idx"]) * 10000 + int(info["type"]) * 100 + int(info["channel"])
            obsblk.setobservation(devid, info["value"], StatCode.READY)

        self.writecb(obsblk)

    def start(self, writecb):
        super(SmartKDAWSMate, self).start(writecb)
        return True

    def stop(self):
        super(SmartKDAWSMate, self).stop()
        return True

if __name__ == "__main__":
    isnutri = False
    opt = {
        'conn' : {
            'url': 'http://52.78.109.164/farmcube/test_jinong_all_sensor_for_js.php'
        }
    }
    
    devinfo = [{
        "id" : "smartkdaws", "dk" : "USB0", "dt": "gw", "children" : [
        ]
    }]
    kdmate = SmartKDAWSMate(opt, devinfo, "1", None)

    mate = Mate ({}, [], "1", None)
    kdmate.start (mate.writeblk)
    print("mate started")

    time.sleep(10)
    kdmate.stop()
    print("mate stopped") 

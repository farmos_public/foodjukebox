#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

cd ..
FARMOS_PATH=`pwd -P`


if [ -e opensource_ui ]; then
    rm -rf opensource_ui
fi

git clone -b ui --single-branch https://gitlab.com/farmos_private/open_source-ui_build.git

mv open_source-ui_build/ui common_api/api/public 
rm -rf open_source-ui_build

npm install --prefix ./common_api --unsafe-perm

cat /dev/null > /home/pi/farmosv2-script/updates/log/temp


#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc. All right reserved.
#
# GREENCS API
#


from collections import defaultdict
import requests
import json
import struct
import time
import traceback
from datetime import datetime
from decimal import *
import pymysql
from .. import *

class GREENCSMate(DSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(GREENCSMate, self).__init__(option, devinfo, coupleid, quelist)
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self.pastdata={}
        for dk, api in self._option["api"].items():
            self.pastdata[dk] = self.loaddata(api)

    def loaddata(self, api):
        try :
            data = json.loads(requests.get(api).text)
        except Exception as ex:
            self._logger.warn("load error : " + str(ex))
            return None
        return data

    def getdata(self, data, key):
        return data["fields"][0][key]
        
    def process(self):
        for gw in self._devinfo:
            nd = gw["children"][0]
            data = self.loaddata(self._option["api"][gw["dk"]])
            self.senddatafornode(data, gw, nd)
            self.pastdata[gw["dk"]] = data

    def senddatafornode(self, data, gw, nd):
        obsblk = Observation(nd["id"])
        notiblk = Notice(nd["id"], NotiCode.ACTUATOR_STATUS)
        for dev in nd["children"]:
            if DevType.issensor(dev["dt"]):
                obsblk.setobservation(dev["id"], self.getvalue(dev["id"], self.getsensorinfo(dev, data)), StatCode.READY)
            elif DevType.isswitch(dev["dt"]):
                notiblk.setcontent(dev["id"], {"opid": 0,"status" : self.getswitchinfo(dev, data)})
            elif DevType.isretractable(dev["dt"]):
                (stat , value) = self.getretractableinfo(dev, data, gw)
                notiblk.setcontent(dev["id"], {"opid": 0,"status" : stat, "position" : value})
            else:
                self._logger.error("Unknown datatype error" + dev["dt"])
        self._writeobs(obsblk)
        self._writenoti(notiblk)

    def getsensorinfo(self, dev, data):
        TRANSLATE = {"북": 0,"우": 90,"남": 180,"좌": 270,"로그온":1,"로그아웃":0,"정상":1,"정전":0}
        if self.getdata(data, dev["dk"]) in TRANSLATE:
            obs = float(TRANSLATE[self.getdata(data, dev["dk"])])
        elif self.getdata(data, dev["dk"])=="":
            obs = None
        else:
            obs = float(self.getdata(data, dev["dk"]))
        return obs

    def getswitchinfo(self, dev, data):
        TRANSLATE = {"기동": StatCode.WORKING.value, "정지": StatCode.READY.value, "자동": StatCode.WORKING.value, "수동": StatCode.MANUAL_WORKING.value, "정전": StatCode.ERROR.value, "정상": StatCode.READY.value}
        return TRANSLATE[self.getdata(data, dev["dk"])]

        
    def getretractableinfo(self, dev, data, gw):
        cutstr=dev["dk"].split(",")
        pos = cutstr[0]
        auto = cutstr[1]
        pos_value = self.getdata(data,pos)
        past_pos_value = self.getdata(self.pastdata[gw["dk"]],pos)
        
        if self.getdata(data, auto) == "자동":
            if pos_value == past_pos_value:
                return StatCode.READY.value, pos_value
            elif pos_value < past_pos_value:
                return StatCode.CLOSING.value, pos_value
            elif pos_value > past_pos_value:
                return StatCode.OPENING.value, pos_value

        elif self.getdata(data, auto) == "수동":
            if pos_value == past_pos_value:
                return StatCode.READY.value, pos_value
            elif pos_value < past_pos_value:
                return StatCode.MANUAL_CLOSING.value, pos_value
            elif pos_value > past_pos_value:
                return StatCode.MANUAL_OPENING.value, pos_value
        else:
            self._logger.error("Unknown data" + self.getdata(data, auto))
            return None

    '''
    def sendsensorinfofornode(self, dev, data, obsblk):
        TRANSLATE = {"북": 0,"우": 90,"남": 180,"좌": 270,"로그온":1,"로그아웃":0,"정상":1,"정전":0}
        if self.getdata(data, dev["dk"]) in TRANSLATE:
            obs = float(TRANSLATE[self.getdata(data, dev["dk"])])
        elif self.getdata(data, dev["dk"])=="":
            obs = None
        else:
            obs = float(self.getdata(data, dev["dk"]))
        obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)
   
    def sendswitchinfofornode(self, dev, data, notiblk):
        TRANSLATE = {"기동": StatCode.WORKING.value, "정지": StatCode.READY.value, "자동": StatCode.WORKING.value, "수동": StatCode.MANUAL_WORKING.value}
        stat = TRANSLATE[self.getdata(data, dev["dk"])]
        notiblk.setcontent(dev["id"], {"opid": 0,"status" : stat})

        
    def sendretractableinfofornode(self, dev, data, gw, notiblk):
        pos = dev["dk"].split(",")[0]
        pos_value = self.getdata(data)[pos]
        auto = dev["dk"].split(",")[1]
        past_pos_value = self.pastdata[gw["dk"]]["fields"][0][pos]

        if self.getdata(data, auto) == "자동":
            if pos_value == past_pos_value:
                return notiblk.setcontent(dev["id"], {"opid": 0,"status" : StatCode.READY.value})
            elif pos_value < past_pos_value:
                return notiblk.setcontent(dev["id"], {"opid": 0,"status" : StatCode.CLOSING.value})
            elif pos_value > past_pos_value:
                return notiblk.setcontent(dev["id"], {"opid": 0,"status" : StatCode.OPENING.value})

        elif self.getdata(data, auto) == "수동":
            if pos_value == past_pos_value:
                return notiblk.setcontent(dev["id"], {"opid": 0,"status" : StatCode.READY.value})
            elif pos_value < past_pos_value:
                return notiblk.setcontent(dev["id"], {"opid": 0,"status" : StatCode.MANUAL_CLOSING.value})
            elif pos_value > past_pos_value:
                return notiblk.setcontent(dev["id"], {"opid": 0,"status" : StatCode.MANUAL_OPENING.value})
        else:
            self._logger.error("Unknown data" + self.getdata(data, auto))
            return None
    '''
if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "api":  {
            "1": "http://api.gcsmagma.com/gcs_fb_api.php/Get_GCS_Data/jaresb4/1",
            "2": "http://api.gcsmagma.com/gcs_fb_api.php/Get_GCS_Data/jaresb4/2"
        }
    }

    devinfo = [
        {"id": "1", "dk": "1", "dt": "gw", "children": [{
            "id": "2", "dk": "1", "dt": "nd", "children": [
                {"id": "3", "dk": "XINTEMP", "dt": "sen"},
                {"id": "4", "dk": "XINTEMP1", "dt": "sen"},
                {"id": "5", "dk": "XINTEMP2", "dt": "sen"},
                {"id": "6", "dk": "XINTEMP3", "dt": "sen"},
                {"id": "7", "dk": "XINHUM", "dt": "sen"},
                {"id": "8", "dk": "XINHUM1", "dt": "sen"},
                {"id": "9", "dk": "XINHUM2", "dt": "sen"},
                {"id": "10", "dk": "XSKYVOL1,XSKYAUTO", "dt": "act/retractable/level0"}
            ]
        }]
        },
        {"id": "11", "dk": "2", "dt": "gw", "children": [{
            "id": "12", "dk": "2", "dt": "nd", "children": [
                {"id": "13", "dk": "XINTEMP", "dt": "sen"},
                {"id": "14", "dk": "XINTEMP1", "dt": "sen"},
                {"id": "15", "dk": "XINTEMP2", "dt": "sen"},
                {"id": "16", "dk": "XINTEMP3", "dt": "sen"},
                {"id": "17", "dk": "XINHUM", "dt": "sen"},
                {"id": "18", "dk": "XINHUM1", "dt": "sen"},
                {"id": "19", "dk": "XINHUM2", "dt": "sen"},
                {"id": "20", "dk": "XSKYVOL2,XSKYAUTO", "dt": "act/retractable/level0"}
            ]
        }]
        }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = GREENCSMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

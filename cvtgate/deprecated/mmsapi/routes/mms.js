const express = require('express')
const mysql = require('sync-mysql')

const router = express.Router()

const connection =  new mysql({
  host     : '210.89.179.119',
  user     : 'root',
  password : 'jinong',
  database : 'api_uuid'
})

const axios = require('axios');
const sendSms = ({key, user_id, sender, receiver, msg, testmode_yn}) => {      
  return axios.post('https://apis.aligo.in/send/', null, {    
      params: {
          key: 'd7c5ijzh6o8wzfuryjk7m2w0uw19pmy9',
          user_id: user_id,
          sender: sender,
          receiver: receiver,
          msg: msg,
          testmode_yn: testmode_yn
      },
  }).then(function(res){                   
      return res.data
  }).catch(function(err){              
      console.log('err', err);
  })
}




/* GET users listing. */
router.post('/', function(req, res, next) {  
  const params = [req.body.key, req.body.msg, req.body.receiver, req.body.sender]    
  const resultA = connection.queueQuery('select uuid from sms_uuid where uuid = ?',[req.body.key])    
  const resultB = connection.queueQuery('insert into sms_history (uuid, date, message, receiver, sender) values (?, date_format(now(), "%Y-%m-%d %H:%i:%s"), ?, ?, ?)', params)  

  try {            
    if(resultA()[0].uuid === req.body.key){
      sendSms({key : req.body.key,
        user_id : req.body.user_id, 
        sender : req.body.sender,
        receiver : req.body.receiver,
        msg : req.body.msg,
        testmode_yn : req.body.testmode_yn
      }).then((result) => {      
        console.log('전송결과', result)           
        if(result.result_code == 1){            
          resultB()
        }
        res.send(result)     
      }).catch(err => {          
        console.log('err', err)    
      })               
    }else{
      res.send({'mesg' : '올바른 key값이 아닙니다'})     
    }
  }catch (error){
    console.log(error)
    res.send({'mesg' : '올바른 key값이 아닙니다'})     
  }
  
  // connection.dispose()

    // connection.connect();
    // connection.query('select uuid from sms_uuid', function (error, results, fields) {
    //   if (error) {
    //       console.log(error);
    //   }
    //   console.log(results[0].uuid)
    //   console.log('req.body.key=' + req.body.key)

    //   if(results[0].uuid == req.body.key){
    //     sendSms({key : req.body.key,
    //       user_id : req.body.user_id, 
    //       sender : req.body.sender,
    //       receiver : req.body.receiver,
    //       msg : req.body.msg,
    //       testmode_yn : req.body.testmode_yn
    //     }).then((result) => {      
    //       console.log('전송결과', result);      
    //     }).catch(err => {          
    //       console.log('err', err);    
    //     })               
    //   }      
    // }) 
    // connection.end()
    

    // if(check_uuid == req.body.key){
    //   console.log('success')
    //   res.send({'status' : 200})
    // }
    // 알리고 인증키 : d7c5ijzh6o8wzfuryjk7m2w0uw19pmy9
    // sendSms({key : req.body.key, 
    //   user_id : req.body.user_id, 
    //   sender : req.body.sender,
    //   receiver : req.body.receiver,
    //   msg : req.body.msg,
    //   testmode_yn : req.body.testmode_yn
    // }).then((result) => {      
    //   console.log('전송결과', result);      
    //   res.send(result)
    // }).catch(err => {          
    //   console.log('err', err);    
    // })                
})

module.exports = router;

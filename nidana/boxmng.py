#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 Farmos, Inc.
# All right reserved.
#
# mate for interaction with farmos 

import json
import pymysql
import pandas
from datetime import datetime
from lib.dbmng import DBManager, MySQLManager

'''
option : {
    "db" : {"host" : "localhost", "user" : "root",
              "password" : "pass", "db" : "db"}
}
'''

class BoxManager(MySQLManager):
    def getmetadata(self):
        _QUERY = "select ifnull(d.field_id, df.field_id) fid, d.id, d.name, d.unit, d.sigdigit from dataindexes d left outer join device_field df on d.device_id = df.device_id order by fid desc, d.id asc"
        rows = self._select(_QUERY, [])
        mdic = {}
        fields = set()
        for row in rows:
            fields.add(row[0])
            if row[1] in mdic:
                mdic[row[1]]["fields"].append (row[0])
            else:
                mdic[row[1]] = {"name" : row[2], "unit": row[3], "sigdigit" : row[4], "fields" : [row[0]]}
        print(fields)
        fields.remove(0)
        fields.remove(None)
        print(fields)
        return mdic, fields

    def getdata(self, sdate):
        _QUERY = "select data_id, DATE_FORMAT(obs_time, '%%Y-%%m-%%d %%H:%%i'), nvalue from observations where DATE(obs_time) = %s order by data_id, obs_time" 
        return self._select(_QUERY, [sdate])

    def _adddatum(self, flddata, fld, datum):
        if (datum[0] < 10000000) or (datum[0] // 10000000 == 1 and 20 < datum[0] % 100 < 30):
            # farmos 관리 데이터 및 일간 통계치 정보는 전달하지 않는다.
            return flddata

        if datum[1] not in flddata[fld]:
            flddata[fld][datum[1]] = {"time" : datum[1]}
        flddata[fld][datum[1]][str(datum[0])] = datum[2]
        return flddata

    def merge(self, meta, flds, data):
        flddata = {}
        for fld in flds:
            flddata[fld] = {}
        print ("merge", len(data), flddata)
     
        for datum in data:
            if datum[0] in meta:
                item = meta[datum[0]]
            else:
                self._logger.warn("No meta for : " + str(datum))
                continue

            for fld in item["fields"]:
                if fld in flddata:    # field id is 0 / outside
                    flddata = self._adddatum(flddata, fld, datum)
                else:
                    for tmp in flds:
                        flddata = self._adddatum(flddata, tmp, datum)
        return flddata

    def makecsv(self, fld, data, sdate):
        df = pandas.DataFrame(data.values())
        df.set_index('time')
        df.sort_index(axis=1, inplace=True)
        col = df.pop('time')
        df.insert(0, 'time', col)
        #df = df.reindex(sorted(df.columns), axis=1)
        df.to_csv('data/' + str(sdate) + '-' + str(fld) + '.csv', encoding='utf-8', index=False)

    def dobackup(self, sdate):
        print ("started", datetime.now())
        meta,flds = self.getmetadata()
        data = self.getdata(sdate)
        print ("selected", datetime.now())
        merged = self.merge(meta, flds, data)
        print ("merged", datetime.now())

        for fld, flddata in merged.items():
            self.makecsv(fld, flddata, sdate)
            print ("csv", fld, datetime.now())

if __name__ == "__main__":
    import sys
    import time
    import logging
    from datetime import date, timedelta

    if len(sys.argv) > 2:
        print("usage: python3 boxmng.py [date yyyy-mm-dd]")
        sys.exit(2)

    if len(sys.argv) == 1:
        sdate = date.today() - timedelta(days=1)
    else:
        sdate = sys.argv[1]

    fp = open("conf/box.conf", 'r')
    opt = json.loads(fp.read())
    fp.close()

    bm = BoxManager(opt, logging)

    bm.connect()
    bm.dobackup(sdate)
    bm.close()


#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

from __future__ import print_function
from builtins import map
from builtins import object
import time
import math
import json
import subprocess32
import importlib
from datetime import datetime
from simpleeval import simple_eval

import util 
from code import RetCode, AlertCode, AlertGrade
from result import ProcResult, AlertResult
from rule import Rule
from variable import Variable

class Processor(object):
    """
    컨트롤러가 가지고있는 작업단위인 프로세서를 처리하기 위한 클래스

    프로세서간 데이터 활용을 위해서는 outputs 로 전달이 되어야 한다.
    values 로 전달이 되는 임시값은 단일 프로세서에서만 의미를 갖는다.
    """

    def __init__(self, logger):
        """
        Processor 의 Constructor
        :param logger: 로깅을 위한 로거.
        """
        self._logger = logger

    def evaluate(self, rule, proc, dbcur):
        """
        개별 processsor 를 평가하기위한 메소드
        :param rule: 룰 
        :param proc: 실행할 프로세서
        """
        pass

class ModuleProcessor(Processor):
    """
    기 구현된 모듈을 사용하여 평가하는 프로세서
    외부 모듈은 (RetCode, [result1, result2, ...]) 의 형식으로 리턴해야한다.
    result1, result2... 는 모듈을 실행한 결과물이고, 이는 순서대로 outputs에 매칭된다. 
    outputs에 매칭되지 않는 값들은 룰에서 사용되지는 않지만 모듈의 다음 계산을 위해 사용될 임시값으로 간주한다.
    """
    def __init__(self, logger):
        super(ModuleProcessor, self).__init__(logger)

    def evaluate(self, rule, proc, dbcur):
        (modname, funcname) = proc["mod"].split(".")

        mod = importlib.import_module("modules." + modname)
        func = getattr(mod, funcname)

        print("mod eval", rule.getinputs(), proc["pvalues"], dbcur)
        ret = func(rule.getinputs(), proc["pvalues"], dbcur)
        # ret = (retcode, values, period)
        if len(ret) == 3:
            return ProcResult(ret[0], proc, ret[1], period=ret[2])
        else:
            return ProcResult(ret[0], proc, ret[1])

class EquationProcessor(Processor):
    """
    단순한 수식을 평가하는 프로세서
    해킹 방지를 위해서 simple_eval을 사용함
    """
    def __init__(self, logger):
        super(EquationProcessor, self).__init__(logger)
        self._tmpdata = None

    def getschedindex(self, times):
        now = datetime.now()
        nsec = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
        print ("current nsec", nsec)
        for idx in range(len(times)):
            if times[idx] is None:
                break
            if 0 <= nsec - times[idx] < 60:
                print ("sched index", idx)
                return idx
        return None 

    def generatevariables(self, inputs, tsidx):
        names = {}
        for key, val in inputs.items():
            if key[0] == '#':
                tmp = key[1:].split(".")
            else:
                tmp = key.split(".")

            n = names
            l = len(tmp)
            for i in range(l):
                term = tmp[i]
                if i == l - 1:
                    n[term] = val.getvalue(tsidx)
                elif term not in n:
                    n.update({term : {}})
                n = n[term]

            print ("generated", key, names)
        return names

    def _evaluate(self, eq, names, func):
        try:
            return simple_eval(eq, names=names, functions=func)
        except Exception as ex:
            self._logger.warn ("Fail to evaluate : " + str(eq) + " : " + str(ex))
            return None

    def evaluate(self, rule, proc, dbcur):
        inputs = rule.getinputs() 
        if rule.isaschedulerule():
            # 스케쥴 룰이라면 table.time 이 꼭 있어야 함.
            sidx = self.getschedindex(inputs["#table.time"].getvalue())
            if sidx is not None:
                inputs["sidx"] = Variable(sidx)
            else:
                return ProcResult(RetCode.NOT_PROPER_TIME, proc, [])

        return self.noruleevaluate(inputs, rule.gettimespanindex(), proc)

    def noruleevaluate(self, inputs, tsidx, proc):
        names = self.generatevariables(inputs, tsidx)
        func = {"exp":math.exp, "log":math.log, "sin": math.sin, "cos": math.cos, "sqrt": math.sqrt, "tan":math.tan, "atan":math.atan}
        values = []
        if isinstance(proc["eq"], list):
            for eq in proc["eq"]:
                values.append (self._evaluate(eq, names, func))
        else:
            values.append (self._evaluate(proc["eq"], names, func))
        
        print(proc["eq"], "evaluated.", values)
        return ProcResult(RetCode.OK, proc, values)

#    def namehandler(self, node):
#        print("#" + node.id, self._tmpdata["#" + node.id].getvalue())
#        return self._tmpdata["#" + node.id].getvalue()

class ExternalProcessor(Processor): 
    """
    외부 프로세스를 실행하여 평가하는 프로세서
    외부 프로세스에 대한 최대 실행시간은 5초를 초과할 수 없다.
    외부 프로세스를 위해 이전 데이터를 전달하거나 하지는 않는다.
    """
    _TIMEOUT = 5
    def __init__(self, logger):
        super(ExternalProcessor, self).__init__(logger)

    def _makeargs(self, proc, data):
        args = [proc["cmd"]]
        for key in proc["args"]:
            if key in data:
                args.append(data[key].getvalue())
            else:
                args.append(key)
        return args

    def evaluate(self, rule, proc, dbcur):
        lines = subprocess32.check_output(self._makeargs(proc, rule.getinputs()), timeout=self._TIMEOUT, universal_newlines=True)
        values = list(map(float, lines.split("\n")))

        return ProcResult(RetCode.OK, proc, values)

class AIProcessor(Processor):
    def __init__(self, logger):
        super(AIProcessor, self).__init__(logger)

    def evaluate(self, rule, proc, dbcur):
        return ProcResult(RetCode.OK, proc, [])

class InternalProcessor(Processor):
    STATSMARGIN = 3 
    def __init__(self, logger):
        """
        Processor 의 Constructor
        :param logger: 로깅을 위한 로거.
        """
        self._logger = logger
        self._data = None
        self._dev = None
        self._timespan = None

    def setup(self, datamng, devmng, timespan):
        self._data = datamng
        self._dev = devmng
        self._timespan = timespan

    def evaluate(self, rule, proc, dbcur):
        """
        개별 processsor 를 평가하기위한 메소드
        :param rule: 룰
        :param proc: 실행할 프로세서
        :param dbcur: 디비 커서
        """
        if "devstatus" == proc["mod"]:
            return self.devstatus(rule, proc)
        elif "diskusage" == proc["mod"]:
            return self.diskusage(rule, proc)
        elif "datastats" == proc["mod"]:
            return self.datastatistics(rule, proc, dbcur)
        elif "isday" == proc["mod"]:
            return self.isday(rule, proc, dbcur)
        elif "devavail" == proc["mod"]:
            return self.devutil(rule, proc)
        return ProcResult(RetCode.OK, proc, [])

    def isday(self, rule, proc, dbcur):
        if rule.getfieldid(): 
            suntime = self._timespan.getsuntime(rule.getfieldid())
        else:
            suntime = self._timespan.getsuntime()
        sunrise = suntime.getsunrise()
        sunset = suntime.getsunset()
        nsec = self._timespan.getcurrentnsec()

        print ("isday", sunrise, sunset, nsec)

        if sunrise <= nsec <= sunset:
            return ProcResult(RetCode.OK, proc, [1, sunrise, sunset])
        else:
            return ProcResult(RetCode.OK, proc, [0, sunrise, sunset])

    def _makefromto(self, now, delta):
        _delta = timedelta(minutes = now.minute, seconds = now.second, microseconds = now.microsecond)
        totm = now - _delta
        fromtm = totm - delta
        return (fromtm, totm)

    def gettermfordatastats(self, term):
        now = datetime.now() 
        if term == 'hour':               #매 3분에 이전 시간대 통계를 계산
            if now.minute == InternalProcessor.STATSMARGIN:
                return self._makefromto(now, timedelta(hours = 1))

        elif term == 'daynnight':        #일출일몰 후 3분에 이전 시간대 통계를 계산
            suntime = self._timespan.getsuntime()
            sunrise = datetime.fromtimestamp(suntime.getsunrise())
            sunset = datetime.fromtimestamp(suntime.getsunset())
            nsec = self._timespan.getcurrentnsec()

            if 180 <= (now - sunrise).total_seconds() < 240:
                return (sunset - timedelta(days = 1), sunrise)
            elif 180 <= (now - sunset).total_seconds() < 240:
                return (sunrise, sunset)

        elif term == 'day':              #매일 0시 3분에 이전 시간대 통계를 계산
            if now.hour == 0 and now.minute == InternalProcessor.STATSMARGIN:
                return self._makefromto(now, timedelta(days = 1))

        elif term == 'week':             #매월요일 0시 3분에 이전 시간대 통계를 계산
            if now.hour == 0 and now.minute == InternalProcessor.STATSMARGIN and now.weekday() == 0:
                return self._makefromto(now, timedelta(days = 7))

        return (None, None)

    def datastatistics(self, rule, proc, dbcur):
        inputs = rule.getinputs()
        term = inputs["#term"].getvalue()
        fromtm, totm = self.gettermfordatastats(term)

        if fromtm is None or totm is None:
            self._logger.info("It's not a proper time to make data statistics.")
            return ProcResult(RetCode.OK, None, None)

        funcs = inputs["#funcs"].getvalue()
        dataid = inputs["#data"].getid()

        fns = []
        for fn in funcs.split(","):
            fns.append (fn + '(nvalue) as ' + fn)

        query = "select " + ",".join(fns) + " from observations where data_id = %s and obs_time between %s and %s"
        print ("query", query, [dataid, str(fromtm), str(totm)])

        try:
            dbcur.execute(query, [dataid, fromtm, totm])
            row = dbcur.fetchone()
            ret = []
            for out in proc["outputs"]:
                ret.append (row[out[1:]])
            print ("ret", ret)
            return ProcResult(RetCode.OK, proc, ret)

        except Exception as ex:
            self._logger.warn ("Fail to get data statistics : " + str(ex))
            self._logger.warn(traceback.format_exc())
            return ProcResult(RetCode.PROCESSOR_EXCEPTION, None, None)

    def devstatus(self, rule, proc):
        if self._data and self._dev:
            inputs = rule.getinputs()
            cnt = 0
            tm = datetime.now() - timedelta(seconds=inputs["#sec"].getvalue())
            for devid in self._dev.getdevices():
                if self._data.isnewdevstatus(devid, tm) == False:
                    cnt = cnt + 1

            if inputs["#min"].getvalue() <= cnt:
                alertid = inputs["#id"].getvalue()
                msg = inputs["#msg"].getvalue() + " (호스트명: " + socket.gethostname() + ", 이상장비수 : " + str(cnt) + ")"
                return ProcResult(RetCode.OK, proc, [CmdCode.ALERT.value, alertid, msg, inputs["#receiver"].getvalue()])
        return ProcResult(RetCode.OK, None, None)

    def devutil(self, rule, proc):
        if self._data and self._dev:
            tm = datetime.now()
            for devid in self._dev.getdevices():
                dataid = self._data.getdataidfordev(devid, 0) # 0은 장비 상태 - 데이터아이디관리체계 참조
                status = self._data.getdata(dataid)

                dataid = self._data.getdataidfordev(devid, 11) # 11은 장비 가동률 - 데이터아이디관리체계 참조
                utilization = self._data.getdata(dataid)

                if utilization is None or status is None:
                    self._logger.info("Device[" + str(devid) + "] does not have utilization or status.")
                    continue

                value = utilization.getvalue()
                if tm - status.getobserved() > timedelta(minutes=2):
                    value = 0 if value < 1 else value - 1
                    self._logger.info("Device[" + str(devid) + "_" + str(dataid) + "] is not updated yet. " + str(utilization.getobserved()))
                else:
                    value = 100 if value >= 99.9 else value + 0.3
                    self._logger.info("Device[" + str(devid) + "_" + str(dataid) + "] is updated. " + str(utilization.getobserved()))
                self._data.updatedata(dataid, value)
        return ProcResult(RetCode.OK, None, None)

    def diskusage(self, rule, proc):
        inputs = rule.getinputs()
        disk = psutil.disk_usage(inputs["#path"].getvalue())
        free = disk.free * 100.0 / disk.total
        print ("free disk", free)
        if inputs["#min"].getvalue() > free:
            alertid = inputs["#id"].getvalue()
            msg = inputs["#msg"].getvalue() + " (호스트명: " + socket.gethostname() + ", 남은공간 : " + format(free, ".2f") + "%)"
            return ProcResult(RetCode.OK, proc, [CmdCode.ALERT.value, alertid, msg, inputs["#receiver"].getvalue()])
        return ProcResult(RetCode.OK, None, None)

class DataCheckProcessor(Processor):
    _dcprocinfo = {
        "max" : {
            "func" : lambda info, val: info["value"] < val,
            "msgfmt": "{device} {data} 정상범위({crit}) 초과. 현재값 : {value:.2f}",
            "code" : AlertCode.FOS_ENV_DATAMAX

        },
        "min" : {
            "func" : lambda info, val: info["value"] > val,
            "msgfmt": "{device} {data} 정상범위({crit}) 미달. 현재값 : {value:.2f}",
            "code" : AlertCode.FOS_ENV_DATAMIN
        } 
    }

    def setup(self, datamng, devmng, timespan):
        self._data = datamng
        self._dev = devmng
        self._timespan = timespan

    def evaluate(self, rule, proc, dbcur):
        alerts = []
        for dataid in self._data.getdataids():
            data = self._data.getdata(dataid)
            if data is None:
                continue

            cinfo = data.getcriticalinfo()
            if cinfo is None:
                continue

            alerts.extend(self.checkcritical(data, cinfo))
        return ProcResult(RetCode.OK, alerts=alerts)

    def checkcritical(self, data, cinfo):
        results = []
        for crit in cinfo["info"]:
            if crit["type"] not in DataCheckProcessor._dcprocinfo:
                self._logger.warn("There is no data check processor for " + crit["type"])
                continue

            dcproc = DataCheckProcessor._dcprocinfo[crit["type"]] 
            if crit["used"] == 1 and dcproc["func"](crit, data.getvalue()) is True:
                devid = data.getmeta("device_id")
                info = {"data" : data.getmeta("name"), "device" : self._dev.getdevice(devid)["name"], "cinfo" : crit, "value" : data.getvalue(), "crit" : crit["value"]}
                msg = dcproc["msgfmt"].format(**info)
                info["msg"] = msg
                results.append(AlertResult(dcproc["code"], AlertGrade.MID_WARNING, json.dumps(info, ensure_ascii=False), data.getmeta("field_id"), None, devid))
        return results

if __name__ == '__main__':
    eqproc = EquationProcessor(util.getdefaultlogger())
    a = Variable(10)
    s = Variable(1)
    b = Variable(20)
    d = Variable([1, 2, 3, 4])
    print(eqproc.noruleevaluate({"#a.obs" : a, "#a.stat" : s, "#b.obs": b, "#d.d" : d}, 0, {"type" : "eq", "eq": "a.obs + b.obs + d.d[2]"})) 


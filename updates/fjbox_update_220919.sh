#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

cd $SHELL_PATH

mysql -u farmos -pfarmosv2@ farmos -e "ALTER TABLE core_rule_applied ADD \`desc\` TEXT CHARACTER SET utf8 AFTER mode;"
mysql -u farmos -pfarmosv2@ farmos -e "ALTER TABLE season CHANGE season_info season_info TEXT CHARACTER SET utf8;"
mysql -u farmos -pfarmosv2@ farmos -e "ALTER TABLE season ADD deleted INT(1) DEFAULT '0' AFTER name;"



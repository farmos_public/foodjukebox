#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`


cd /home/pi/farmosv2-script/backup/image/18


nvalue=$`mysql -u farmos -pfarmosv2@ farmos -e "select nvalue from current_observations where data_id = '100004'"`

plantingDay=${nvalue:7}
today=`date "+%s"`

diff=`expr $today - $plantingDay`
diffDay=`expr $diff / 84600`

convert -resize 25% -delay 30 -loop 0 `find -mtime -$diffDay -type f | shuf -n 20 | sort` ../animation.gif



#cat <(crontab -l) <(echo '0 1 * * * cd /home/pi/foodjukebox/scripts; ./makegifimage.sh') | crontab -


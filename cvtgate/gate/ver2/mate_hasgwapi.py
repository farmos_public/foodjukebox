#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import requests

from imports import *

from mate import Mate
from mate_httpget import HttpGetMate

'''
{
    "conn" : {
        "base" : "http://220.92.228.68:8080/api.php", 
        "params":{"token":"3dc5f1ca-4513-4752-a40a-67baeb084d0e"}
    }
}

'''

class HASGWApiMate(HttpGetMate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(HASGWApiMate, self).__init__(option, devinfo, coupleid, logger)
        self._node = self._devinfo.getgw()["children"][0]
   
    def sendobs(self):
        obsblk = Observation(self._node["id"])
        obsblk.setobservation(self._node["id"], 0, StatCode.READY)
        for key in list(self._lastres["data"].keys()):
            did = self._devinfo.findid(key, "sen", self._node["children"])
            obsblk.setobservation(did, self._lastres["data"][key], StatCode.READY)
        self.writecb(obsblk)


if __name__ == "__main__":
    option = {"conn" : {"base" : "http://220.92.228.68:8080/api.php", "params":{"token":"3dc5f1ca-4513-4752-a40a-67baeb084d0e"}}}
    devinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "11", "dk" : "", "dt": "nd", "children" : [{
                "id" : "12", "dk" : "air_temperature", "dt": "sen"
            }, {
                "id" : "14", "dk" : "relative_humidity", "dt": "sen"
            }, {
                "id" : "15", "dk" : "amount_of_rainfall", "dt": "sen"
            }, {
                "id" : "16", "dk" : "atmospheric_pressure", "dt": "sen"
            }, {
                "id" : "17", "dk" : "amount_of_solar_radiation", "dt": "sen"
            }, {
                "id" : "18", "dk" : "photosynthetically_active_radiation", "dt": "sen"
            }, {
                "id" : "19", "dk" : "carbon_dioxide", "dt": "sen"
            }, {
                "id" : "20", "dk" : "wind_speed", "dt": "sen"
            }, {
                "id" : "21", "dk" : "wind_direction", "dt": "sen"
            }]
        }]
    }]
    hget = HASGWApiMate(option, devinfo, "1", None)
    mate = Mate({}, [], "1", None)
    hget.start(mate.writeblk)
    time.sleep(100)
    hget.stop()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 Farmos, Inc.
# All right reserved.
#
# mate for interaction with farmos 

import json
import pprint
import pymysql
from datetime import datetime
from deepdiff import DeepDiff
from lib.dbmng import DBManager, MySQLManager

'''
option : {
    "db" : {"host" : "localhost", "user" : "root",
              "password" : "pass", "db" : "db"}
}
'''

class SyncManager(MySQLManager):
    TABLES = ["fields", "devices", "dataindexes", "core_timespan", "core_rule_applied"]

    def _farmselect(self, query, params, farmid):
        if farmid:
            params.append(farmid)
            rows = self._select(query + " and farm_id = %s", params)
        else:
            rows = self._select(query, params)

        cols = self.getcolumns()
        return dict(zip(cols, rows[0]))

    def _gettablefp(self, tname, farmid=None):
        query = "select min(id) as minid, max(id) as maxid, count(*) - sum(deleted) as cnt from " + tname + " where 1 = 1 "
        return self._farmselect(query, [], farmid)
        
    def getfingerprint(self, farmid=None):
        fp = {}
        for t in self.TABLES:
            fp[t] = self._gettablefp(t, farmid)
        return fp

class BoxManager(SyncManager):
    def _deleterow(self, table, row):
        if table == "core_timespan":
            query = "update " + table  + " set deleted = 1 where id = %s and field_id = %s"
            return self._execute(query, [row["id"], row["field_id"]])
        else:
            query = "update " + table  + " set deleted = 1 where id = %s"
            return self._execute(query, [row["id"]])

    def _deleterows(self, table, ids):
        query = "update " + table  + " set deleted = 1 where id not in %s"
        #print(query, [row["id"]])
        return self._execute(query, [ids])

    def _insupdrow(self, table, row):
        if table == "core_timespan":
            pkeys = ["id", "field_id"]
        else:
            pkeys = ["id"]
        keys = row.keys()
        cols = ", ".join(keys)
        vals = ", ".join(["%s"] * len(keys))
        params = [row[k] for k in keys]
        tmp = []
        for k in keys:
            if k not in pkeys:
                tmp.append(k + "=%s")
                params.append(row[k])
        ups = ", ".join(tmp)
        query = "insert into " + table + "(" + cols + ") values (" + vals + ") on duplicate key update " + ups
        #print (query, params)
        return self._execute(query, params)

    def _truncate(self, table):
        query = "truncate table " + table
        #print (query, [])
        return self._execute(query, [])

    def _insobs(self, row):
        query = "insert into current_observations (data_id, obs_time) values (%s, now())"
        return self._execute(query, [row["id"]])

    def updatemeta(self, meta):
        print (meta.keys())
        for t in self.TABLES:
            ids = []
            for row in meta[t]:
                if t != "fields":
                    del row["farm_id"]

                if "deleted" in row and row["deleted"] != 0:
                    self._deleterow(t, row)
                else:
                    self._insupdrow(t, row)

                ids.append (row["id"])
            self._deleterows(t, ids)

        # device_field
        self._truncate("device_field")
        for row in meta["device_field"]:
            del row["farm_id"]
            self._insupdrow("device_field", row)

        # current_observations by dataindexes
        for row in meta["dataindexes"]:
            if "deleted" in row and row["deleted"] == 0:
                self._insobs(row)

class DPManager(SyncManager):
    def loadmeta(self, farmid):
        meta = {}
        for t in self.TABLES + ["device_field"]:
            rets = self._select ("select * from " + t + " where farm_id = %s", [farmid])
            meta[t] = []
            cols = ["`desc`" if x == "desc" else x for x in self.getcolumns()]
            for ret in rets:
                meta[t].append(dict(zip(cols, ret)))
        return meta

class BoxSync:
    def __init__(self, boxopt, dpopt, logger):
        self._bm = BoxManager(boxopt, logger)
        self._dm = DPManager(dpopt, logger)

    def connect(self):
        self._bm.connect()
        self._dm.connect()

    def close(self):
        self._bm.close()
        self._dm.close()

    def compare(self, farmid):
        boxfp = self._bm.getfingerprint()
        dpfp = self._dm.getfingerprint(farmid)
        ret = DeepDiff(boxfp, dpfp, verbose_level=2)
        if ret:
            pprint.pprint(ret)
            return ret
        else:
            return None

    def sync(self, farmid):
        meta = self._dm.loadmeta(farmid)
        self._bm.updatemeta(meta)

if __name__ == "__main__":
    import sys
    import time
    import logging
    from datetime import date, timedelta

    if len(sys.argv) != 3:
        print("usage: python3 boxsync.py [comp|sync] farmid")
        sys.exit(2)

    fp = open("conf/box.conf", 'r')
    boxopt = json.loads(fp.read())
    fp.close()

    fp = open("conf/dp.conf", 'r')
    dpopt = json.loads(fp.read())
    fp.close()
    dpopt["db"] = dpopt["masternode"]

    bs = BoxSync(boxopt, dpopt, logging)

    bs.connect()
    if sys.argv[1] == 'comp':
        bs.compare(sys.argv[2])
    elif sys.argv[1] == 'sync':
        bs.sync(sys.argv[2])
    else:
        print("usage: python3 boxsync.py [comp|sync] farmid")

    bs.close()


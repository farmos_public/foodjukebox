import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isAuth: false
  },
  mutations: {
    isAuth (state, isAuth) {
      state.isAuth = isAuth
    }
  },
  getters: {
    getIsAuth: state => state.isAuth
  },
  plugins: [createPersistedState({
    key: ['isAuth']
  })]
})

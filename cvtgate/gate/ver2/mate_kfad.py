#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#
# KFAD driver for Haman smart greenhouse
#

import time
from mate import Mate
from mate_fixedlen import FixedLengthSerialMate

'''
option : {
    "conn" : {
        "port" : "/dev/ttyUSB0",
        "baudrate": 9600,
    },
    "msgfmt" : {
        "name" : "NUMON",
        "STX" : "str",
        "ETX" : None,
        "size" : 22,
        "ididx" : 1,
        "info" : [
            {"len" : 6, "scale" : None},
            {"len" : 3, "scale" : 0.1},
            {"len" : 3, "scale" : 0.1},
            {"len" : 3, "scale" : 1},
            {"len" : 4, "scale" : 1}
        ]
    }
}

devinfo : [
    {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "sen"},
        {"id" : "6", "dk" : "2", "dt": "sen"},
        {"id" : "7", "dk" : "3", "dt": "sen"}
    ]}
]
'''
class KFADMate(FixedLengthSerialMate):
    msgfmt = {
        "CTRL" : {
            "STX" : "str",
            "ETX" : None,
            "size" : 52,
            "ididx" : None,
            "info" : [
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 4, "scale" : 1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 4, "scale" : 0.01},
                {"len" : 4, "scale" : 1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 1},
                {"len" : 1, "scale" : 1}
            ]
        },
        "NUTRI" : {
            "STX" : "str",
            "ETX" : None,
            "size" : 16,
            "ididx" : None,
            "info" : [
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 4, "scale" : 1},
                {"len" : 3, "scale" : 1}
            ]
        },
        "NUMON" : {
            "STX" : "str",
            "ETX" : None,
            "size" : 28,
            "ididx" : 1,
            "info" : [
                {"len" : 6, "scale" : None},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 1},
                {"len" : 4, "scale" : 1},
                {"len" : 3, "scale" : 0.1}
            ]
        },
    }

    def __init__(self, option, devinfo, coupleid, logger):
        option["msgfmt"] = self.msgfmt[option["conn"]["type"]]
        super(KFADMate, self).__init__(option, devinfo, coupleid, logger)

if __name__ == "__main__":
    #option = {"conn" : {"port" : "/dev/ttyUSB0", "baudrate": 9600, "type":"CTRL"}}
    #devinfo = [{"id" : "1010", "dk" : "-1", "dt": "nd", "children" : [{"id" : "1001", "dk" : "0", "dt": "sen"}, {"id" : "1002", "dk" : "1", "dt": "sen"}, {"id" : "1004", "dk" : "2", "dt":"sen"}, {"id" : "1551", "dk" : "3", "dt": "sen"},{"id" : "1552", "dk" : "4", "dt": "sen"},{"id" : "1", "dk" : "5", "dt": "sen"},{"id" : "3", "dk" : "6", "dt": "sen"},{"id" : "4", "dk" : "7", "dt": "sen"}, {"id" : "5", "dk" : "8", "dt": "sen"},{"id" : "1632", "dk" : "9", "dt": "sen"},{"id" : "1642", "dk" : "10", "dt": "sen"},{"id" : "1662", "dk" : "11", "dt": "sen"},{"id" : "1672", "dk" : "12", "dt": "sen"},{"id" : "1612", "dk" : "13", "dt": "sen"},{"id" : "1622", "dk" : "14", "dt": "sen"},{"id" : "1651", "dk" : "15", "dt": "sen"}]}]

    #option = {"conn" : {"port" : "/dev/ttyKFCTRL", "baudrate": 9600, "type":"NUMON"}}
    #devinfo = [{"id" : "3", "dk" : "1", "dt": "nd", "children" : [{"id" : "4", "dk" : "0", "dt": "sen"}, {"id" : "5", "dk" : "1", "dt": "sen"}, {"id" : "6", "dk" : "2", "dt": "sen"}, {"id" : "7", "dk" : "3", "dt": "sen"}]}]

    #option = {"conn" : {"port" : "/dev/ttyKFNUTRI", "baudrate": 9600, "type":"NUTRI"}}
    #devinfo = [{"id" : "3", "dk" : "1", "dt": "nd", "children" : [{"id" : "4", "dk" : "0", "dt": "sen"}, {"id" : "5", "dk" : "1", "dt": "sen"}, {"id" : "6", "dk" : "2", "dt": "sen"}, {"id" : "7", "dk" : "3", "dt": "sen"}]}]

    option = {"conn" : {"port" : "/dev/ttyUSB0", "baudrate": 9600, "type":"NUMON"}}
    devinfo = [{"id" : "3", "dk" : "1", "dt": "nd", "children" : [{"id" : "4", "dk" : "2", "dt": "sen"}, {"id" : "5", "dk" : "3", "dt": "sen"}, {"id" : "6", "dk" : "4", "dt": "sen"}, {"id" : "7", "dk" : "5", "dt": "sen"}, {"id" : "8", "dk" : "6", "dt": "sen"}]}]

    mate = KFADMate(option, devinfo, None)
    mate2 = Mate({}, [], None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(10)
    mate.stop()
    print("mate stopped")


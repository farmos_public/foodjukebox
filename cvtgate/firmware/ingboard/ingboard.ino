/*
 ingboard.c
 인지시스템 2022년 신규보드 제어용.

 arduino-cli compile --fqbn arduino:avr:leonardo -u -p /dev/ttyACM0 /home/farmos/cvtgate/firmware/ingboard 
 arduino-cli compile --fqbn arduino:avr:uno -u -p /dev/ttyACM1 /home/farmos/cvtgate/firmware/ingboard 

 Copyright (c) 2022 FarmOS, Inc.
 All right reserved.
 */
#define MAXINBUF	16
#define MAXOUTBUF	110 	// 16 * 6 + 5(header) + numt * 2 + 1 (\n)

#define RCLKPIN		10
#define SRCLKPIN	11
#define SERPIN		12

#define LOADPIN		3
#define CLKENPIN	4
#define DINPIN		5
#define CLKPIN		6

#define MDCHPIN	    7
#define LIVEPIN	    13 

#define AUTO		1
#define MANUAL		0

#define MANUALON	30	// about 3 seconds
#define OUTPUTON	10	// about 1 second

#define MAXBOARD	8

#include <EEPROM.h>

byte writebuf[MAXINBUF];
byte readbuf[MAXINBUF]; 
// inbuf ==> return of Serial.readStringUntil
char inbuf[MAXOUTBUF];
char outbuf[MAXOUTBUF];
char lastbuf[MAXOUTBUF];

int mode = AUTO;	// default is auto
// 실제로 모드체인지는 사용하지 않음. 그래도 코드는 영향이 없어서 남겨두는 것으로 함.
int modechanged = 0;    // if mode is changed then 1
int debug = 0;		// debug mode is 0

byte numt = 1;		// number of toggle board
byte nums = 1;		// number of switch board
int numm = 0;		// number of manual user input on
int numl = 0;		// number of loop
int ledstatus = 0;

byte
togglepattern(char *pbuf) {
  byte ret = 0;
  for (int i = 0; i < 3; i++, pbuf++) {
    if (*pbuf == 'O')
      ret += 2;
    else if (*pbuf == 'C')
      ret += 1;
    else if (*pbuf == 'S')
      ret += 0;
    else
      return 255;
    if (i < 2)
      ret = ret << 2;
  }
  return ret << 2;
}

byte
switchpattern(char *pbuf) {
  byte ret = 0;
  for (int i = 0; i < 6; i++, pbuf++) {
    if (*pbuf == 'o')
      ret += 1;
    else if (*pbuf == 'f')
      ret += 0;
    else
      return 255;
    if (i < 5)
      ret = ret << 1;
  }
  return ret << 1;
}

// return number of bytes to write
// The number of Toggle boards should be under 10
// The number of Switch boards should be under 10
int
generateinput(char *pin, byte *pwrite) {
  int n = 0;
  int t = 0;
  int s = 0;
  int am = 0;
  char *pidx;

  if (pin[0] == 'A')
    am = 1;
  else if (pin[0] == 'M')
    am = 0;
  else
    return -1;

  if (pin[1] != 'T' || pin[3] != 'S')
    return -2;

  if (isDigit(pin[2]) && isDigit(pin[4])) {
    t = pin[2] - '0';
    s = pin[4] - '0';
  } else {
    return -3;
  }

  if (am == mode) {
    pidx = pin + 5;

    for (n = 2 * t + s - 1; n >= s; n--) {
      pwrite[n] = togglepattern(pidx);
      pwrite[n] += 3;		// set auto
      if (pwrite[n] == 255)
        return -4;
      pidx += 3;
    } 

    for (; n >= 0; n--) {
      pwrite[n] = switchpattern(pidx);
      pwrite[n] += 1 << 7;	// set auto
      if (pwrite[n] == 255)
        return -5;
      pidx += 6;
    }

/*
    if (t == 0)
      pwrite[0] += am << 6;
    else
      pwrite[1] += am;
*/
  }

  if (t != numt) {
    numt = t;
    EEPROM.write(0, numt);
  }

  if (s != nums) {
    nums = s;
    EEPROM.write(1, nums);
  }
  return 2 * t + s;
}

void
write595(int n, byte *pwrite) {
  if (debug) {
    Serial.print("Write 595 : ");
    Serial.println(n);
    for (int i = 0; i < n; i++) {
      Serial.println(pwrite[i], BIN);
    }
  }

  // ST_CP LOW 
  digitalWrite(RCLKPIN, LOW);
 
  // Shift out the bits
  for (; n > 0; n--, pwrite++) {
    shiftOut(SERPIN, SRCLKPIN, MSBFIRST, *pwrite);
  }

  // ST_CP HIGH 
  digitalWrite(RCLKPIN, HIGH);
}

void
read165(int n, byte *pread) {
  digitalWrite(CLKPIN, HIGH);

  digitalWrite(LOADPIN, LOW);
  delayMicroseconds(5);
  digitalWrite(LOADPIN, HIGH);
  delayMicroseconds(5);
 
  // Get data from 74HC165
  for (; n > 0; n--) {
    *pread = shiftIn(DINPIN, CLKPIN, MSBFIRST);
    pread++;
  }
}

int
filltoggle(byte *pread, char *pout, char *puserin) {
  byte front = ~(pread[0]);
  byte back = ~(pread[1]);

  if ((front & (1 << 7)) != 0) pout[0] = 'O';
  else if ((back & (1 << 3)) != 0) pout[0] = 'C';
  else pout[0] = 'S';

  if ((front & (1 << 6)) != 0) pout[1] = 'O';
  else if ((back & (1 << 2)) != 0) pout[1] = 'C';
  else pout[1] = 'S';

  if ((front & (1 << 5)) != 0) pout[2] = 'O';
  else if ((back & (1 << 1)) != 0) pout[2] = 'C';
  else pout[2] = 'S';

  if ((front & (1 << 4)) != 0) pout[3] = 'O';
  else if ((back & 1) != 0) pout[3] = 'C';
  else pout[3] = 'S';

  if ((front & 1) != 0) pout[4] = 'O';
  else if ((back & (1 << 4)) != 0) pout[4] = 'C';
  else pout[4] = 'S';

  if ((front & (1 << 1)) != 0) pout[5] = 'O';
  else if ((back & (1 << 5)) != 0) pout[5] = 'C';
  else pout[5] = 'S';

  if ((back & (1 << 7)) != 0) puserin[0] = 'T';
  else puserin[0] = 'F';

  if ((back & (1 << 6)) != 0) puserin[1] = 'T';
  else puserin[1] = 'F';

  return 0;
}

int
fillswitch(byte *pread, char *pout) {
  int k = 1 << 2;
  byte newr = ~(*pread);


  for (int i = 0; i < 6; i++) {
    if ((newr & k) != 0)
        pout[i] = 'o';
    else
        pout[i] = 'f';
    k = k << 1;
  }
  return 0;
}

void
filloutput(int n, byte *pread, int ret, char *pout) {
  int i;
  char userin[MAXBOARD*2];

  if (debug) {
    Serial.print("Read 165 : ");
    Serial.println(n);
    for (int i = 0; i < n; i++) {
      Serial.print(pread[i]);
      Serial.print(" ");
      Serial.println(pread[i], BIN);
    }
  }

  if (mode == AUTO)
    pout[0] = 'A';
  else
    pout[0] = 'M';

  pout[1] = 'T';
  pout[2] = numt + '0';
  pout[3] = 'S';
  pout[4] = nums + '0';

  pout += 5;

  for (i = 0; i < numt; i++) {
    filltoggle(pread, pout, userin + i * 2);
    pread += 2;
    pout += 6;
  }

  for (i = 0; i < nums; i++) {
    fillswitch(pread, pout);
    pread += 1;
    pout += 6;
  }

  for (i = 0; i < numt * 2; i++, pout++) {
    *pout = userin[i]; 
  }

  if (numt > 0) {
    if (userin[0] == 'T') {
      numm++;
    } else {
      numm = 0;
    }
  }
}

int
executecommand() {
  int ret;

  if (mode == AUTO) {
    ret = generateinput(inbuf, writebuf);
  } else {
    ret = generatemanual(writebuf);
  }

  if (modechanged) {
    digitalWrite(MDCHPIN, HIGH);
  }

  if (ret > 0) {
    write595(ret, writebuf); 
  }

  if (modechanged) {
    digitalWrite(MDCHPIN, LOW);
    modechanged = 0;
  }

  return ret;
}

int
generatemanual(byte *pwrite) {
  int n = numt * 2 + nums;

  for (int i = 0; i < n; i++) {
    pwrite[i] = 0;
  }

  return n;
} 

void
initinbuf() {
  char *p;
  int i;

  inbuf[0] = 'A';
  inbuf[1] = 'T';
  inbuf[2] = numt + '0';
  inbuf[3] = 'S';
  inbuf[4] = nums + '0';

  p = inbuf + 5;
  for (i = 0; i < numt * 6; i++, p++) {
    *p = 'S';
  }
  for (i = 0; i < nums * 6; i++, p++) {
    *p = 'f';
  }
}

//*/
void
setup() {
  Serial.begin(9600);
  while (!Serial) {
    ;
  }

  pinMode(LOADPIN, OUTPUT);
  pinMode(CLKENPIN, OUTPUT);
  pinMode(CLKPIN, OUTPUT);
  pinMode(DINPIN, INPUT);

  pinMode(MDCHPIN, OUTPUT);

  pinMode(RCLKPIN, OUTPUT);
  pinMode(SRCLKPIN, OUTPUT);
  pinMode(SERPIN, OUTPUT); 

  pinMode(LIVEPIN, OUTPUT);

  numt = EEPROM.read(0);
  nums = EEPROM.read(1);
  initinbuf();
}

void
loop() {
  int ret;

  if (Serial.available() > 0) {
    String instr = Serial.readStringUntil('\n');
    if (instr[0] == 'A') {
      mode = AUTO;
      modechanged = 1;
    }

    if (instr[0] == 'D') {
      debug = ~debug; 
      return;
    }

    if (debug) {
      Serial.print("mode : ");
      Serial.println(mode);
    }

    if (mode == AUTO) {
      strcpy(inbuf, instr.c_str());
      ret = executecommand();
    }
  } 

  if (numt + nums > 0) {
    int k = numt * 2 + nums;
    read165(k, readbuf);
    filloutput(k, readbuf, ret, outbuf);

    // for cvtgate or user
    if (strcmp(lastbuf, outbuf) != 0 || numl > OUTPUTON) { 
      strcpy(lastbuf, outbuf);
      Serial.println(lastbuf);
      numl = 0;
    }

    if (numm > MANUALON) {
	  mode = ~mode;
	  modechanged = 1;
	  /*
      if (mode == AUTO) {
        mode = MANUAL;
      } else {
        mode = AUTO;
      }
	  */
      numm = 0;
    }

    ret = executecommand();
  }

  numl++;
  ledstatus = ~ledstatus;
  digitalWrite(LIVEPIN, ledstatus);
  delay(100);
}


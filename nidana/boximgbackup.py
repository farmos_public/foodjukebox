#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.


from datetime import datetime, date, timedelta
import base64
import requests
import json
import os
import shutil
from pathlib import Path

def getAPI(curpath):
    with open(curpath+'/cvtgate/gate/conf/cpmng.conf', 'r') as f:
        cpmng_conf = json.load(f)

    return (cpmng_conf["url"].replace('gate','upload/')+cpmng_conf["id"])

def getPath():
    curpath = Path.cwd().parent
    return (curpath)

def backuplist(curpath):
    if os.path.exists(curpath+"/nidana/picture/"):
        shutil.rmtree(curpath+"/nidana/picture/")
    shutil.copytree(curpath+"/cvtgate/gate/picture", curpath+"/nidana/picture")
    path = curpath+"/nidana/picture"
    os.listdir(path)
    file_list = os.listdir(path)
    backupimg = [file for file in file_list if file.endswith('.jpg')]
    backupimg.sort()

    return (backupimg)

def uploadhttp(curpath,url, ftype, fname, extra = None):
    """ HTTP로 파일을 업로드 함. 백업파일 업로드 용이지만, 그외 사진 파일 같은 다른 파일 업로드도 가능. """
    try:
        fp = open(fname, "rb")
        fdata = fp.read()
        encoded = base64.b64encode(fdata).decode('utf8')
        fp.close()
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        data = {"type" : ftype, "data": encoded, "filename": fname, "meta" : extra}
        res = requests.post(url, headers=headers, data=json.dumps(data))
        if res.status_code == 200:
            os.remove(curpath+"/cvtgate/gate/"+fname)
            return True
        print("fail to upload. " + fname + " : " + str(res.status_code) + " " + str(res.reason))
        return False
    except Exception as ex:
        print("fail to upload. " + fname + " : " + str(ex))
        return False

def runBoxImage(extra):

    curpath = str(getPath())
    print (curpath, type(curpath))

    flist = backuplist(curpath)
    if not flist :
        exit()

    url = getAPI(curpath)
    ftype = "image"
    print (flist, url, ftype)

    for file in flist :
        dt_datetime = datetime.strptime(file[0:-4], '%Y%m%d-%H%M')
        str_datetime = datetime.strftime(dt_datetime, '%Y-%m-%d %H:%M:%S')
        extra["date"] = str_datetime
        fname = "picture/"+file
        print (fname, extra)
        uploadhttp(curpath,url, ftype, fname, extra)
    shutil.rmtree(curpath+"/nidana/picture/")


if __name__ == "__main__":

    extra = {
        "deviceId" : 18,
        "date" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        "meta" : None
    }

    runBoxImage(extra)




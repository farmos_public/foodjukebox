#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

echo -e '\n\n 1. UI update'

cd /home/pi/farmosv2-script

mv common_api/api/public/menu_m.json menu_m.json
mv common_api/api/public/menu_pc.json menu_pc.json

cd common_api; git pull origin master

cd ..; git clone https://gitlab.com/jinong/opensource_ui.git

cd opensource_ui; sudo ./install.sh
cd ..
sleep 2s

rm common_api/api/public/menu_m.json
rm common_api/api/public/menu_pc.json

sleep 2s

mv menu_m.json common_api/api/public/menu_m.json
mv menu_pc.json common_api/api/public/menu_pc.json


mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes (id,name,unit,sigdigit,field_id,deleted) values (100005,'종료일','date',0,1,0)";

cat /dev/null > /home/pi/foodjukebox/updates/log/temp

echo -e '\n\n 2. Set Up Crontab \n'

cat <(crontab -l) <(echo '30 1 * * * cd /home/pi/farmosv2-script/cvtgate/util; python3 daily_stats.py') | crontab -

echo -e '\n\n 3. Set Up DataBase \n'

mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) select 10000000 + d.id * 100 + 21, NULL, '일간 개수', NULL, 0, d.id, f.field_id, 0 from devices d, device_field f where d.id = f.device_id and d.spec like '%\"Class\":\"sensor\"%' and d.deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) select 10000000 + d.id * 100 + 22, NULL, '일간 평균', NULL, 0, d.id, f.field_id, 0 from devices d, device_field f where d.id = f.device_id and d.spec like '%\"Class\":\"sensor\"%' and d.deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) select 10000000 + d.id * 100 + 23, NULL, '일간 최고', NULL, 0, d.id, f.field_id, 0 from devices d, device_field f where d.id = f.device_id and d.spec like '%\"Class\":\"sensor\"%' and d.deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) select 10000000 + d.id * 100 + 24, NULL, '일간 최저', NULL, 0, d.id, f.field_id, 0 from devices d, device_field f where d.id = f.device_id and d.spec like '%\"Class\":\"sensor\"%' and d.deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) select 10000000 + d.id * 100 + 25, NULL, '일간 표준편차', NULL, 0, d.id, f.field_id, 0 from devices d, device_field f where d.id = f.device_id and d.spec like '%\"Class\":\"sensor\"%' and d.deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) select 10000000 + d.id * 100 + 26, NULL, '일간 변동편차', NULL, 0, d.id, f.field_id, 0 from devices d, device_field f where d.id = f.device_id and d.spec like '%\"Class\":\"sensor\"%' and d.deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 10000000 + id * 100 + 21, now(), NULL, now(), NULL from devices where spec like '%\"Class\":\"sensor\"%' and deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 10000000 + id * 100 + 22, now(), NULL, now(), NULL from devices where spec like '%\"Class\":\"sensor\"%' and deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 10000000 + id * 100 + 23, now(), NULL, now(), NULL from devices where spec like '%\"Class\":\"sensor\"%' and deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 10000000 + id * 100 + 24, now(), NULL, now(), NULL from devices where spec like '%\"Class\":\"sensor\"%' and deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 10000000 + id * 100 + 25, now(), NULL, now(), NULL from devices where spec like '%\"Class\":\"sensor\"%' and deleted = 0";
mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 10000000 + id * 100 + 26, now(), NULL, now(), NULL from devices where spec like '%\"Class\":\"sensor\"%' and deleted = 0";


mysql -u farmos -pfarmosv2@ farmos -e "delete from requests";
mysql -u farmos -pfarmosv2@ farmos -e "alter table requests add primary key (opid, device_id, senttime)";


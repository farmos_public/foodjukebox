#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`


cd /home/fjbox/backup/image

deviceList=`ls`


for deviceSerial in $deviceList
do
    echo -e $deviceSerial
    cd /home/fjbox/backup/image/$deviceSerial/18


    numofPhoto=`find . -type f | wc -l`

    echo -e $numofPhoto

    if [[ $numofPhoto -ge 500 ]]; then
        find . -name '*.jpg' -mtime +40 -delete
    fi

    numofPhoto=`find . -type f | wc -l`

    echo -e $numofPhoto

    if [[ $numofPhoto -gt 500 ]]; then
        exceededPhoto=`expr $numofPhoto - 500`
        ls -td1 *.jpg | tail -n $exceededPhoto | xargs rm -f
    fi


done




#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

cd $SHELL_PATH

./uiupdate.sh

mysql -u farmos -pfarmosv2@ farmos -e "CREATE TABLE season (  season_id int(11) NOT NULL AUTO_INCREMENT,  field_id int(11) NOT NULL,  start_date datetime NOT NULL,  end_date datetime DEFAULT NULL,  season_info int(11) DEFAULT NULL,  name varchar(50) DEFAULT NULL,  PRIMARY KEY (season_id,field_id) ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;"
mysql -u farmos -pfarmosv2@ farmos -e "CREATE TABLE alert (  id int(11) NOT NULL AUTO_INCREMENT,  farm_id int(11) NOT NULL,  occured_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,  hwid int(11) DEFAULT NULL,  alert_code int(11) NOT NULL,  device_id int(11) DEFAULT NULL,  field_id int(11) DEFAULT NULL,  grade int(11) DEFAULT NULL,  info text,  mute_time datetime DEFAULT NULL,  occured_count int(11) DEFAULT '1',  PRIMARY KEY (id,farm_id) ) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;"
mysql -u farmos -pfarmosv2@ farmos -e "CREATE TABLE alert_delivery (  id int(11) NOT NULL,  alert_id varchar(250) NOT NULL DEFAULT '',  delivery_method varchar(50) NOT NULL,  delivery_time datetime NOT NULL,  result int(11) DEFAULT NULL,  user_id int(11) NOT NULL,  farm_id int(11) DEFAULT NULL,  msg text NOT NULL,  PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
mysql -u farmos -pfarmosv2@ farmos -e "ALTER TABLE farmos_user ADD alarm_configuration TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql -u farmos -pfarmosv2@ farmos -e "ALTER TABLE devices ADD configuration TEXT CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql -u farmos -pfarmosv2@ farmos -e "ALTER TABLE dataindexes ADD critical_info TEXT CHARACTER SET utf8 COLLATE utf8_general_ci"


cd $SHELL_PATH
cd ../..

sudo pip3 install pandas

cat <(crontab -l) <(echo '20 0 * * * cd /home/pi/nidana; python3 boxmng.py') | crontab

sed -i '/복제/d' /home/pi/farmosv2-script/updates/log/temp


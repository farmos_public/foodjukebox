#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc. All right reserved.
#
# PHSENSOR driver for KIST
#

import time
import spidev, time

from imports import *
from dinfo import DevInfo
from mate import Mate, ThreadMate, DevType

''' 
option : { 
    "sleep": {
        "time": 3,
        "obs": 5,
        "noti": 2
    },
    "calibration": {
        "3": {
            "type": "linear",
            "args": {
                "a": 2.91,
                "b": 3.95
            }
        }
    }
}

devinfo = [{
    "id" : "1", "dk" : "", "dt": "gw", "children" : [{
        "id" : "101", "dk" : '', "dt": "nd", "children" : [
            {"id" : "102", "dk" : '0', "dt": "sen"}
        ]
    }]
}]

'''

class PHSENSORMate(ThreadMate):
    def analog_read(self, channel):
        spi = spidev.SpiDev()
        spi.open(0,0)
        spi.max_speed_hz = 1000000

        values = []
        for i in range(10):
            r = spi.xfer2([1, (8 + channel) << 4, 0])
            values.append(((r[1]&3) << 8) + r[2])
        spi.close()
        return values

    def readmsg(self):
        readinglist = self.analog_read(0)
        readinglist.sort()
        temp = readinglist[2:8]
        obs = sum(temp)/len(temp)
        print("obs", obs)

        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        dev = nd["children"][0]

        obsblk = Observation(nd["id"])
        obsblk.setobservation(nd["id"], 0, StatCode.READY)
        obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)
        self._msgq.append(obsblk)


if __name__ == "__main__":
    opt = {
      "calibration": {
        "3": {
          "type": "linear",
          "args": {
            "a": 0.0094,
            "b": 3.95
          }
        }
      }
    }

    devinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : '0', "dt": "sen"}
            ]
        }]
    }]

    mate = PHSENSORMate(opt,devinfo,"1", None)
    mate2 = Mate(opt, devinfo, "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(10)
    mate.stop()
    print("mate stopped")


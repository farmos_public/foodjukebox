from collections import defaultdict
import requests
import json
import struct
import time
import traceback
from datetime import datetime
from decimal import *
import datetime
from .. import *


class IOCROPSMate(DSMate):
    def timeset(self):
        now = datetime.datetime.now()
        past = datetime.datetime.now() - datetime.timedelta(minutes=5)
        nowtime = now.strftime('%Y-%m-%d %H:%M:%S').split(' ')
        pasttime = past.strftime('%Y-%m-%d %H:%M:%S').split(' ')
        time_data = {"now":nowtime, "past":pasttime}
        return time_data

    def loaddata(self):
        copt = self._option["urlkey"]
        _time_data = self.timeset()
        url = "https://openapi.iofarm.com/chart?startDate={}T{}&endDate={}T{}&macAddress={}&timeInterval={}".format(_time_data["past"][0],_time_data["past"][1],_time_data["now"][0],_time_data["now"][1],copt["macip"],copt["timeInterval"])
        header = {"x-api-key":"{}".format(copt["header"])}
        response = requests.get(url=url,headers=header)
        if response.status_code == 200:
            data = json.loads(response.text)
        else:
            self._logger.error("error loaddata" + response.status_code)
        return data

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return
        self._updatetime(Mate.OBSTYPE)
        copt = self._option["urlkey"]
        data = self.loaddata()
        serial_data = data["node."+copt["node_serial"]][-1]
        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])

        for dev in nd["children"]:
            if dev["dk"] in serial_data:
                obs = serial_data[dev["dk"]]
                obsblk.setobservation(dev["id"], self.getvalue(dev["id"], float(obs)), StatCode.READY)
                print(obs)
        self._writeobs(obsblk)


if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "urlkey" : {
            "macip":"00:00:00:00:00:00",
            "timeInterval":"5m",
            "header":"EtxCUhioFxHKrKTixC4U7dIJF3EluK094xDm33d8",
            "node_serial":"0429211012",
        }
    }


    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id": "4", "dk": "RT_root_loadcell", "dt": "sen"},
                {"id": "5", "dk": "RT_soil_WC", "dt": "sen"},
                {"id": "6", "dk": "RT_soil_EC", "dt": "sen"},
                {"id": "7", "dk": "RT_soil_temp", "dt": "sen"},
                {"id": "8", "dk": "RT_soil_dp", "dt": "sen"},
                {"id": "9", "dk": "RT_soil_bulk_EC", "dt": "sen"},
                {"id": "10", "dk": "RT_water_out", "dt": "sen"},
                {"id": "11", "dk": "RT_drain_rate", "dt": "sen"},
                {"id": "12", "dk": "RT_temp", "dt": "sen"},
                {"id": "13", "dk": "RT_hum", "dt": "sen"},
                {"id": "14", "dk": "RT_CO2", "dt": "sen"},
                {"id": "15", "dk": "RT_radiation", "dt": "sen"}

            ]
        }]
    }]



    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = IOCROPSMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

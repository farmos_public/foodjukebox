# nidana

FarmOS Data 관리를 위한 프로젝트입니다.

nidana 는 불교용어로 인연. 인연이 있으면 데이터가 잘 모이겠지.....라는 마음으로 만들어봅니다.


## Installation

### python packages

* pandas
* deepdiff

## Box

### boxmng

다음과 같이 실행하면 어제 데이터를 구역별로 csv로 만들어서 data 폴더에 넣습니다.

conf/box.conf 설정파일을 사용합니다. 

```
python3 boxmng.py 
```

특정한 날을 지정해서 실행도 가능합니다. 

```
python3 boxmng.py 2022-04-14
```

### boxsync

디피 마스터노드에 있는 메타데이터와 FOSBox 에 있는 메타데이터를 비교하고, 동기화 합니다. 
conf/box.conf 와 conf/dp.conf 설정파일을 사용합니다. 

```
python3 boxsync.py comp|sync farmid
```
형식으로 comp 를 사용하면 비교를 수행하고, sync 를 사용하면 동기화를 수행합니다.


### boximgbackup

FOSBox 에서 전송되지 않은 이미지를 재전송합니다. 별도의 설정파일을 사용하지 않고, 소스에서 설정합니다. 

## DP

### Data Sync

다음과 같이 실행하면 data 폴더에 있는 모든 csv파일을 순차적으로 디비에 적재합니다.  적재된 파일은 trash 폴더로 이동합니다. 새로운 백업파일을 만들어서 backup 폴더에 저장합니다.  파일당 실행시간은 약 5분 정도이지만 데이터 량과 서버 상태에 따라 매우 가변적입니다. 
conf/dp.conf 설정파일을 사용합니다. 

```
python3 dpmng.py sync
```

data 폴더와 backup 폴더에는 관리하는 농장 아이디 폴더가 있어야 합니다.

### Data Set Maker

아직 개발되지 않았습니다.



#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import os, glob, sys, time
from datetime import datetime
import requests
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008
import Adafruit_DHT as DHT
import threading
from threading import Thread
from time import sleep
import RPi.GPIO as GPIO, time
import RPi.GPIO as gpio

from imports import *
from mate import Mate, ThreadMate, DevType

'''
option : {
    "conn" : {
    },
    "location" : "chungmae01"
}

children: [
    {"id" : "1", "dk" : "chungmae01", "dt" : "nd", "children" : [
        {"id" : "11", "dk" : "0", "dt" : "sen"},
        {"id" : "12", "dk" : "1", "dt" : "sen"},
        {"id" : "13", "dk" : "2", "dt" : "sen"},
        {"id" : "14", "dk" : "3", "dt" : "sen"},
        {"id" : "15", "dk" : "4", "dt" : "sen"},
        {"id" : "16", "dk" : "5", "dt" : "sen"},
        {"id" : "17", "dk" : "6", "dt" : "sen"}
    ]}
]

'''

# initialize
SPI_PORT   = 0
SPI_DEVICE = 0
day = datetime.now().day
count = 0

def edgecounter(ch):
    global count
    count = count + 1

class SCNUPlumMate(ThreadMate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(SCNUPlumMate, self).__init__(option, devinfo, coupleid, logger)
        self._mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))
        self._obs = [0] * 7
        self._stat = [StatCode.READY] * 7
        os.system('modprobe w1-gpio')
        if os.system('modprobe w1-therm') != 0:
            self._devfile = None
            self._logger.warn("There is a problem in ground temperature sensor.")
        else:
            self._devfile = glob.glob("/sys/bus/w1/devices/28*/w1_slave")[0]


        GPIO.setmode(GPIO.BCM)
        GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(26, GPIO.RISING, callback=edgecounter) 

        self._lastlog = {}

    def start(self, writecb):
        self._logger.info("SCNUPlumMate was started.")
        return super(SCNUPlumMate, self).start(writecb)

    def stop(self):
        super(SCNUPlumMate, self).stop()
        self._logger.info("SCNUPlumMate was stopped.")
        return True

    def setobs(self, idx, value):
        if value is None:
            self._stat[idx] = StatCode.ERROR
        else:
            self._stat[idx] = StatCode.READY
            self._obs[idx] = value

    def readtempfromdevfile(self):
        if self._devfile is None:
            self._devfile = glob.glob("/sys/bus/w1/devices/28*/w1_slave")[0]
            if self._devfile is None:
                return None

        for i in range(5):
            fp = open(self._devfile, "r")
            lines = fp.readlines()
            fp.close()
            if lines[0].split(" ")[-1][:3] == "YES":
                return float(lines[1].split("=")[-1])/1000
            self._logger.info("retry to read temperature from device file.")
            time.sleep(0.5)
        self._logger.warn("fail to read temperature from device file.")
        return None

    def resetcounter(self):
        global count
        global day
        if day != datetime.now().day:
            day = datetime.now().day
            count = 0

    def readobservations(self):
        hum, temp = DHT.read_retry(22, 25)
        self.setobs(0, hum)
        self.setobs(1, temp)
        temp = self.readtempfromdevfile()
        self.setobs(2, temp)
        for i in [3, 4, 5]:
            temp = self._mcp.read_adc(i)
            self.setobs(i, temp)

    def wrapblk(self):
        try:
            dt = datetime.now()
            node = self._devinfo.getgw()["children"][0]
            obs = Observation(node["id"])
            for i in range(len(self._obs)):
                sid = self._devinfo.findid(i, DevType.SENSOR, node["children"])
                if sid is None:
                    self._logger.warn("fail to find id. " + str(i))
                    continue
                obs.setobservation(sid, self.getvalue(sid, self._obs[i]), self._stat[i])

            self._logger.info("observation: " + obs.stringify())
            return obs
        except Exception as ex:
            self._logger.warn("fail to wrap. " + str(ex))
            return None

    def readmsg(self):
        pass

    def sendobs(self):
        self.readobservations()
        self.writecb(self.wrapblk())

if __name__ == "__main__":
    option = {
        "conn": {
            "sleep" : 600,
            "location" : "chungmae01"
        },
        "calibration": {
            "11": {"type": "no", "args": None},
            "12": {"type": "no", "args": None},
            "13": {"type": "no", "args": None},
            "14": {"type": "no", "args": None},
            "15": {"type": "no", "args": None},
            "16": {"type": "no", "args": None}
        }
    }

    devinfo = [
        {"id" : "1", "dk" : "chungmae01", "dt" : "nd", "children" : [
            {"id" : "11", "dk" : "0", "dt" : "sen"},
            {"id" : "12", "dk" : "1", "dt" : "sen"},
            {"id" : "13", "dk" : "2", "dt" : "sen"},
            {"id" : "14", "dk" : "3", "dt" : "sen"},
            {"id" : "15", "dk" : "4", "dt" : "sen"},
            {"id" : "16", "dk" : "5", "dt" : "sen"}
        ]}
    ]

    mate = SCNUPlumMate(option, devinfo, None)
    mate2 = Mate({}, [], None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(10)
    mate.stop()
    print("mate stopped")

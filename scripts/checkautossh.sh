#!/bin/bash

curl -I http://fjboxtest.jinong.co.kr:8073

if [ $? -eq 0 ]
then
    echo "No problems"
else
    echo "Restart autossh"
    pkill -ef fjboxtest.jinong.co.kr
    autossh -N -f -M 20438 -R 2073:localhost:22 fjbox@fjboxtest.jinong.co.kr
    autossh -N -f -M 20440 -R 0.0.0.0:8073:localhost:8081 fjbox@fjboxtest.jinong.co.kr
    autossh -N -f -M 20442 -R 0.0.0.0:9073:localhost:9001 fjbox@fjboxtest.jinong.co.kr
fi

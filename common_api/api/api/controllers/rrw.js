/**
 * @fileoverview farmos api for farmos
 * @author joonyong.jinong@gmail.com
 * @version 1.0.0
 * @since 2017.07.25
 */

/* jshint esversion: 6 */

/**
 * notice
 * swagger-node 에서 controller 에 대한 initalize 와 finalize를
 * 별도로 지원하지 않는것으로 보인다.
 * farmos.js 의 경우 initialize 는 여러번 호출해도 상관없기 때문에
 * 모든 작업 시작전에 initialize 를 호출한다.
 * finalize 의 경우 디비와의 연결을 종료하는 것인데, 프로세스 종료시
 * 자동으로 될 것으로 간주한다.
 **/
// var jsonfile = require('jsonfile');
// var conffile = '../../common_api/conf/hasg.json';
// var _config = jsonfile.readFileSync(conffile);

// var configfile = __dirname + '/farmos-server.ini';

// var rrw = require('rrw.js')(_config);
// var _modulename = 'rrw api for farmos';
// var fs = require("fs");

var rrw = require('rrw')
var _modulename = 'rrw api for farmos'

var rrwApi = function () {
  /**
     * @method getAllMashingInfo
     * @description 막걸리 전체 담금 보고서 정보를 조회한다
     */
  var getAllMashingInfo = async function (req, res) {
    console.log(_modulename, 'getAllMashingInfo')
    try {
      const result = await rrw.getAllMashingInfo()
      result.length > 0 ? res.json(result) : res.json({})
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method setMashingBasicInfo
     * @description 막걸리 담금 보고서 기본정보를 저장한다
     */
  var setMashingBasicInfo = async function (req, res) {
    console.log(_modulename, 'setMashingBasicInfo')
    var basicInfo = req.swagger.params.body.value
    try {
      await rrw.setMashingBasicInfo(basicInfo)
      await rrw.setEpochTime(basicInfo)
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMashingInfo
     * @description 막걸리 개별 담금 보고서 정보를 조회한다
     */
  var getMashingInfo = async function (req, res) {
    console.log(_modulename, 'getMashingInfo')
    var mashingId = req.swagger.params.mashing_id.value
    var obj = {}
    var elementObj = {}

    try {
      const result1 = await rrw.getMashingInfo(mashingId)
      const result2 = await rrw.getTankList(mashingId)
      const result3 = await rrw.getMoringTemp(mashingId)
      const result4 = await rrw.getAfterNoonTemp(mashingId)
      const result5 = await rrw.getWaterTemp(mashingId)
      const result6 = await rrw.getManulaTemp(mashingId)
      const result7 = await rrw.getAci(mashingId)
      const result8 = await rrw.getAlc(mashingId)
      const result9 = await rrw.getBrix(mashingId)
      const result10 = await rrw.getInput(mashingId)
      const result11 = await rrw.getInput2(mashingId)

      obj.mashing_id = result1[0].mashing_id
      obj.start_dt = result1[0].start_dt
      obj.end_dt = result1[0].end_dt
      obj.name = result1[0].name
      obj.step_day = result1[0].step_day
      obj.tank_list = result2
      obj.moring_temp_list = result3
      obj.afternoon_temp_list = result4
      obj.water_temp_list = result5

      elementObj.manual_temp_list = result6
      elementObj.aci_list = result7
      elementObj.alc_list = result8
      elementObj.brix_list = result9
      obj.element_list = elementObj

      if (result10.length !== 0) {
        obj.input_list = result10
      } else {
        obj.input_list = result11
      }

      res.json(obj)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMashingChartInfo
     * @description 막걸리 개별 담금 보고서 차트 정보를 조회한다
     */
  var getMashingChartInfo = async function (req, res) {
    console.log(_modulename, 'getMashingChartInfo')
    var mashingId = req.swagger.params.mashing_id.value
    var obj = {}
    try {
      const result1 = await rrw.getMashingInfo(mashingId)
      const result2 = await rrw.getTankList(mashingId)
      const result3 = await rrw.getMashingChartInfo(mashingId)

      obj.mashing_id = result1[0].mashing_id
      obj.start_dt = result1[0].start_dt
      obj.end_dt = result1[0].end_dt
      obj.name = result1[0].name
      obj.tank_list = result2
      obj.temp_list = result3

      res.json(obj)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method setMashingInfo
     * @description 막걸리 개별 담금 보고서 차트 정보를 저장한다
     */
  var setMashingInfo = async function (req, res) {
    console.log(_modulename, 'setMashingInfo')
    var mashingId = req.swagger.params.mashing_id.value
    var setInfo = req.swagger.params.body.value

    try {
      await rrw.setMoringTemp(setInfo)
      await rrw.setAfterNoonTemp(setInfo)
      await rrw.setWaterTemp(setInfo)
      await rrw.setManulaTemp(setInfo)
      await rrw.setAci(setInfo)
      await rrw.setAlc(setInfo)
      await rrw.setBrix(setInfo)
      await rrw.setInput(mashingId, setInfo)
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMashingStepInfo
     * @description 막걸리 담금단계를 조회한다(기준 - 오늘날짜)
     */
  var getMashingStepInfo = async function (req, res) {
    console.log(_modulename, 'getMashingStepInfo')
    try {
      const result = await rrw.getMashingStepInfo()
      result.length > 0 ? res.json(result) : res.json({})
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMashingCurrentObservation
     * @description 막걸리 담금탱크 최근 데이터를 가져온다
     */
  var getMashingCurrentObservation = async function (req, res) {
    console.log(_modulename, 'getMashingCurrentObservation')
    try {
      const result = await rrw.getMashingCurrentObservation()
      result.length > 0 ? res.json(result) : res.json({})
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMashingDashboardChartInfo
     * @description 막걸리 개별 담금 보고서 대시보드 차트 정보를 조회한다
     */
  var getMashingDashboardChartInfo = async function (req, res) {
    console.log(_modulename, 'getMashingDashboardChartInfo')
    var mashingId = req.swagger.params.mashing_id.value
    var obj = {}
    try {
      const result1 = await rrw.getMashingInfo(mashingId)
      const result2 = await rrw.getTankList(mashingId)
      const result3 = await rrw.getDashboardTempChartInfo(mashingId)
      const result4 = await rrw.getDashboardAlcholeChartInfo(mashingId)
      const result5 = await rrw.getDashboardPhChartInfo(mashingId)

      obj.mashing_id = result1[0].mashing_id
      obj.start_dt = result1[0].start_dt
      obj.end_dt = result1[0].end_dt
      obj.name = result1[0].name
      obj.tank_list = result2
      obj.temp_list = result3
      obj.alchole_list = result4
      obj.ph_list = result5

      res.json(obj)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method deleteMashingInfo
     * @description 막걸리 개별 담금 보고서정보를 삭제한다
     */
  var deleteMashingInfo = async function (req, res) {
    console.log(_modulename, 'deleteMashingInfo')
    var mashingId = req.swagger.params.mashing_id.value
    var basicInfo = req.swagger.params.body.value
    try {
      if (basicInfo.mashing_id === 0) {
        await rrw.deleteMashing(mashingId)
        await rrw.deleteMashingMap(mashingId)
        await rrw.deleteEpochTime(basicInfo)
      }

      if (basicInfo.mashing_id !== 0) {
        console.log('basic_info.mashing_id=' + basicInfo.mashing_id)
        console.log('basic_info.mashing_id=' + basicInfo.company)
        await rrw.deleteMorningTemp(basicInfo.start_dt, basicInfo.end_dt, basicInfo.company)
        await rrw.deleteAfternoonTemp(basicInfo.start_dt, basicInfo.end_dt, basicInfo.company)
      }

      await rrw.deleteWaterInfo(basicInfo.tank_list, basicInfo.start_dt, basicInfo.end_dt)
      await rrw.deleteManualTempInfo(basicInfo.tank_list, basicInfo.start_dt, basicInfo.end_dt)
      await rrw.deleteAciInfo(basicInfo.tank_list, basicInfo.start_dt, basicInfo.end_dt)
      await rrw.deleteAlcholeInfo(basicInfo.tank_list, basicInfo.start_dt, basicInfo.end_dt)
      await rrw.deleteBrixInfo(basicInfo.tank_list, basicInfo.start_dt, basicInfo.end_dt)
      await rrw.deleteKojiInfo(mashingId)
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMashingSetting
     * @description 막걸리 담금 자동제어 셋팅정보를 조회한다.
     */
  var getMashingSetting = async function (req, res) {
    console.log(_modulename, 'getMashingSetting')
    var id = req.swagger.params.id.value
    var gubun = req.swagger.params.gubun.value
    const configList = []
    const stirringList = []
    const mashingList = []

    try {
      if (gubun === 0) { // 고급, 탱크
        const result = await rrw.getMashingSetting1(id)
        result.length > 0 ? res.json(result) : res.json({})
      } else { // 담금
        const resultId = await rrw.getMashingSetting2(id)
        for (var i = 0; i < resultId.length; i++) {
          const result = await rrw.getMashingSetting1(resultId[i].field_id)
          for (var ii in result) {
            if (result[ii].name.indexOf('교반') !== -1) {
              stirringList.push(result[ii].configurations)
            } else {
              configList.push(result[ii].configurations)
            }
          }
        }

        let gubun = 'same'
        let mashingConf = ''
        let stirringConf = ''

        if (configList.length === 1) {
          mashingConf = stirringList
          stirringConf = configList

          var temp1 = {}
          temp1.configurations = mashingConf

          var temp2 = {}
          temp2.configurations = stirringConf

          mashingList.push('same')
          mashingList.push(resultId)
          mashingList.push(temp1)
          mashingList.push(temp2)

          mashingList.push(JSON.parse(stirringConf))
          res.json(mashingList)
        } else if (configList.length > 1) {
          for (var j = 0; j < configList.length; j++) {
            for (var jj = 0; jj < configList.length; jj++) {
              if (configList[j] !== configList[jj]) {
                gubun = 'different'
              } else {
                mashingConf = configList[jj]
              }
            }
          }

          for (var k = 0; k < stirringList.length; k++) {
            for (var kk = 0; kk < stirringList.length; kk++) {
              if (stirringList[k] !== stirringList[kk]) {
                gubun = 'different'
              } else {
                stirringConf = stirringList[kk]
              }
            }
          }

          if (gubun === 'same') {
            const temp1 = {}
            temp1.configurations = mashingConf

            const temp2 = {}
            temp2.configurations = stirringConf

            mashingList.push('same')
            mashingList.push(resultId)
            mashingList.push(temp1)
            mashingList.push(temp2)

            mashingList.push(JSON.parse(stirringConf))
            res.json(mashingList)
          } else {
            mashingList.push('differnet')
            mashingList.push(resultId)
            res.json(mashingList)
          }
        }
      }
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method setMashingSetting
     * @description 막걸리 담금 자동제어 셋팅정보를 수정한다
     */
  var setMashingSetting = async function (req, res) {
    console.log(_modulename, 'setMashingSetting')
    var basicInfo = req.swagger.params.body.value
    try {
      if (basicInfo.gubun === 0) { // 고급, 탱크
        const result = await rrw.getMashingSetting1(basicInfo.id)
        const sResult = await rrw.getStirringSetting(basicInfo.id)

        var configuration = JSON.parse(result[0].configurations)
        var sConfiguration = JSON.parse(sResult[0].configurations)

        var obj = {}
        configuration.basic[0].value = basicInfo.alarmTemp1
        configuration.basic[1].value = basicInfo.controlTemp1
        configuration.basic[2].value = basicInfo.alarmTemp2
        configuration.basic[3].value = basicInfo.controlTemp2
        configuration.basic[4].value = basicInfo.controAlchole1
        configuration.basic[5].value = basicInfo.controCool1
        configuration.basic[6].value = basicInfo.controAlchole2
        configuration.basic[7].value = basicInfo.controCool2
        configuration.basic[8].value = basicInfo.controAlchole3
        configuration.basic[9].value = basicInfo.controCool3
        obj.basic = configuration.basic
        obj.advanced = configuration.advanced
        obj.timespan = configuration.timespan
        var sobj1 = JSON.stringify(obj)

        var obj2 = {}
        sConfiguration.basic[0].value = basicInfo.controlHour1
        sConfiguration.basic[1].value = basicInfo.controlMinute1
        sConfiguration.basic[2].value = basicInfo.controlHour2
        sConfiguration.basic[3].value = basicInfo.controlMinute2
        obj2.basic = sConfiguration.basic
        obj2.advanced = sConfiguration.advanced
        obj2.timespan = sConfiguration.timespan
        var sobj2 = JSON.stringify(obj2)

        await rrw.setSettingInfo1(basicInfo.id, sobj1)
        await rrw.setSettingInfo2(basicInfo.id, sobj2)
      } else { // 기본
        const resultId = await rrw.getMashingSetting2(basicInfo.id)
        const result = await rrw.getMashingSetting1(resultId[0].field_id)
        const sResult = await rrw.getStirringSetting(resultId[0].field_id)

        configuration = JSON.parse(result[0].configurations)
        sConfiguration = JSON.parse(sResult[0].configurations)

        const obj = {}
        configuration.basic[0].value = basicInfo.alarmTemp1
        configuration.basic[1].value = basicInfo.controlTemp1
        configuration.basic[2].value = basicInfo.alarmTemp2
        configuration.basic[3].value = basicInfo.controlTemp2
        configuration.basic[4].value = basicInfo.controAlchole1
        configuration.basic[5].value = basicInfo.controCool1
        configuration.basic[6].value = basicInfo.controAlchole2
        configuration.basic[7].value = basicInfo.controCool2
        configuration.basic[8].value = basicInfo.controAlchole3
        configuration.basic[9].value = basicInfo.controCool3
        obj.basic = configuration.basic
        obj.advanced = configuration.advanced
        obj.timespan = configuration.timespan

        const obj2 = {}
        sConfiguration.basic[0].value = basicInfo.controlHour1
        sConfiguration.basic[1].value = basicInfo.controlMinute1
        sConfiguration.basic[2].value = basicInfo.controlHour2
        sConfiguration.basic[3].value = basicInfo.controlMinute2
        obj2.basic = sConfiguration.basic
        obj2.advanced = sConfiguration.advanced
        obj2.timespan = sConfiguration.timespan

        const sobj1 = JSON.stringify(obj)
        const sobj2 = JSON.stringify(obj2)

        for (var i = 0; i < resultId.length; i++) {
          await rrw.setSettingInfo1(resultId[i].field_id, sobj1)
          await rrw.setSettingInfo2(resultId[i].field_id, sobj2)
        }
      }
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMMSInfo
     * @description 막걸리 이상발생 내역 수신자 정보를 조회한다
     */
  var getMMSInfo = async function (req, res) {
    console.log(_modulename, 'getMMSInfo')
    try {
      const result = await rrw.getMMSInfo()
      result.length > 0 ? res.json(result) : res.json({})
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method setMMSInfo
     * @description 막걸리 이상발생 내역 수신자 정보를 저장한다.
     */
  var setMMSInfo = async function (req, res) {
    console.log(_modulename, 'setMMSInfo')
    var basicInfo = req.swagger.params.body.value
    try {
      await rrw.setMMSInfo(basicInfo)
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method deleteMMSInfo
     * @description 막걸리 이상발생 내역 수신자 정보를 삭제한다
     */
  var deleteMMSInfo = async function (req, res) {
    console.log(_modulename, 'deleteMMSInfo')
    var basicInfo = req.swagger.params.body.value
    try {
      await rrw.deleteMMSInfo(basicInfo)
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method updateMMSInfo
     * @description 막걸리 이상발생 내역 수신자 정보를 수정한다
     */
  var updateMMSInfo = async function (req, res) {
    console.log(_modulename, 'updateMMSInfo')
    var basicInfo = req.swagger.params.body.value
    try {
      await rrw.updateMMSInfo(basicInfo)
      res.json('success')
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method getMMSList
     * @description 막걸리 알람 이상내역 리스트를 조회한다
     */
  var getMMSAlarmList = async function (req, res) {
    console.log(_modulename, 'getMMSAlarmList')
    var companyGubun = req.swagger.params.company.value
    try {
      const result = await rrw.getMMSList(companyGubun)
      result.length > 0 ? res.json(result) : res.json({})
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  /**
     * @method gertInterval
     * @description DB 주기적 호출
     */
  var gertInterval = async function (req, res) {
    console.log(_modulename, 'gertInterval')
    try {
      const result = await rrw.gertInterval()
      result.length > 0 ? res.json(result) : res.json({})
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }

  return {
    getAllMashingInfo: getAllMashingInfo,
    setMashingBasicInfo: setMashingBasicInfo,
    getMashingInfo: getMashingInfo,
    getMashingChartInfo: getMashingChartInfo,
    setMashingInfo: setMashingInfo,
    getMashingStepInfo: getMashingStepInfo,
    getMashingCurrentObservation: getMashingCurrentObservation,
    getMashingDashboardChartInfo: getMashingDashboardChartInfo,
    deleteMashingInfo: deleteMashingInfo,
    getMashingSetting: getMashingSetting,
    setMashingSetting: setMashingSetting,
    getMMSInfo: getMMSInfo,
    setMMSInfo: setMMSInfo,
    deleteMMSInfo: deleteMMSInfo,
    updateMMSInfo: updateMMSInfo,
    getMMSAlarmList: getMMSAlarmList,
    gertInterval: gertInterval
  }
}

module.exports = rrwApi()

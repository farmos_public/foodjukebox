from json import decoder
from multiprocessing import Queue, Process
import logging
import os
import time

from threading import Thread, Lock
import traceback

from six import u
from lib.external import nutrient
#from ..external.nutrient import client
#from ..mblock import CmdCode
#from .. import *

from lib.external.nutrient import client
from lib.external.nutrient.address import StandardAddress
from lib.mblock import CmdCode, Response, StatCode, ResCode
from lib import *
from lib import DevType
from lib.ssmate.mate_jnmqtt import JNMQTTMate

import json
import re

from urllib.parse import urlparse

# FORMAT = ('%(asctime)-15s %(threadName)-15s'\          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)

log = logging.getLogger()
log.setLevel(logging.INFO)


class NutrientMate(DSMate):
    def __init__(self, option, dev_info, coupleid, quelist):
        """
        양액기 메이트를 초기화한다.
        dev_info는 device 정보를 가지고 있다. 
        option은 연결 옵션을 의미한다.
        coupleid는 SSMate와 커플링할 id이다.
        quelist는 가지고 있는 큐 리스트들을 의미한다.

        Args:
            option (dic): 양액기 연결 정보를 의미한다. 
            ex) {
                'conn' : [{
                    'name' : 'ACM0',
                    'method': 'tcp',
                    'host' : '192.168.0.53',
                    'port' : 2288,
                    'timeout': 5}]}
            dev_info (dic): 
            연결 정보를 의미한다.
            "dk" : '[1,40221,["value","status"]]'
            dk : 1 - 장비의 slave id
                40221 - 읽기 시작하는 레지스터
            Ex) dev_info = [{
                    "id" : "1", 
                    "dk" : "",
                    "host": 
                    "dt": "gw", 
                    "children" : [{
                        "id" : "101", 
                        "dk" : '[1,40201,["status"],45001,["operation","opid"]]', 
                        "dt": "nd", 
                        "children" : [
                            {"id" : "102", 
                            "dk" : '[1,40211,["control","status","area","alert","opid"],45001, ["operation", "opid", "control","EC","pH", "start-area", "stop-area", "on-sec"]]', 
                            "dt": "nutrient-supply/level1"},
                        {"id" : "103", "dk" : '[1,40221,["value","status"]]', "dt": "sen"},
                        {"id" : "104", "dk" : '[1,40231,["value","status"]]', "dt": "sen"},
                        {"id" : "105", "dk" : '[1,40241,["value","status"]]', "dt": "sen"},
                        {"id" : "106", "dk" : '[1,40251,["value","status"]]', "dt": "sen"},
                        {"id" : "107", "dk" : '[1,40261,["value","status"]]', "dt": "sen"}]}]}]
            coupleid (string): 
                ex) "1"
            quelist (list): 
                ex) [Queue(), Queue(), Queue(), Queue()] 
        """
        super(NutrientMate, self).__init__(option, dev_info, coupleid, quelist)
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        client.setlogger(self._logger)
        self.conns = option["conn"]
        if type(self.conns) == dict:
            self.conns = [self.conns]

    def find_option(self, gateway_key):
        """
        Args:
            gateway_key ([type]): [description]

        Returns:
            [type]: [description]
        """

        scheme = None

        splitted = gateway_key.split(":")
        if len(splitted) == 2:
            scheme = "tcp"
            host, port = splitted
        else:
            scheme = "rtu"
            port = gateway_key

        for conn_option in self.conns:
            if scheme == "tcp" and conn_option["host"] == host and int(conn_option["port"]) == int(port):
                return conn_option
            elif scheme == "rtu" and conn_option["port"] == port:
                return conn_option

        return None

    def get_client(self, option):
        """
        클라이언트 접속 객체를 가져온다.
        
        Returns:
            clientConnector 
        """
        if "client" in option:
            return option["client"]
        return None

    def get_client_from_gw(self, gateway):
        """
        게이트웨이로부터 클라이언트 접속 정보를 가져온다.        
        """
        option = self.find_option(gateway["dk"])
        if not option:
            return None
        return self.get_client(option)

    def isgwconnected(self, gateway):
        """
        게이트웨이가 연결되었는지 확인한다.        
        """
        is_open = False

        option = self.find_option(gateway["dk"])

        if not option:
            return

        client_obj = self.get_client(option)
        is_open = client.is_open(client_obj)
        return is_open

    def all_connected(self):
        connected = True
        for option in self.conns:
            client_obj = self.get_client(option)
            if not client.is_open(client_obj):
                connected = False
        return connected

    def any_connected(self):
        connected = False
        for option in self.conns:
            client_obj = self.get_client(option)
            if client.is_open(client_obj):
                connected = True
        return connected

    def connect(self):
        """ 
        연결정보에 기록되어 있는 id를 기반으로 하나씩 연결한다. dt가 gw인 element에서 connection 정보를 얻을 수 있다. 단 gw당 1개의 node가 있는 것으로 가정한다.
        """
        for opt in self.conns:
            client_obj = self.get_client(opt)
            if not client.is_open(client_obj):
                host = opt["host"]
                port = opt["port"]
                self._logger.info("Try to connect : " +
                                  str(host) + " : " + str(port))
                opt["client"] = client.connect(host, port, "tcp")

    def run(self):
        self.matestart()
        self._logger.info(type(self).__name__ +
                          " mate run ... sleep : " + str(self._sleep["time"]))

        while self.isexecuting():
            try:
                if self.isexecuting() and not self.all_connected():
                    self._logger.info("try to connect for any connection.")
                    self.connect()
                    time.sleep(1)

                time.sleep(self._sleep["time"])
                self.process()
                self.uploadbackup()
                #print("heartbeat", type(self).__name__, datetime.now().strftime("%H:%M:%S"))
                self._logger.heartbeat(os.getpid())

            except Exception as ex:
                self._logger.error("There is an exception : " + str(ex))
                self._logger.error(traceback.format_exc())
                try:
                    self.close()
                except:
                    pass
        self.matestop()
        self._logger.info(
            "mate stop - " + str(self._coupleid) + " : " + str(os.getpid()))

    def close(self):
        """
        RTU 연결을 닫는다. 

        Returns:
            Any : 상위 클래스에서 닫기
        """

        for opt in self.conns:
            if "client" in opt:
                cli = opt["client"]
                cli.close()
        return super().close()

    def build_payload_and_write(self, conn, unit_id, builder, input):
        """
        페이로드를 만들고 Modbus tcp에 연결합니다.

        Args:
            conn ([type]): [description]
            unit_id ([type]): [description]
            builder ([type]): [description]
            input ([type]): [description]

        Returns:
            [type]: [description]
        """
        start_address = input["start"]
        payload = client.build_payload(builder, input)
        return client.write(conn, unit_id, start_address, payload)

    def find_nutrient(self, node: dict):
        if node is None:
            return None
        children = node.get("children", [])
        for child in children:
            if DevType.isnutsupplier(child["dt"]):
                return child
        return None

    def find_nutrient_type(self, node):
        nutrient = self.find_nutrient(node)
        if nutrient is not None and "dt" in nutrient:
            return nutrient["dt"]
        return None

    def get_request_build_set(self, req: Request):
        self._logger.debug("Command :" + str(req.getcommand()))

        node_id = req.getnodeid()
        self._logger.debug("Request :" + str(req))

        gateway = self._devinfo.findgateway(node_id)
        self._logger.debug("Gateway :" + str(gateway))

        node = self._devinfo.finddevbyid(node_id)
        self._logger.debug("Node :" + str(node))

        client_obj = self.get_client_from_gw(gateway)

        node_key = node["dk"]
        unit_id = self.get_unit_id(node_key)

        builder = client.create_payload_builder()

        return client_obj, unit_id, builder

    def process_cancel_detect_request(self, req: Request):
        n = Notice(None, NotiCode.DETECT_CANCELED)
        self._writenoti(n)

    def process_detect(self, req: Request):
        opid = req.getopid()
        res = Response(req)
        res.setresult(ResCode.OK)
        self._writeres(res)
        self.detect_devices(opid)
        return

    def process_change_control(self, req: Request):
        opid = req.getopid()
        conn, unit_id, builder = self.get_request_build_set(req)

        if conn is None:
            resp = Response(req)
            resp.setresult(ResCode.DISCONNECTED)
            self._writeres(resp)
            self._logger.debug("connection is lost.")
            return None

        if unit_id is None:
            resp = Response(req)
            resp.setresult(ResCode.FAIL)
            self._writeres(resp)
            self._logger.debug("unit_id is lost.")
            return None

        params = req.getparams()
        input = client.get_change_control_input(opid,
                                                command=2,
                                                control=params["control"])
        result = self.build_payload_and_write(conn, unit_id, builder, input)
        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def process_param_watering(self, req: Request):
        opid = req.getopid()
        conn, unit_id, builder = self.get_request_build_set(req)

        if unit_id is None or conn is None:
            resp = Response(req)
            resp.setresult(ResCode.FAIL_NO_DEVICE)
            self._writeres(resp)
            return None

        params = req.getparams()
        input = client.get_param_on_input(opid,
                                          start_area=params["start-area"],
                                          stop_area=params["stop-area"],
                                          on_sec=params["on-sec"],
                                          EC=params["EC"],
                                          pH=params["pH"])
        result = self.build_payload_and_write(conn, unit_id, builder, input)
        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def process_area_watering(self, req: Request):
        opid = req.getopid()
        conn, unit_id, builder = self.get_request_build_set(req)

        if unit_id is None or conn is None:
            resp = Response(req)
            resp.setresult(ResCode.FAIL_NO_DEVICE)
            self._writeres(resp)
            return None

        params = req.getparams()
        input = client.get_area_on_input(opid,
                                         start_area=params["start-area"],
                                         stop_area=params["stop-area"],
                                         on_sec=params["on-sec"])
        result = self.build_payload_and_write(conn, unit_id, builder, input)
        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def process_off_request(self, req: Request):
        opid = req.getopid()
        conn, unit_id, builder = self.get_request_build_set(req)

        if unit_id is None or conn is None:
            resp = Response(req)
            resp.setresult(ResCode.FAIL_NO_DEVICE)
            self._writeres(resp)
            return None

        input = client.get_off_input(opid)
        result = self.build_payload_and_write(conn, unit_id, builder, input)
        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def process_on_request(self, req: Request):
        opid = req.getopid()
        conn, unit_id, builder = self.get_request_build_set(req)

        if unit_id is None or conn is None:
            resp = Response(req)
            resp.setresult(ResCode.FAIL_NO_DEVICE)
            self._writeres(resp)
            return None

        input = client.get_on_input(opid)
        result = self.build_payload_and_write(conn, unit_id, builder, input)

        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def process_if_disconnected(self, req: Request):
        result = ResCode.DISCONNECTED
        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def process_not_proper_cmd(self, req: Request):
        result = ResCode.FAIL_NOT_PROPER_COMMAND
        resp = Response(req)
        resp.setresult(result)
        self._writeres(resp)

    def processrequest(self, req: Request):
        """
        SSMate에서 DSMate로 들어온 Request를 처리한다.

        request가 관수일 때, operation info를 수행
        request가 제어일 때, 노드 제어를 수행
        Args:
            req (Request): 제어 요청 수행

        Request Code는 다음과 같습니다.    
        공통 : 0 OFF 작동 정지
        노티 코드 : 
                1 RESET 리셋
                2 CONTROL 제어권 획득/반납
                201 ON 작동
                202 TIMED ON 일정시간 작동
                203 DIRECTIONAL_ON 일정시간 및 방향 작동

                301 OPEN
                302 CLOSE
                303 TIMED
                304 TIMED_CLOSE
                305 POSITION
                306 SET_TIME

                401 ONCE_WATERING
                402 AREA_WATERING
                403 PARAMED_WATERING
        """
        self._logger.debug("Process request!")

        command = req.getcommand()
        self._logger.debug("Process command : " + str(command))
        # TODO : 문서에는 1이 Reset, 2가 ChangeControl이며, code 와 다르다.
        if command == CmdCode.ON or command == CmdCode.ONCE_WATERING:
            self.process_on_request(req)
        elif command == CmdCode.OFF:
            self.process_off_request(req)
        elif command == CmdCode.AREA_WATERING:
            self.process_area_watering(req)
        elif command == CmdCode.PARAMED_WATERING:
            self.process_param_watering(req)
        elif command == CmdCode.CHANGE_CONTROL:
            self.process_change_control(req)
        elif command == CmdCode.DETECT_DEVICE:
            self.process_detect(req)
        elif command == CmdCode.CANCEL_DETECT:
            self.process_cancel_detect_request(req)
        else:
            self.process_if_disconnected(req)
            self.process_not_proper_cmd(req)

    def find_node(self, node_id):
        """
        node id를 가진 node 정보를 찾는다.

        Args:
            node_id (str): node id

        Returns:
            dict: node info 
        """
        for gateway in self._devinfo:
            for node in gateway["children"]:
                if node_id == node["id"]:
                    return node

    def apply_all_device(self, device_func):
        """

        Args:
            device_func ([type]): [description]
        """
        result_list = []

        for gateway in self._devinfo:
            for node in gateway["children"]:
                for device in node["children"]:
                    result = device_func(gateway, node, device)
                    result_list.append(result)
        return result_list

    def apply_all_node(self, node_func):
        """

        Args:
            device_func ([type]): [description]
        """
        result_list = []

        for gateway in self._devinfo:
            for node in gateway["children"]:
                result = node_func(gateway, node)
                result_list.append(result)
        return result_list

    def apply_all_gateway(self, gateway_func):
        """

        Args:
            device_func ([type]): [description]
        """
        result_list = []

        for gateway in self._devinfo:
            result = gateway_func(gateway)
            result_list.append(result)
        return result_list

    def get_unit_id(self, device_key):
        self._logger.debug(device_key)
        if type(device_key) is str:
            try:
                device_key = json.loads(device_key)
                return device_key[0]
            except Exception as e:
                self._logger.error(traceback.format_exc())
                return -1
        elif type(device_key) is list:
            return device_key[0]
        return None

    def get_device_status_address(self, device_key):
        """
        레지스터 주소 리스트를 가져온다. devinfo에 다음과 같이 설정되있다고 가정한다.
        {"id":"203", "dk":[2,201]}
        가져오는 값은 201이다.
        Args:
        dev_id (장치 id): 장치 식별자
        """

        if type(device_key) is list and len(device_key) >= 2:
            return device_key[1]
        elif type(device_key) is str:
            try:
                device_list = json.loads(device_key)
                return self.get_device_status_address(device_list)
            except:
                self._logger.error(traceback.format_exc())
                return None
        return None

    def processobservations(self):
        """
        Mate에서 Observation을 처리한다. 장치 상태를 파악할 시간대인지 체크를 하고 장치를 파악한다. 
        장치를 파악한 뒤에는 시간을 업데이트한다.
        register_number --> device-id
        """
        if not self._timetocheck(Mate.OBSTYPE):
            return

        self._logger.debug("Process observation.")

        def check_observation(gateway, node):
            if not self.isgwconnected(gateway):
                return

            conn = self.get_client_from_gw(gateway)
            node_key = node["dk"]
            unit_id = self.get_unit_id(node_key)

            observation = Observation(node["id"])

            for device in node["children"]:
                if not DevType.issensor(device["dt"]):
                    continue

                device_id = device["id"]
                device_key = device["dk"]

                address = self.get_device_status_address(device_key)
                try:
                    registers = client.read(conn, unit_id, address, 3)
                    decoder = client.create_decoder(registers)
                    sensor_status_info = client.parse_sensor_status_info(
                        decoder)
                except Exception as e:
                    self._logger.error(traceback.format_exc())
                    continue

                value = sensor_status_info["sensor_value"]
                status = sensor_status_info["sensor_status"]
                status_value = StatCode(status)

                observation.setobservation(device_id, value, status_value)
            self._writeobs(observation)

        self.apply_all_node(check_observation)
        self._updatetime(Mate.OBSTYPE)

    def sendnodestatus(self, conn, device, node_id, unit_id):

        node_status_payload = client.read(conn, unit_id, StandardAddress.NODE_STATUS.value, 3)

        decoder = client.create_decoder(node_status_payload)
        node_status_info = client.parse_node_status_info(decoder)

        if node_status_info and decoder:
            n = Notice(node_id, NotiCode.ACTUATOR_STATUS)
            n.settime(None)
            n.setkeyvalue(node_id, {
                "status": node_status_info["status"],
                "opid": node_status_info["opid"],
                "control": node_status_info["control"]
            })
            self._writenoti(n)

    def sendnutstatus(self, conn, device, node_id, unit_id):
        nutrient_status_payload = client.read(
            conn, unit_id, StandardAddress.NUTRIENT_STATUS.value, 4)
        decoder = client.create_decoder(nutrient_status_payload)
        nutrient_status_info = client.parse_nutrient_status(decoder)

        if nutrient_status_info and decoder:

            n = Notice(node_id, NotiCode.ACTUATOR_STATUS)
            n.settime(None)

            n.setkeyvalue(device["id"], {
                "status": nutrient_status_info["status"],
                "area": nutrient_status_info["area"],
                "alert": nutrient_status_info["alert"],
                "opid": nutrient_status_info["opid"]
            })

            self._writenoti(n)

    def processnotices(self):
        """
        노티스를 전송한다.

        """

        if not self.any_connected():
            return

        self._logger.debug("process notices.")

        def send_node_control_message(gateway, node, device):
            if device["dt"] not in ["act/nutrient-supply/level0",
                                    "act/nutrient-supply/level1",
                                    "act/nutrient-supply/level2",
                                    "act/nutrient-supply/level3"]:
                return None

            if not self.isgwconnected(gateway):
                return

            conn = self.get_client_from_gw(gateway)
            node_id = node["id"]
            device_key = node["dk"]
            unit_id = self.get_unit_id(device_key)

            node_status_payload = client.read(
                conn, unit_id, StandardAddress.NODE_STATUS.value, 3)
            decoder = client.create_decoder(node_status_payload)
            node_status_info = client.parse_node_status_info(decoder)

            if not node_status_info:
                return

            active_status = node_status_info.get("status", None)

            if active_status == StatCode.SUPPLYING.value:
                if self._timetocheck(Mate.ACTNOTITYPE):
                    self.sendnodestatus(conn, device, node_id, unit_id)
                    self.sendnutstatus(conn, device, node_id, unit_id)
                    self._updatetime(Mate.ACTNOTITYPE)
            else:
                if self._timetocheck(Mate.NOTITYPE):
                    self.sendnodestatus(conn, device, node_id, unit_id)
                    self.sendnutstatus(conn, device, node_id, unit_id)
                    self._updatetime(Mate.NOTITYPE)

        self.apply_all_device(send_node_control_message)

    def process(self):
        """
        Mate에서 processing을 수행한다.
        """
        if not self.any_connected():
            return
        self.processrequests()
        self.processobservations()
        self.processnotices()

    def get_device_code_info(self, conn, node_unit_id):
        """
        장치의 코드 정보를 가져온다.
        노드 유닛 아이디와, 연결 객체를 인자값으로 받는다.
        Args:
            conn (DeviceConnection): 
            node_unit_id (int): 노드의 유닛 id

        Returns:
            deice_code 정보: 클라이언트로부터 device의 코드를 가져온다.
        """
        device_install_start_address = 101
        max_device_count = 20

        payload = client.read(conn, node_unit_id,
                              device_install_start_address, max_device_count)
        decoder = client.create_decoder(payload)
        device_code_info = client.parse_device_code_info(decoder)

        return device_code_info

    def get_node_info(self, conn, node_unit_id):
        """
        장치의 정보들을 가져온다.
        노드 정보와 유닛 아이디를 인자값으로 받는다.
        Args:
            conn (Connection type):  
            node_unit_id (int): 노드 유닛 id를 

        Returns:
            dict : 노드 정보를 파싱한 값
        """
        node_info_size = 8
        node_info_address = 1

        payload = client.read(conn, node_unit_id,
                              node_info_address, node_info_size)
        decoder = client.create_decoder(payload)
        return client.parse_node_info(decoder)

    def detect_node_info(self, node_unit_id, conn):
        """
        노드를 탐색하는데, 노드 정보를 찾는 과정을 거친다. 
        Args:
            node_unit_id (int): [description]
            conn (connection): [description]

        Returns:
            [type]: [description]
        """
        self._logger.debug("detect_node_info : " + str(node_unit_id))

        node_info = self.get_node_info(conn, node_unit_id)

        protocol_version = node_info["protocol_version"]
        product_type = node_info["product_type"]
        product_code = node_info["product_code"]

        if protocol_version != 20:
            time.sleep(3.0)
            n = Notice(None, NotiCode.DETECT_UNKNOWN_PROTOCOL_VER)
            self._writenoti(n)
            return None

        if product_type != 3:
            time.sleep(3.0)
            n = Notice(None, NotiCode.DETECT_UNKNOWN_NODE)
            self._writenoti(n)
            return None

        if product_code not in [1, 2, 3, 4]:
            time.sleep(3.0)
            n = Notice(None, NotiCode.DETECT_WRONG_DEVICE)
            self._writenoti(n)
            return None

        return {"compcode": node_info["company_code"],
                "nodetype": node_info["product_type"],
                "nodecode": node_info["product_code"],
                "devcodes": self.get_device_code_info(conn, node_unit_id)}

    def detect_gateway(self, operation_id, host, port, method):
        node_unit_id = 1
        connection = client.connect(host, port, method)

        if client.is_open(connection):
            # Detection Started
            n = Notice(None, NotiCode.DETECT_NODE_DETECTED)
            n.setkeyvalue("opid", operation_id)
            n.settime(None)
            self._logger.debug("detect info : " + str(n._content))
            self._writenoti(n)
            content = {}
            node_info = self.detect_node_info(node_unit_id, connection)
            if node_info and node_unit_id:
                content[node_unit_id] = node_info

            self._logger.debug("detected content : " + str(content))
            return content
        else:
            n = Notice(None, NotiCode.DETECT_NO_NODE)  # Detection Started
            n.setkeyvalue("opid", operation_id)
            n.settime(None)
            self._logger.debug("not detect info : " + str(n._content))
            self._writenoti(n)
            return None

    def detect_start(self, operation_id):
        n = Notice(None, NotiCode.DETECT_NODE_STARTED)  # Detection Started
        n.setkeyvalue("opid", operation_id)
        n.settime(None)
        self._writenoti(n)

    def detect_no_node(self, operation_id):
        n = Notice(None, NotiCode.DETECT_NO_NODE)  # Detection Started
        n.setkeyvalue("opid", operation_id)
        n.settime(None)
        self._logger.debug("not detect info : " + str(n._content))
        self._writenoti(n)

    def detect_finish(self, operation_id):
        # Detection Started
        detect_fin = Notice(None, NotiCode.DETECT_FINISHED)
        detect_fin.setkeyvalue("opid", operation_id)

        for option in self.conns:
            if not "host" in option or not "port" in option:
                continue
            host = option["host"]
            port = option["port"]
            location = str(host) + ":" + str(port)
            self._logger.debug("finding location : " + location)
            content = self.detect_gateway(operation_id, host, port, "tcp")
            detect_fin.setcontent(location, content)

        self._logger.debug("detect finished : " + str(detect_fin._content))
        detect_fin.settime(None)
        self._writenoti(detect_fin)

    def detect_devices(self, operation_id):
        """
        device를 탐색하는 작업을 요청한다. 
        양액기 노드의 경우, 표준에 레지스터들이 모두 정의되어 있으므로, 
        device가 등록되어 있는지만 확인한다.
        """
        self.detect_start(operation_id)

        self._logger.debug("detect from options : " + str(self._option))

        if "conn" not in self._option:
            self.detect_no_node(operation_id)
            return None

        self.detect_finish(operation_id)


if __name__ == "__main__":
    from multiprocessing import Queue
    """
    tcp://127.0.0.1:2882
    rtu://dev/ttyUSB
    """
    devinfo = [{
        "id": "1", "dk": "tcp://127.0.0.1:2222",
        "host": '192.168.1.194',
        "port": 2222,
        "dt": "gw",
        "children": [
            {"id": "101",
             "dk": '[1, 1,["certification", "company_code", "product_type", "product_code", "protocol_version","channel_number","serial_number"]]',
             "dt": "nd",
             "children": [
                {"id": "102",
                 "dk": '[1,204,["value","status"]]',
                 "dt": "sen"},
                {"id": "103",
                 "dk": '[1,207,["value","status"]]',
                 "dt": "sen"},
                {"id": "104",
                 "dk": '[1,210,["value","status"]]',
                 "dt": "sen"},
                {"id": "105",
                 "dk": '[1, 261,["valve","status","control","node-command","node-opid","nutrient-command","nutrient-opid","start-area","stop-area","on-sec","EC","pH"]]',
                 "dt": "act/nutrient-supply/level3"
                 }
             ]}
        ]},
        {"id": "2", "dt": "gw", "dk": ""}]
    opt = {
        'conn': [{
            "name": 'ACM0',
            "method": 'tcp',
            "host": '127.0.0.1',
            "port": 2222,
            "timeout": 5
        }]
    }

    quelist = [Queue(), Queue(), Queue(), Queue()]

    numate = NutrientMate(opt, devinfo, coupleid="1", quelist=quelist)
    jnmatt_opt = {
        "conn": {
            "host": "dev.jinong.co.kr",
            "port": 1883,
            "keepalive": 60
        },
        "mqtt": {
            "svc": "cvtgate",
            "id": "1"
        },
        "area": "local"
    }
    ssmate = JNMQTTMate(jnmatt_opt, devinfo, "1", quelist)

    ssmate.start()
    numate.start()

    req = Request("101")
    req.setcommand("105", CmdCode.ONCE_WATERING, params={})
    ssmate._writereq(req)

    time.sleep(10)

    req = Request("101")
    req.setcommand("105", CmdCode.AREA_WATERING, params={
                   "on-sec": 2, "start-area": 3, "stop-area": 9})
    ssmate._writereq(req)
    time.sleep(10)

    req = Request("101")
    req.setcommand("105", CmdCode.PARAMED_WATERING, params={
                   "EC": 5.6, "pH": 4.6, "on-sec": 2, "start-area": 3, "stop-area": 9})
    ssmate._writereq(req)

    time.sleep(10)

    req = Request("101")
    req.setcommand("105", CmdCode.SET, params={"command": 2, "control": 1})
    ssmate._writereq(req)

    time.sleep(3)

    req = Request("101")
    req.setcommand("1", CmdCode.DETECT_DEVICE, {'saddr': 1, 'eaddr': 3})
    ssmate._writereq(req)

    time.sleep(120)

    for q in quelist[1:]:
        q.close()
        q.join_thread()
    numate.stop()
    ssmate.stop()

    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass

    quelist[0].close()
"""
{"value" : (2, "float"), "status" : (1, "status"), 
"opid" : (1, "short"), "state-hold-time" : (2, "int"), "ratio": (1, "short"), 
"position" : (1, "short"), "remain-time" : (2, "int"),
"control": (1, "control"), "area" : (1, "short"), "alert" : (1, "alert"), 
"hold-time" : (2, "int"), "operation" : (1, "operation"),
"time" : (2, "int"), "opentime" : (1, "short"), "closetime" : (1, "short"),
"EC": (2, "float"), "pH": (2, "float"), "on-sec" : (1, "short"),
"start-area" : (1, "short"), "stop-area": (1, "short"), 
"epoch" : (2, "int"), "vfloat": (2, "float"), "vint" : (2, "int")}
"""

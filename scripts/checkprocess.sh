#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

function findProcess(){
    process=$(ps aux | grep $1)
    num=$(echo $process | grep -o $1 | wc -w)
    echo -e $num
}

function checkProcess(){
    if [ $1 -eq $2 ] ; then
        cat /var/log/cpmng.log | tail -n 5 > temp
        sudo pkill -ef $3 >> temp
        python processSlack.py one$3
    elif [ $1 -eq 2 ] ; then
        cat /var/log/cpmng.log | tail -n 5 > temp
        sudo pkill -ef $3 >> temp
        python processSlack.py two$3

    else
        echo -e "good"
        exit
    fi
}

#compareValue=$(findProcess fcore)
#checkProcess $compareValue 2 fcore

compareValue=$(findProcess couplemng)
checkProcess $compareValue 3 couple

echo -e "\n" `date` >> log.txt
cat temp >> log.txt



DROP DATABASE IF EXISTS cvtgatesvc;
CREATE DATABASE cvtgatesvc DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'farmos'@'localhost' IDENTIFIED BY 'farmosv2@';
GRANT ALL PRIVILEGES ON cvtgatesvc.* TO 'farmos'@'localhost';
FLUSH PRIVILEGES;

use cvtgatesvc;

CREATE TABLE mate (
    id      varchar(50) primary key,
    side    char(2) not null,
    deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    config  text
);

CREATE TABLE gate (
    id      varchar(50) not null,
    name    varchar(50) not null,
    farmid  int(11), 
    description text, 
    deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE couple (
    id      varchar(50) not null,
    gateid  varchar(50) not null,
    name    varchar(50) not null,
    deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    config  text
);

CREATE TABLE devices (
    id       integer not null auto_increment,
    devid   integer,
    nodeid   integer,
    gwid     integer,
    coupleid varchar(50),
    gateid   varchar(50) not null,  /* serial ID */
    server   varchar(50) not null,
    name     varchar(50),
    deleted  boolean default false,
    spec     text,
    description text,
    PRIMARY KEY (`id`)
);

CREATE TABLE `farm` (
    `id` int(11) NOT NULL,
    `name` varchar(100) NOT NULL,
    `info` text,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into mate(id, side, config) values('9651f4f9-5d54-43c0-b14b-5f82b75d864c', 'ss',
'{
  "id":"9651f4f9-5d54-43c0-b14b-5f82b75d864c",
  "side":"ss",
  "mod":"mate_jnctrl",
  "class":"JNCtrlMate",
  "opt":{
    "conn" : {"host" : "fos001.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "db" : {"host" : "220.90.133.21", "user" : "smartLink", "password" : "smartLink1!", "db" : "smartLink"},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"}
  }
}');

insert into mate(id, side, config) values('7d151935-c2a0-469d-8bde-49699680dd76', 'ss',
'{
  "id":"7d151935-c2a0-469d-8bde-49699680dd76",
  "side":"ss",
  "mod":"mate_farmos2",
  "class":"FarmosMate2",
  "opt":{
    "conn" : {"host" : "localhost", "port" : 1883, "keepalive" : 60},
    "db" : {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"}
    "mqtt" : {"svc" : "cvtgate", "id" : "1"}
  }
}');

insert into mate(id, side, config) values('c66b64ec-88a9-4b6d-a940-9fd2de4bcdd1', 'ds',
'{
  "id":"c66b64ec-88a9-4b6d-a940-9fd2de4bcdd1",
  "side":"ds",
  "mod":"mate_dummy",
  "class":"DummyMate"
  "opt" : {
  }
}');

insert into mate(id, side, config) values('c66b64ec-88a9-4b6d-a940-9fd2de4bcddf', 'ds',
'{
  "id":"c66b64ec-88a9-4b6d-a940-9fd2de4bcddf",
  "side":"ds",
  "mod":"mate_ksx3267v3",
  "class":"KSX3267MateV3",
  "opt":{
    "conn": [{
        "name" : "ING502",
        "method": "tcp",
        "host" : "test.ingsys.kr",
        "port" : 502,
        "timeout": 5
    }, {
        "name" : "ING503",
        "method": "tcp",
        "host" : "test.ingsys.kr",
        "port" : 503,
        "timeout": 5
    }]
  }
}');

insert into gate(id, name) values('78bf-8e1e-0ace', '사내테스트용 스마트링크');

insert into couple(id, gateid, name, config) values('12e19fed-0196-489f-921f-c93239f3c582', 
'78bf-8e1e-0ace', '인지장비연동',
'{
  "id": "12e19fed-0196-489f-921f-c93239f3c582",
  "name" : "인지장비연동",
  "dsmate": {
    "mod":"mate_ksx3267v3",
    "class":"KSX3267MateV3",
    "opt":{
      "conn": [{
          "name" : "192.168.0.33:502",
          "method": "tcp",
          "host" : "192.168.0.33",
          "port" : 502,
          "timeout": 5
      }, {
          "name" : "192.168.0.53:502",
          "method": "tcp",
          "host" : "192.168.0.53",
          "port" : 502,
          "timeout": 5
      }]
    }
  },
  "ssmate": {
    "mod":"mate_farmos2",
    "class":"FarmosMate2",
    "opt":{
      "conn" : {"host" : "localhost", "port" : 1883, "keepalive" : 60},
      "db" : {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"},
      "mqtt" : {"svc" : "cvtgate", "id" : "12e19fed-0196-489f-921f-c93239f3c582"}
    }
  },
  "children": [
    {"id" : "1", "dk" : "192.168.0.33:502", "dt": "gw", "children" : [
        {"id" : "2", "dk" : "[0,202,[\\"status\\"]]", "dt": "nd", "children" : [
            {"id" : "3", "dk" : "[0,203,[\\"value\\",\\"status\\"]]", "dt": "sen"},
            {"id" : "4", "dk" : "[0,212,[\\"value\\",\\"status\\"]]", "dt": "sen"}
        ]}
    ]},
    {"id" : "11", "dk" : "192.168.0.53:503", "dt": "gw", "children" : [
        {"id" : "12", "dk" : "[0,201,[\\"opid\\", \\"status\\"], 501,[\\"operation\\", \\"opid\\"]]", "dt": "nd", "children" : [
            {"id" : "13", "dk" : "[0,267,[\\"opid\\",\\"status\\",\\"remain-time\\"], 567,[\\"operation\\", \\"opid\\", \\"time\\"]]", "dt": "act/retractable/level1"},
            {"id" : "14", "dk" : "[0,271,[\\"opid\\",\\"status\\",\\"remain-time\\"], 571,[\\"operation\\", \\"opid\\", \\"time\\"]]", "dt": "act/retractable/level1"}
        ]}
    ]}
  ]
}');

insert into gate(id, name) values('__external__jnctrl__', '지농관제연동용');
insert into couple(id, gateid, name, config) values('13e19fed-0196-489f-921f-c93239f3c582', 
'__external__jnctrl__', '인지장비-지농관제연동',
'{
  "id": "13e19fed-0196-489f-921f-c93239f3c582",
  "name" : "인지장비-지농관제연동",
  "dsmate": {
    "mod":"mate_dummy",
    "class":"DummyMate",
    "opt":{
    }
  },
  "ssmate": {
    "mod":"mate_jnctrl",
    "class":"JNCtrlMate",
    "opt":{
      "conn" : {"host" : "farmos003.jinong.co.kr", "port" : 1883, "keepalive" : 60},
      "db" : {"host" : "220.90.133.21", "user" : "smartLink", "password" : "smartLink1!", "db" : "smartLink"},
      "mqtt" : {"svc" : "cvtgate", "id" : "#"}
    }
  },
  "children": []
}');

INSERT INTO `farm` VALUES (1, '지농 시험 농장', '{"farmer":"조윤성", "telephone":"0313601970","gps":"37.397962970070104,126.93206214966011","address":"경기 안양시 동안구 시민대로 248번길 25","postcode":"14067"}');

INSERT INTO devices(id, devid, nodeid, gwid, coupleid, gateid, server, name, spec, description) values
(1, NULL, NULL, NULL, NULL, '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '사내테스트용 스마트링크', '', ''),
(2, NULL, NULL, NULL, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '사내테스트용 스마트링크', '', ''),
(3, NULL, NULL, 1, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '192.168.0.33:503', '', ''),
(4, NULL, 2, 1, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '센서노드', '', ''),
(5, 3, 2, 1, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '온도센서', '{"Class":"sensor","Type":"temperature-sensor"}', ''),
(6, 4, 2, 1, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '습도센서', '{"Class":"sensor","Type":"humidity-sensor"}', ''),
(7, NULL, NULL, 11, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '192.168.0.33:503', '', ''),
(8, NULL, 12, 11, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '구동기노드', '', ''),
(9, 13, 12, 11, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '개폐기1', '{"Class":"actuator","Type":"retractable/level1"}', ''),
(10, 14, 12, 11, '12e19fed-0196-489f-921f-c93239f3c582', '78bf-8e1e-0ace', 'farmos003.jinong.co.kr', '개폐기2', '{"Class":"actuator","Type":"retractable/level1"}', '');


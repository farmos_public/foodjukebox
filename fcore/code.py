#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

from enum import IntEnum

class RetCode(IntEnum):
    OK = 0
    NOT_USED = 1
    TRIGGER_NOT_ACTIVATED = 2
    PROCESSOR_EXCEPTION = 3
    UNKNOWN_ERROR = 4
    WRONG_TIMESPAN = 5
    ABNORMAL_DEVICE = 6
    PARAM_ERROR = 7
    NOT_PROPER_TIME = 8
    MULTIRULE_NOT_ACTIVATED = 9

class VarCode(IntEnum):
    NORM = 0    # Normal Variable size = 1
    LIST = 1    # List Variable size = 1
    DICT = 2    # Dict Variable size = 1
    HIST = 3    # Historical Variable size = n
    TSPAN = 4   # TimeSpan Variable size = 1
    IMG = 5     # Image Variable size = n
    STRING = 6  # String Variable size = 1. same with NORM

class AlertCode(IntEnum):
    FOS_HW_DISKSHORTAGE = 1010001
    FOS_HW_CPULOAD = 1010002
    FOS_SW_FCORE = 1020001
    FOS_SW_CVTGATE = 1020002

    FOS_SEN_DATARATE = 2010001
    FOS_SEN_STATUS = 2010002
    FOS_ACT_STATUS = 2020001
    FOS_NUT_STATUS = 2030001
    FOS_NUT_ALERT = 2040000
    FOS_AUTO_FAIL = 2050001

    FOS_ENV_DATAMAX = 3010001
    FOS_ENV_DATAMIN = 3010002

class AlertGrade(IntEnum):
    LOW_CAUTION = 10
    MID_WARNING = 20
    HIGH_DANGER = 30


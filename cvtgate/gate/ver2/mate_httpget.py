#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import requests

from imports import *
from mate import Mate, ThreadMate

'''
{
    "conn" : {
        "base" : "http://220.92.228.68:8080/api.php", 
        "params":{"token":"3dc5f1ca-4513-4752-a40a-67baeb084d0e"}
    }
}

'''

class HttpGetMate(ThreadMate):
    def __init__(self, option, devinfo, coupleid, logger):
        self._base = option["conn"]["base"]
        self._params = option["conn"]["params"]
        self._lastres = None
        super(HttpGetMate, self).__init__(option, devinfo, coupleid, logger)
   
    def readmsg(self):
        res = requests.get(self._base, params=self._params)
        if res.status_code == 200:
            self._lastres = res.json()
            print("msg", self._lastres)
        else:
            self._lastres = None
            self._logger.warn("failed : " + str(res))


if __name__ == "__main__":
    option = {"conn" : {"base" : "http://220.92.228.68:8080/api.php", "params":{"token":"3dc5f1ca-4513-4752-a40a-67baeb084d0e"}}}
    devinfo = []
    hget = HttpGetMate(option, devinfo, "1", None)
    mate = Mate({}, [], "1", None)
    hget.start(mate.writeblk)
    time.sleep(3)
    hget.stop()

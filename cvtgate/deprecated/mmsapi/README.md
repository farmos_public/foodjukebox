== 소개 ==

알리고 서비스를 통해 문자메세지를 전송해주는 서비스

== 샘플 ==
* path : http://dev.jinong.co.kr:3030/mms
* method : POST
* body 값 샘플:
```
{
  "key":"eb9b404e-7547-11ea-bc55-0242ac130003",
  "user_id":"jinong",
  "sender" : "010-3840-7930",
  "msg" : "dev.jinong.co.kr 테스트 중입니다. ",
  "receiver" : "010-3714-7380",
  "testmode_yn" : "Y"
}
```

== KEY 발급 ==

발급은 라라펠님께 문의한다. 발급내역은 [드라이브의 문서](https://drive.google.com/drive/u/0/folders/1WgkCWuJPZP3ZcNdL8MYfaC6FO2ldX9E1) 를 확인한다. 


CREATE DATABASE cvtgate DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use cvtgate;

CREATE TABLE mate (
    id      varchar(50) primary key,
    side    char(2) not null,
    deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    config  text
);

CREATE TABLE gate (
    id      varchar(50) not null,
    name    varchar(50) not null,
    deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE couple (
    id      varchar(50) not null,
    gateid  varchar(50) not null,
    name    varchar(50) not null,
    deleted boolean default false,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    config  text
);

insert into mate(id, side, config) values('9651f4f9-5d54-43c0-b14b-5f82b75d864c', 'ss',
'{
  "id":"9651f4f9-5d54-43c0-b14b-5f82b75d864c",
  "side":"ss",
  "mod":"mate_hspestapi",
  "class":"HSPestAPIMate",
  "opt":{
    "conn" : {"base" : "http://hs.jinong.co.kr:8080/hsict/pest/"}
  }
}');

insert into mate(id, side, config) values('7d151935-c2a0-469d-8bde-49699680dd76', 'ss',
'{
  "id":"7d151935-c2a0-469d-8bde-49699680dd76",
  "side":"ss",
  "mod":"mate_rrwdb",
  "class":"RRWDBMate",
  "opt":{
    "conn" : {"host" : "localhost", "user" : "root", "password" : "jinong#0801!", "db" : "iot_rrw"}
  }
}');

insert into mate(id, side, config) values('c66b64ec-88a9-4b6d-a940-9fd2de4bcddf', 'ds',
'{
  "id":"c66b64ec-88a9-4b6d-a940-9fd2de4bcddf",
  "side":"ds",
  "mod":"mate_hscdtp",
  "class":"HSCDTPMate",
  "opt":{
    "conn":{"port": 10010},
    "sleeptime": 53,
    "gatewaykey" : "JN-01"
  }
}');

insert into mate(id, side, config) values('1679124b-505a-4f66-a3c2-a4968032df6e', 'ds',
'{
  "id":"1679124b-505a-4f66-a3c2-a4968032df6e",
  "side":"ds",
  "mod":"mate_kdrrw",
  "class":"KDRRWMate",
  "opt":{
    "conn":{"host": "", "port": 8825}
  }
}');


insert into gate(id, name) values('abeba8c9-4714-4feb-bd23-b2ba491eae92', '화성병해충기상대');
insert into couple(id, gateid, name, config) values('12e19fed-0196-489f-921f-c93239f3c582', 
'abeba8c9-4714-4feb-bd23-b2ba491eae92', '화성병해충기상대',
'{
  "id": "12e19fed-0196-489f-921f-c93239f3c582",
  "name" : "화성병해충기상대",
  "ssmate": {
    "mod":"mate_hspestapi",
    "class":"HSPestAPIMate",
    "opt":{
      "conn" : {"base" : "http://hs.jinong.co.kr:8080/hsict/pest/"}
    }
  },
  "dsmate": {
    "mod":"mate_hscdtp",
    "class":"HSCDTPMate",
    "opt":{
      "conn":{"port": 10010},
      "sleeptime": 53,
      "gatewaykey" : "JN-01"
    }
  },
  "children": []
}');

insert into gate(id, name) values('44e1c889-d6ab-4cac-b910-c8fb4e79a967', '우리술담금조관리');
insert into couple(id, gateid, name, config) values('ce85387c-2fda-48f7-a14b-f229d5a88770', 
'44e1c889-d6ab-4cac-b910-c8fb4e79a967', '우리도가담금조',
'{
  "id": "ce85387c-2fda-48f7-a14b-f229d5a88770",
  "name" : "우리도가담금조",
  "ssmate": {
    "id":"7d151935-c2a0-469d-8bde-49699680dd76",
    "side":"ss",
    "mod":"mate_rrwdb",
    "class":"RRWDBMate",
    "opt":{
      "conn" : {"host" : "localhost", "user" : "root", "password" : "jinong#0801!", "db" : "iot_rrw"}
    }
  },
  "dsmate": {
    "id":"1679124b-505a-4f66-a3c2-a4968032df6e",
    "side":"ds",
    "mod":"mate_kdrrw",
    "class":"KDRRWMate",
    "opt":{
      "conn":{"host": "", "port": 8825}
    }
  },
  "children": [{"id" : "1", "dk" : "EB0132", "dt": "gw", "children" :[
      {"id" : "3", "dk" : 1, "dt": "nd", "children" : [
          {"id" : "4", "dk" : "[1, 0]", "dt": "sen"},
          {"id" : "5", "dk" : "[1, 1]", "dt": "sen"},
          {"id" : "6", "dk" : "[1, 2]", "dt": "sen"}
      ]},
      {"id" : "13", "dk" : 2, "dt": "nd", "children" : [
          {"id" : "14", "dk" : "[2, 0]", "dt": "sen"},
          {"id" : "15", "dk" : "[2, 1]", "dt": "sen"},
          {"id" : "16", "dk" : "[2, 2]", "dt": "sen"}
      ]},
      {"id" : "23", "dk" : 3, "dt": "nd", "children" : [
          {"id" : "24", "dk" : "[3, 0]", "dt": "sen"},
          {"id" : "25", "dk" : "[3, 1]", "dt": "sen"},
          {"id" : "26", "dk" : "[3, 2]", "dt": "sen"}
      ]}
  ]}]
}');

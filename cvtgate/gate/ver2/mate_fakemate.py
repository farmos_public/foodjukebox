#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc. All right reserved.
#
# Fakemate driver
#

import time
import random
import pymysql

from imports import *
from dinfo import DevInfo
from mate import Mate, ThreadMate, DevType

'''
option : {
    "conn" : {"host" : "localhost", "user" : "root",
              "password" : "pass", "db" : "db"}
}

devinfo = [{
    "id" : "1", "dk" : "", "dt": "gw", "children" : [{
        "id" : "101", "dk" : '', "dt": "nd", "children" : [
            {"id" : "102", "dk" : '0', "dt": "sen"}
        ]
    }]
}]

'''

class FAKEMate(ThreadMate):
    def readmsg(self):

        copt = option["conn"]
        conn = pymysql.connect(host=copt["host"], user=copt["user"], password=copt["password"], db=copt["db"])
        curs = conn.cursor()
        dataid = []

        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])
        obsblk.setobservation(nd["id"], 0, StatCode.READY)
        devdict = {}
        
        for dev in nd["children"]:
            if dev["dk"].isdigit() is True :
                dataid.append(int(dev["dk"]))
                devdict[dev["dk"]] = dev["id"]
            else :
                obs = random.randrange(1, 100)
                print("obs",dev["dk"], obs)
                obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)
                
                
        sql = "select data_id, avg(nvalue) from observations where data_id in %s and obs_time between date_sub(now(),interval 1440 minute) and date_sub(now(),interval 1435 minute) group by data_id" 
        curs.execute(sql,[dataid])
        data = curs.fetchall()

        for row in data:
            devid = devdict[str(row[0])]
            obs = row[1]
            print("obs",devid, obs)
            obsblk.setobservation(devid, self.getvalue(devid, obs), StatCode.READY)


        self._msgq.append(obsblk)

if __name__ == "__main__":
    option = {"conn" : {"host" : "localhost", "user" : "root", "password" : "password", "db" : "db"}}
    devinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : '0', "dt": "sen"},
                {"id" : "4", "dk" : '1', "dt": "sen"},
                {"id" : "5", "dk" : 'a', "dt": "sen"}
            ]
        }]
    }]

    mate = FAKEMate(option,devinfo,"1", None)
    mate2 = Mate(option, devinfo, "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(10)
    mate.stop()
    print("mate stopped")


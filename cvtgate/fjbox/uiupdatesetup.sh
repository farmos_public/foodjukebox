#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
cd ../..
SHELL_PATH=`pwd -P`

cd cvtgate/fjbox


echo -e "\n monit stop \n"
sudo service monit stop

echo -e "\n fui stop \n"
sudo /etc/init.d/fui stop

echo -e "\n fcore stop \n"
sudo ${SHELL_PATH%}/scripts/fcore stop

echo -e "\n cvtgate stop \n"
sudo ${SHELL_PATH%}/scripts/cvtgate stop


ret=$(python3 fjbox-serial.py)
sleep 5s
ret=$(python3 fjbox-serial.py)
temp=($ret)
devcode=${temp[0]}
serial=${temp[1]}
hashkey=${temp[2]}
echo  -e $devcode, $serial, $hashkey

equipment=${temp[4]}

echo  -e 'equipment : ' $equipment

connServer=${temp[6]}

sshport=${temp[7]}
uiport=${temp[8]}
mqttport=${temp[9]}

echo -e 'SSH Connect Port : ' $sshport, ' UI Connect Port : ' $uiport, ' MQTT Connect Port : '$mqttport
echo -e 'Connect Server :' $connServer ;

userid=${temp[19]}
userpw=${temp[20]}

echo -e 'userid :' $userid , 'userpw :' $userpw

echo -e '\n\n Change Conf file \n'

rm ${SHELL_PATH%}'/common_api/conf/config.json'
rm ${SHELL_PATH%}'/cvtgate/gate/conf/cpmng.conf'
sudo mv config.json /${SHELL_PATH%}'/common_api/conf/config.json'
sudo mv cpmng.conf /${SHELL_PATH%}'/cvtgate/gate/conf/cpmng.conf'


echo -e '\n\n Change menu set \n'


cat << "EOF" > "menu_pc.json"
["data/reference","data/correlation","data/statistics","research","device","setting/process","setting/farm","control/auto/0","control/manual/nutrient-supply","setting/timespan","rule/template","add/template","modify/timespan"]
EOF

cat << "EOF" > "menu_m.json"
["device/state","setting/place","setting/farm"]
EOF

rm ${SHELL_PATH%}'/common_api/api/public/menu_m.json'
sudo mv menu_m.json ${SHELL_PATH%}'/common_api/api/public/menu_m.json'

rm ${SHELL_PATH%}'/common_api/api/public/menu_pc.json'
sudo mv menu_pc.json ${SHELL_PATH%}'/common_api/api/public/menu_pc.json'

if [ $? -eq 0 ];then
    echo "setup success"
else
    echo "setup fail"
fi


echo -e '\n\n Change database set \n'
mysql -u farmos -pfarmosv2@ farmos -e "update fields set name = '외부' where id = '0'";


sudo monit
sudo monit reload
sudo /etc/init.d/fui start

#sudo reboot

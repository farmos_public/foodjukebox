# FoodJukeBox

## Introduction

본 프로젝트는 보급형 식물재배 시스템 구동을 위한 소프트웨어이다.
이 소프트웨어는 오픈소스 개방형 제어기 FarmOS V2를 기반으로 동작하는 소프트웨어이며, FarmOS V2 스크립트에 대한 설명은 다음의 프로젝트 홈페이지를 참고한다.

* [FarmOSV2](https://gitlab.com/JiNong_Public/farmosv2-script)


## 개발 환경
* python3를 사용한다.
* raspberry pi에서 구동하여 raspberry pi os가 설치되어 있어야한다.


## 작동 방법
* 식물 재배 시스템과 라즈베리 카메라가 연결되어 있어야 하며, 시리얼 및 카메라, ssh를 사용할 수 있도록 raspberry pi 설정을 변경해야 한다.
* 다음의 코드로 foodjukebox 소프트웨어를 다운받고 실행한다.
```
git clone https://gitlab.com/farmos_public/foodjukebox.git
cd foodjukebox
``` 
* 보급형 식물 재배 시스템 구동을 위한 소프트웨어의 기반이 되는 제어기 프로그램을 먼저 설치해야한다. scripts 폴더의 install3.sh 파일을 실행한다.
```
sudo scripts/install3.sh
```
* 보급형 식물 재배 시스템 구동을 위한 소프트웨어인 setup 폴더의 setup.sh 파일을 실행한다.
```
sudo setup/setup.sh
```


## 디렉토리 별 담당

| 디렉토리 | 담당 |
| ------ | ------ |
| common_api | 보급형 식물재배 시스템 소프트웨어 UI에 필요한 api가 있다. |
| cvtgate | 보급형 식물재배 시스템 소프트웨어 내부의 센서 값을 수집하고 구동기를 제어할 수 있다. |
| fcore | 보급형 식물재배 시스템 소프트웨어의 작동 규칙을 실행한다. |
| scripts | 보급형 식물재배 시스템 소프트웨어의 기반이 되는 제어기를 설치하고 프로그램이 잘 동작하는지 체크한다. |
| setup | 보급형 식물재배 시스템 소프트웨어 프로그램을 설치한다. |
| updates | 보급형 식물재배 시스템 소프트웨어 업데이트를 관리한다. |


## 주의사항
현재 지원하는 시리얼번호는 100번대 이하, 1000번 ~ 1499번까지이다. 이 외의 번호도 추후 사용 예정이다.

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import pymysql
import traceback

from imports import *
from mate import Mate

'''
option : {
    "conn" : {"host" : "localhost", "user" : "root", 
              "password" : "pass", "db" : "db"}
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw"}, 
    {"id" : "3", "dk" : "1", "dt": "nd"}, 
    {"id" : "4", "dk" : "0", "dt": "sen"}, 
    {"id" : "5", "dk" : "1", "dt": "act"}
]
'''

class RRWDBMate(Mate):
    def __init__(self, option, devinfo, coupleid, logger):
        self._conn = None
        self._cur = None
        super(RRWDBMate, self).__init__(option, devinfo, coupleid, logger)

    def start(self, writecb):
        self.connect()
        super(RRWDBMate, self).start(writecb)

    def connect(self):
        if self._cur:
            try:
                self.cur.close()
            except Exception as ex:
                self._logger.warn("fail to close")
        if self._conn:
            try:
                self._conn.close()
            except Exception as ex:
                self._logger.warn("fail to close")
        copt = self._option["conn"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"], password=copt["password"], db=copt["db"])
        self._cur = self._conn.cursor()

    def close(self):
        try:
            self._cur.close()
        except Exception as ex:
            self._logger.warn("fail to close")
        try:
            self._conn.close()
        except Exception as ex:
            self._logger.warn("fail to close")

    def stop(self):
        super(RRWDBMate, self).stop()

    def writeblk(self, blk):
        print(blk.getcontent())
        if BlkType.isobservation(blk.gettype()):
            try:
                query = "insert into raw_value(measure_dt, value, sensor_id) values(%s, %s, %s)"
                squery = "update sensor set status = %s where sensor_id = %s"
                content = blk.getcontent()
                print("content", content)
                tm = content['time']
                del content['time']
                for sid, val in content.items():
                    print("value", val, val[0], val[1])
                    params = [tm, val[0], sid]
                    #params = [str(tm), row['value'], row['id']]
                    self._logger.info("insert data : " + str(params))
                    self._cur.execute(query, params)
                    #params = ['on', row['id']]
                    #self._cur.execute(squery, params)
                self._conn.commit()
            except Exception as ex:
                self._logger.warn("except : " + str(ex))
                print(traceback.format_exc())
                #self.close()
                self.connect()
        elif BlkType.isresponse(blk.gettype()):
            print("response")
        else:
            # error log
            print("wrong type messsage", blk)
            return False
        return True



if __name__ == "__main__":
    option = {"conn" : {"host" : "localhost", "user" : "root", "password" : "pass", "db" : "db"}}
    devinfo = [{"id" : "2", "dk" : "1", "dt": "gw"}, {"id" : "3", "dk" : "1", "dt": "nd"}, {"id" : "4", "dk" : "0", "dt": "sen"}, {"id" : "5", "dk" : "1", "dt": "act"}]
    rrw = RRWDBMate(option, devinfo)
    mate = Mate({}, [])
    rrw.start(mate.writeblk)

    time.sleep(3)
    rrw.stop()

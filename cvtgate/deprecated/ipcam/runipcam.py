#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 JiNong, Inc. 
# All right reserved.
#

import sys
import json
import datetime
import time
import subprocess
import traceback
import requests
import logging
import logging.handlers

GWID = 300
RETRY = 3
OPTFILE = "ipcam.conf"

log = logging.getLogger("jnipcam")
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
log.addHandler(streamHandler)

formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
fileHandler = logging.handlers.RotatingFileHandler("logs/runipcam.log", maxBytes=200000, backupCount=5)
fileHandler.setFormatter(formatter)
log.addHandler(fileHandler)

def getoption(fname):
    fp = open(fname, 'r')
    option = json.loads(fp.read())
    fp.close()
    return option

def takepicture(camera):
    nowfmt = datetime.datetime.now().strftime("%H:%M")
    print(nowfmt)
    if nowfmt in camera["saveTimes"]:
        ret = {"id" : camera["id"], "fname" : None, "tm": nowfmt}
        log.info("time(" + nowfmt + ") is matched : " + str(camera))
        nowstr = datetime.datetime.now().strftime("%Y%m%d%H%M")
        basecmd = '/usr/bin/avconv -y -loglevel debug -rtsp_transport tcp -i "rtsp://'
        if "idpw" in camera:
            basecmd = basecmd + camera["idpw"] + "@"
        else:
            basecmd = basecmd + "admin:jinong30#@"
        #basecmd = basecmd + "admin:jinong30#@"
        fname = camera["id"] + "-" + nowstr + ".png"
        cmd = basecmd + camera["ip"] + '/11" ' + camera["option"] + " " + fname
        for i in range(RETRY):
            print(cmd)
            if subprocess.call(cmd, shell=True) == 0:
                ret["fname"] = fname
                break
            else:
                log.warn("fail to execute avconv : " + str(i) + " " + fname)
        return ret
    return None

def sendpicturewithretry(camid, files, values):
    #URL = "http://220.90.128.179:8585/interface/IPCamera/savePicture/" + str(GWID) + "/"
    URL = "http://scs.cwg.go.kr/interface/IPCamera/savePicture/" + str(GWID) + "/"

    print(URL, values)
    ret = False
    for i in range(RETRY):
        try:
            if files is None:
                res = requests.post(URL + camid, data=values)
            else:
                res = requests.post(URL + camid, files=files, data=values)
            if res.status_code == 200:
                ret = True
                break
            else:
                log.warn("fail to send a picture : status code " + str(res.status_code) + " " + str(values))
        except Exception as ex:
            log.warn("fail to send a picture : " + str(ex) + " " + str(values))
    return ret

def sendpicture(ret):
    if ret is None:
        return 

    if ret["fname"] is None:
        log.warn("fail to take a picture : " + str(ret))
        values = {"saveTime": ret["tm"], "saveImg": "", "result": "F"}
        sendpicturewithretry(ret["id"], None, values)
    else:
        values = {"saveTime": ret["tm"], "saveImg": ret["fname"], "result": "S"}
        files = {"upload_file": open(ret["fname"], "rb")}
        sendpicturewithretry(ret["id"], files, values)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage : python runipcam.py GWID")
        sys.exit(2)

    GWID = sys.argv[1]

    try:
        option = getoption(OPTFILE)

        for cam in option["cameras"]:
            sendpicture(takepicture(cam))

        log.info("runipcam executed.")

    except Exception as err:
        try:
            exc_info = sys.exc_info()
        finally:
            traceback.print_exception(*exc_info)
            del exc_info


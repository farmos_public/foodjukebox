#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2016 JiNong, Inc. 
# All right reserved.
#

import sys
import json
import datetime
import time
import subprocess
import requests
import logging
import logging.handlers

GWID = 300
OPTFILE = "ipcam.conf"

log = logging.getLogger("jnipcamoption")
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
log.addHandler(streamHandler)

formatter = logging.Formatter('[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s > %(message)s')
fileHandler = logging.handlers.RotatingFileHandler("logs/checkoption.log", maxBytes=200000, backupCount=5)
fileHandler.setFormatter(formatter)
log.addHandler(fileHandler)

def writeoption(option):
    with open(OPTFILE, 'w') as outfile:
        json.dump(option, outfile)

def getoption():
    #URL = "http://220.90.128.179:8585/interface/IPCamera/getOption/" + str(GWID)
    URL = "http://scs.cwg.go.kr/interface/IPCamera/getOption/" + str(GWID)

    try:
        res = requests.get(URL)
        if res.status_code == 200:
            ret = res.json()
            if ret["result"] == "S":
                writeoption(ret["option"])
            else:
                log.warn("fail to get option : result is not success : " + str(ret))
        else:
            log.warn("fail to get option : status code " + str(res.status_code))
    except Exception as ex:
        log.warn("fail to get option : " + str(ex))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage : python checkoption.py GWID")
        sys.exit(2)

    GWID = sys.argv[1]

    try:
        getoption()
    except Exception as ex:
        log.warn("fail to get : " + str(ex))

    log.info("checkoption executed.")

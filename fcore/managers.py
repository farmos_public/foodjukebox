#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object

import json
import pymysql
import psutil
import time
import socket
import traceback
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime, timedelta

import util
import processors
from code import VarCode, RetCode
from result import ProcResult
from mblock import Request, CmdCode, Notice, NotiCode
from variable import Variable

class TimeSpanManager:
    def __init__(self, datamng, option, logger, farmid = None):
        self._datamng = datamng
        self._farmid = farmid
        self._timespans = {}
        self._day = time.localtime(time.time()).tm_yday

    def getsuntime(self, fldid = 0):
        coord = self._datamng.loadcoordinates(fldid, self._farmid)
        return util.SunTime(coord[1], coord[0])

    def updatesuntimespan(self, ts, fldid=0):
        suntime = self.getsuntime(fldid)
        sunrise = suntime.getsunrise()
        sunset = suntime.getsunset()
        #self._datamng.updatesuntime(fldid, sunrise, sunset)

        if ts["configuration"]["timing"] == "sun":
            ts["times"] = {"#sunrise" : Variable(sunrise), "#sunset" : Variable(sunset)}
            for part in ts["parts"]:
                if part["type"] == "rise+":
                    part["to"] = sunrise + int(part["value"])
                elif part["type"] == "rise-":
                    part["to"] = sunrise - int(part["value"])
                elif part["type"] == "rise":
                    part["to"] = sunrise
                elif part["type"] == "set+":
                    part["to"] = sunset + int(part["value"])
                elif part["type"] == "set-":
                    part["to"] = sunset - int(part["value"])
                elif part["type"] == "set":
                    part["to"] = sunset
                else:
                    part["to"] = int(part["to"])
                print("update suntimespan", part)
        return ts

    def updatetimespans(self):
        if self._day == time.localtime(time.time()).tm_yday:
            return 

        # It needs to recalculate once a day.
        self._day = time.localtime(time.time()).tm_yday

        for tmp, ts in self._timespans.items():
            self.updatesuntimespan(ts, tmp[1])

    def addtimespan(self, tsid, fldid, timespan):
        print("addtimespan", tsid, fldid, timespan)
        self._timespans[(tsid, fldid)] = timespan
        if timespan["configuration"]["timing"] == "sun":
            self.updatesuntimespan(timespan)
        elif timespan["configuration"]["timing"] == "fixed":
            for p in timespan["parts"]:
                p["to"] = int(p["to"])

        else: 
            self._logger.warn("Unknown timing : " + str([tsid, fldid, timespan["configuration"]["timing"]]))

        timespan["tvar"] = {"tsidx": Variable(0)}
        for th in timespan["threshold"]:
            timespan["tvar"]["#" + th["id"]] = Variable() 

    def loadtimespan(self, updated):
        rows = self._datamng.loadtimespan(updated, self._farmid)
        for row in rows:
            if row['field_id'] < 0:
                continue

            self.addtimespan(row['id'], row['field_id'], json.loads(row['timespan']))

    def getcurrentnsec(self):
        now = datetime.now()
        return (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()

    def getcurrentindex(self, tsid, fldid):
        """
            return : current timespan index, current epoch, times
        """
        nsec = self.getcurrentnsec()

        if tsid == 0:
            return (0, nsec, {})

        timespan = self._timespans[(tsid, fldid)]

        idx = -1
        for i in range(len(timespan["parts"])):
            if timespan["parts"][i]["to"] > nsec:
                print("found timespan index : ", i, timespan["parts"][i]["to"], nsec)
                idx = i
                break

        if idx < 0:
            self._logger.warn("Fail to get current timespan for tsid : " + str(tsid) + " " + str(nsec))
            return (None, None, {})

        if "times" in timespan:
            return (idx, nsec, timespan["times"])
        else:
            return (idx, nsec, {})

    def getthresholdvalue(self, part, thd, idx, current):
        opt = thd["timeoption"]
        if thd["linetype"] == "fixed":
            return opt[idx]["to"]

        # monotone, straight 는 같다고 취급한다. (작은 차이는 무시한다. :p)
        if idx == 0:
            return opt[idx]["to"]
        else:
            ratio  = (old_div((current * 1.0 - part[idx-1]["to"]), (part[idx]["to"] - part[idx-1]["to"])))
            return opt[idx-1]["to"] + (opt[idx]["to"] - opt[idx-1]["to"]) * ratio


    def getthresholds(self, tsid, fldid, idx, current):
        if tsid == 0:
            return {}

        timespan = self._timespans[(tsid, fldid)]
        ret = {}
        timespan["tvar"]["tsidx"].setvalue(idx)
        for thd in timespan["threshold"]:
            timespan["tvar"]["#" + thd["id"]].setvalue(self.getthresholdvalue(timespan["parts"], thd, idx, current))
        print("thresholds", timespan["tvar"])
        return timespan["tvar"]

class DeviceManager:
    def __init__(self, datamng, option, logger, farmid = None):
        self._datamng = datamng
        self._farmid = farmid
        self._devices = {}

    def loaddevices(self, updated):
        rows = self._datamng.loaddevices(updated, self._farmid)
        for row in rows:
            if row["deleted"] == 1:
                if row["id"] in self._devices:
                    del self._devices[row["id"]]
            else:
                self._devices[row["id"]] = row
        print("devices ", self._devices)

    def getdevice(self, devid):
        print("getdevice", devid)
        return self._devices[devid]

    def getdevices(self):
        return self._devices.keys()

if __name__ == '__main__':
    pass

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object

import json
import pymysql
import psutil
import time
import socket
import traceback
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime, timedelta

import util
from code import VarCode, RetCode
from result import ProcResult
from mblock import Request, CmdCode, Notice, NotiCode
from variable import Variable

class DataManager:
    INTDATA = 0
    DEVDATA = 1
    USRDATA = 2
    RULDATA = 3
    EXTDATA = 4
    CONFDATA = 6
    HWDATA = 7

    DEVDATABASE = 10000000
    DEVCONFBASE = 60000000

    PATH = "../backup/image/"

    def __init__(self, option, logger):
        self._option = option
        self._logger = logger
        self._data = {}

    def connect(self):
        pass

    def close(self):
        pass

    def loadserial(self, farmid = None):
        pass
  
    def loaddata(self, farmid = None):
        self.loaddatameta(farmid)
        self.loaddatavalues(farmid)
        #print("Data loaded : ", self._data)

    def loaddatavalues(self, farmid = None):
        pass

    def loaddatameta(self, farmid = None):
        pass

    def updatedata(self, dataid, value, farmid = None):
        pass

    def updatedatavariable(self, dataid, variable, farmid = None):
        pass

    def writedata(self, farmid = None):
        pass

    def isimgdata(self, dataid, farmid = None):
        if (self.getdatagroup(dataid) == DataManager.DEVDATA) and ((dataid % 100) == 10):
            return True
        return False

    def reloadimgdata(self, dataid, count, farmid = None, fldid = None):
        pass

    def reloaddata(self, dataid, count, farmid = None, fldid = None):
        pass

    def loadpostfixes(self, farmid = None):
        pass

    def getdataids(self, farmid = None):
        return self._data.keys()

    def getdata(self, dataid, count=1, farmid = None, fldid = None):
        pass

    def getdatagroup(self, dataid):
        return int(dataid / DataManager.DEVDATABASE)

    def getdataidforfield(self, fldid, outcode):
        #print ("getdataidforfield", fldid, outcode, fldid * 100000 + outcode)
        return fldid * 100000 + outcode

    def getdataidfordev(self, devid, outcode):
        #print ("getdataidfordev", devid, outcode)
        return DataManager.DEVDATABASE + devid * 100 + outcode

    def getdataidfordevconf(self, devid, outcode):
        #print ("getdataidfordevconf", devid, outcode)
        return DataManager.DEVCONFBASE + devid * 100 + outcode

    def isprocresult(self, dataid, farmid = None):
        if DataManager.RULDATA == self.getdatagroup(dataid) and dataid % 100 == 0:
            return True
        else:
            return False

    def isnewdevstatus(self, devid, tm, farmid = None):
        dataid = self.getdataidfordev(devid, 0)
        datum = self._data[dataid]
        if tm > datum.getobserved():
            self._logger.warn("data_id(" + str(dataid) + ") is old.")
            return False
        return True

    def loadcoordinates(self, fldid = 0, farmid = None):
        pass

    def updatesuntime(self, fldid, sunrise, sunset, farmid = None):
        pass

    def loadtimespan(self, updated, farmid = None):
        pass

    def loaddevices(self, updated, farmid = None):
        pass

    def loadappliedrules(self, updated, sched, ruleid, farmid = None):
        pass

    def writealert(self, alertret):
        pass

    def select(self, query, params):
        pass

class DBManager(DataManager):
    def __init__(self, option, logger):
        super(DBManager, self).__init__(option, logger)
        self._data = {}
        self.connect()

    def connect(self):
        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], cursorclass=pymysql.cursors.DictCursor, use_unicode=True, charset='utf8')
        self._cur = self._conn.cursor()

    def close(self):
        self._cur.close()
        self._conn.close()

    def loadserial(self, farmid = None):
        query = "select uuid from gate_info"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            return row['uuid']

    def loadpostfixes(self, farmid = None):
        pass

    def loaddatavalues(self, farmid = None):
        query = "select * from current_observations"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            if self.isimgdata(row['data_id']):
                continue
            if row['data_id'] not in self._data:
                self._logger.warning("Weird data id : " + str(row))
            else:
                v = self._data[row['data_id']]
                v.setfromdb(row)
                self._data[row['data_id']] = v

    def loaddatameta(self, farmid = None):
        query = "select * from dataindexes"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            if row['id'] not in self._data:
                if self.isimgdata(row['id']):
                    v = Variable(vid=row['id'], vcode=VarCode.IMG)
                else:
                    v = Variable(vid=row['id'])
                if v.setmeta(row) is False:
                    self._logger.warn("Data critical info is not correct. " + str(row))
                self._data[row['id']] = v

    def updatedata(self, dataid, value, farmid = None):
        if dataid in self._data:
            self._data[dataid].setvalue(value)
        else:
            self._logger.info("Set a new data : " + str(dataid) + " " + str(value))
            self._data[dataid] = Variable(value, dataid)

    def updatedatavariable(self, dataid, variable, farmid = None):
        if dataid in self._data:
            self._data[dataid].setvalue(variable.getvalue(), variable.getobserved())
        else:
            self._logger.info("Set a new data : " + str(dataid) + " " + str(variable))
            self._data[dataid] = variable

    def writedata(self, farmid = None):
        uquery = "update current_observations set nvalue = %s, obs_time = %s, modified_time = %s where data_id = %s"
        iquery = "insert into observations(data_id, obs_time, nvalue) values (%s, %s, %s)"
        for dataid, variable in self._data.items():
            if variable.isupdated():
                print("writedata", dataid, variable, variable.getobserved())
                try:
                    self._cur.execute(uquery, [variable.getlastestvalue(), variable.getobserved(), variable.getmodified(), dataid])
                    if self.isprocresult(dataid) == False:
                        self._cur.execute(iquery, [dataid, variable.getobserved(), variable.getlastestvalue()])
                    variable.applied()
                except Exception as ex:
                    self._logger.warn("Fail to write data : " + str([dataid, variable]) + str(ex))
        self._conn.commit()

    def reloadimgdata(self, dataid, count, farmid = None, fldid = None):
        query = "select data_id, obs_time, path, meta from gallery where data_id = %s order by obs_time desc limit %s"
        self._cur.execute(query, [dataid, count])

        rows = self._cur.fetchall()
        for row in rows:
            meta = '{}' if row['meta'] is None else row['meta']
            row['nvalue'] = {'path' : DataManager.PATH + row['path'], 'meta' : json.loads(meta)}

        if self._data[dataid].setimagesfromdb(rows, self._option) is False:
            self._logger.warn("Fail to relod previous images : " + str(dataid) + "," + str(count))

    def reloaddata(self, dataid, count, farmid = None, fldid = None):
        query = "select * from observations where data_id = %s order by obs_time desc limit %s"
        self._cur.execute(query, [dataid, count])
        if self._data[dataid].setpreviousfromdb(self._cur.fetchall()) is False:
            self._logger.warn("Fail to relod previous data : " + str(dataid) + "," + str(count))

    def getdailystats(self, dataid):
        query = "select min(nvalue) minv, max(nvalue) maxv, avg(nvalue) avgv, count(nvalue) cntv, sum(nvalue) sumv from observations where data_id = %s and DATE(obs_time) = CURDATE()"
        self._cur.execute(query, [dataid])
        return self._cur.fetchone()

    def getdata(self, dataid, count=1, farmid = None, fldid = None):
        if dataid in self._data:
            if self.isimgdata(dataid):
                if self._data[dataid].getvalue() is None:
                    self.reloadimgdata(dataid, count)
            else:
                if self._data[dataid].getsize() < count:
                    print ("reload", dataid, count)
                    self.reloaddata(dataid, count)
            return self._data[dataid]

        """
        if self.isimgdata(dataid):
            print ("isimgdata2", dataid)
            self._data[dataid] = Variable(vid=dataid, vcode=VarCode.IMG)
            self.reloadimgdata(dataid, count)
        """
        self._logger.warn("Unknown data : " + str(dataid))
        return None

    def loadcoordinates(self, fldid = 0, farmid = None):
        _DEF_LONGITUDE = 127.0541365
        _DEF_LATITUDE = 37.27593497

        dataids = [fldid * 100000 + 1, fldid * 100000 + 2]
        query = "select data_id, nvalue from current_observations where data_id in %s"
        self._cur.execute(query, [dataids])
        ret = [_DEF_LATITUDE, _DEF_LONGITUDE]
        for row in self._cur.fetchall():
            if row['data_id'] == 1:
                ret[0] = row['nvalue']
            if row['data_id'] == 2:
                ret[1] = row['nvalue']
        return ret

    def updatesuntime(self, fldid, sunrise, sunset, farmid = None):
        self.updatedata(fldid * 100000 + 9, sunrise, farmid)
        self.updatedata(fldid * 100000 + 10, sunset, farmid)

    def loadtimespan(self, updated, farmid = None):
        query = "select * from core_timespan where unix_timestamp(updated) >= %s and field_id in (select id from fields where deleted = 0)"
        self._cur.execute(query, [updated])
        return self._cur.fetchall()

    def loaddevices(self, updated, farmid = None):
        query = "select a.id, a.name, a.coupleid, a.gateid, a.deleted, b.id nodeid, a.control from devices a, (select * from devices where devindex is null) b where unix_timestamp(a.updated) > %s and a.nodeid = b.nodeid and a.deleted = 0"
        self._cur.execute(query, [updated])
        return self._cur.fetchall()

    def _schedquery(self, sched, query):
        if sched:
            return query + " and sched <> 0"
        else:
            return query + " and sched = 0"

    def getnumberofrules(self, sched, guide, farmid = None):
        if "disallows" in guide and len(guide["disallows"]) > 0:
            query = "select count(*) cnt from core_rule_applied where id not in %s and advanced < 10"
            self._cur.execute(self._schedquery(sched, query), [guide["disallows"]])
        elif "allows" in guide and len(guide["allows"]) > 0:
            query = "select count(*) cnt from core_rule_applied where id in %s and advanced < 10"
            self._cur.execute(self._schedquery(sched, query), [guide["allows"]])
        else:
            query = "select count(*) cnt from core_rule_applied where advanced < 10"

            try:
                self._cur.execute(self._schedquery(sched, query), [])
            except:    # 임시 코드 : sched 가 없는 경우를 위한 하위호환성
                self._cur.execute(query, [])
        rows = self._cur.fetchone()
        return rows["cnt"]

    def loadappliedrules(self, updated, sched, guide, farmid = None):
        if "disallows" in guide and len(guide["disallows"]) > 0:
            query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and id not in %s and advanced < 10"
            self._cur.execute(self._schedquery(sched, query), [updated, guide["disallows"]])
        elif "allows" in guide and len(guide["allows"]) > 0:
            query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and id in %s and advanced < 10"
            self._cur.execute(self._schedquery(sched, query), [updated, guide["allows"]])
        else:
            query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and advanced < 10"

            try:
                self._cur.execute(self._schedquery(sched, query), [updated])
            except:    # 임시 코드 : sched 가 없는 경우를 위한 하위호환성
                self._cur.execute(query, [updated])
        return self._cur.fetchall()

    def _getalert(self, params):
        tmp = ""
        for idx, key in [(4, 'hwid'), (5, 'device_id'), (6, 'field_id')]:
            if params[idx] is None:
                tmp = tmp + " and " + key + " is NULL"
            else:
                tmp = tmp + " and " + key + " = " + str(params[idx])

        query = "select id, mute_time, (mute_time < now()) as expired from alert where farm_id = %s and alert_code = %s and grade = %s " + tmp + " order by occured_time desc limit 1"
        try:
            self._cur.execute(query, params[:3])
            row = self._cur.fetchone()
        except Exception as ex:
            self._logger.warn("Fail to check alert : " + str(params) + str(ex))
            return None
        return row

    def writealert(self, alertret):
        params = alertret.getwparams()
        alert = self._getalert(params)
        try:
            # 경보가 존재. 확인이 안되었거나 뮤트중이라면 카운트 증가.
            if alert and (alert["mute_time"] is None or alert["expired"] == 0):
                query = "update alert set occured_count = occured_count + 1 where id = %s"
                self._cur.execute(query, [alert["id"]])
            else:
                query = "insert into alert(farm_id, alert_code, grade, info, hwid, device_id, field_id) values (%s, %s, %s, %s, %s, %s, %s)"
                self._cur.execute(query, params)
        except Exception as ex:
            self._logger.warn("Fail to write alert : " + str(params) + str(ex))
        self._conn.commit()

    def select(self, query, params):
        try:
            self._cur.execute(query, params)
            rows = self._cur.fetchall()
        except Exception as ex:
            self._logger.warn("Fail to select : " + query + str(params) + str(ex))
            return None
        return rows

class DPManager(DataManager):
    def __init__(self, option, logger):
        super(DPManager, self).__init__(option, logger)
        self._data = {}
        self._postfixes = {}
        self.connect()

    def connect(self):
        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], cursorclass=pymysql.cursors.DictCursor, use_unicode=True, charset='utf8')
        self._cur = self._conn.cursor()

        copt = self._option["dp"]
        self._dpconn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], cursorclass=pymysql.cursors.DictCursor, use_unicode=True, charset='utf8')
        self._dpcur = self._dpconn.cursor()

    def close(self):
        self._cur.close()
        self._conn.close()
        self._dpcur.close()
        self._dpconn.close()

    def getfarmids(self):
        query = "select farm_id from farm where data_node = %s"
        self._dpcur.execute(query, [self._option["datanode"]])
        farmids = []
        for row in self._dpcur.fetchall():
            farmids.append(row['farm_id'])
        return farmids

    def loadpostfixes(self, farmid):
        query = "select id from fields where farm_id = %s and id != 0 and deleted = 0"
        self._dpcur.execute(query, [farmid])
        self._postfixes = {}
        for row in self._dpcur.fetchall():
            self._postfixes[row["id"]] = "_" + str(farmid) + "_" + str(row["id"])

        query = "select field_id, season_id, postfix from season where deleted = 0 and farm_id = %s and season_id in (select max(season_id) from season where farm_id = %s group by field_id)"
        self._dpcur.execute(query, [farmid, farmid])
        for row in self._dpcur.fetchall():
            self._postfixes[row["field_id"]] = row["postfix"]
        print("loadpostfixes", farmid, self._postfixes)

    def getpostfix(self, dataid, farmid, fldid):
        return self._postfixes[fldid]

    def loadserial(self, farmid = None):
        # DP에서는 serial 을 이용해서 정보를 포워딩 하지 않는다.
        # 추후 DP에서 경보이외의 값을 생성한다면 수정되어야 한다.
        return None

    def loaddatavalues(self, farmid = None):
        query = "select * from current_observations where farm_id = %s"
        self._cur.execute(query, [farmid])
        for row in self._cur.fetchall():
            if self.isimgdata(row['data_id']):
                continue
            if row['data_id'] not in self._data:
                self._logger.warning("Weird data id : " + str(row))
            else:
                v = self._data[row['data_id']]
                v.setfromdb(row)
                self._data[row['data_id']] = v

    def loaddatameta(self, farmid = None):
        query = "select * from dataindexes where farm_id = %s"
        self._dpcur.execute(query, [farmid])
        for row in self._dpcur.fetchall():
            if row['id'] not in self._data:
                if self.isimgdata(row['id']):
                    v = Variable(vid=row['id'], vcode=VarCode.IMG)
                else:
                    v = Variable(vid=row['id'])
                if v.setmeta(row) is False:
                    self._logger.warn("Data critical info is not correct. " + str(row))
                self._data[row['id']] = v

    def updatedata(self, dataid, value, farmid = None):
        if dataid in self._data:
            self._data[dataid].setvalue(value)
        else:
            self._logger.info("Set a new data : " + str(dataid) + " " + str(value))
            self._data[dataid] = Variable(value, dataid)

    def updatedatavariable(self, dataid, variable, farmid = None):
        if dataid in self._data:
            self._data[dataid].setvalue(variable.getvalue(), variable.getobserved())
        else:
            self._logger.info("Set a new data : " + str(dataid) + " " + str(variable))
            self._data[dataid] = variable

    def writedata(self, farmid = None):
        uquery = "update current_observations set nvalue = %s, obs_time = %s, modified_time = %s where data_id = %s and farm_id = %s"
        for dataid, variable in self._data.items():
            if variable.isupdated():
                print("writedata", dataid, variable, variable.getobserved())
                try:
                    self._cur.execute(uquery, [variable.getlastestvalue(), variable.getobserved(), variable.getmodified(), dataid, farmid])
                    if self.isprocresult(dataid):
                        # 프로세스 처리 결과는 굳이 이력으로 남기지는 않는다.
                        pass
                    else: 
                        # 다른 종류의 모든 데이터는 원래 저장이 되어야 하지만, DP에서는 우선 처리하지 않는다.
                        # iquery = "insert into observations" + self.getpostfix(dataid, farmid) + "(farm_id, data_id, obs_time, nvalue) values (%s, %s, %s, %s)"
                        #self._cur.execute(iquery, [farmid, dataid, variable.getobserved(), variable.getlastestvalue()])
                        pass
                    variable.applied()
                except Exception as ex:
                    self._logger.warn("Fail to write data : " + str([dataid, variable]) + str(ex))
        self._conn.commit()

    def reloadimgdata(self, dataid, count, farmid = None, fldid = None):
        query = "select data_id, obs_time, path, meta from gallery" + self.getpostfix(dataid, farmid, fldid) + " where data_id = %s order by obs_time desc limit %s"
        self._cur.execute(query, [dataid, count])

        rows = self._cur.fetchall()
        for row in rows:
            meta = '{}' if row['meta'] is None else row['meta']
            row['nvalue'] = {'path' : DataManager.PATH + row['path'], 'meta' : json.loads(meta)}

        if self._data[dataid].setimagesfromdb(rows, self._option) is False:
            self._logger.warn("Fail to relod previous images : " + str(dataid) + "," + str(count))

    def reloaddata(self, dataid, count, farmid = None, fldid = None):
        query = "select * from observations" + self.getpostfix(dataid, farmid, fldid) + " where data_id = %s order by obs_time desc limit %s"
        self._cur.execute(query, [dataid, count])
        if self._data[dataid].setpreviousfromdb(self._cur.fetchall()) is False:
            self._logger.warn("Fail to relod previous data : " + str(dataid) + "," + str(count))

    def getdata(self, dataid, count=1, farmid = None, fldid = None):
        if dataid in self._data:
            if self.isimgdata(dataid):
                if self._data[dataid].getvalue() is None:
                    self.reloadimgdata(dataid, count, farmid, fldid)
            else:
                if self._data[dataid].getsize() < count:
                    print ("reload", dataid, count)
                    self.reloaddata(dataid, count)
            return self._data[dataid]

        """
        if self.isimgdata(dataid):
            print ("isimgdata2", dataid)
            self._data[dataid] = Variable(vid=dataid, vcode=VarCode.IMG)
            self.reloadimgdata(dataid, count)
        """
        self._logger.warn("Unknown data : " + str(dataid))
        return None

    def loadcoordinates(self, fldid = 0, farmid = None):
        _DEF_LONGITUDE = 127.0541365
        _DEF_LATITUDE = 37.27593497

        dataids = [fldid * 100000 + 1, fldid * 100000 + 2]
        query = "select data_id, nvalue from current_observations where data_id in %s and farm_id = %s"
        self._cur.execute(query, [dataids, farmid])
        ret = [_DEF_LATITUDE, _DEF_LONGITUDE]
        for row in self._cur.fetchall():
            if row['data_id'] == 1:
                ret[0] = row['nvalue']
            if row['data_id'] == 2:
                ret[1] = row['nvalue']
        return ret

    def loadtimespan(self, updated, farmid = None):
        query = "select * from core_timespan where unix_timestamp(updated) >= %s and field_id in (select id from fields where deleted = 0) and farm_id = %s"
        self._dpcur.execute(query, [updated, farmid])
        return self._dpcur.fetchall()

    def loaddevices(self, updated, farmid = None):
        query = "select a.id, a.name, a.coupleid, a.gateid, a.deleted, b.id nodeid, 1 as control from devices a, (select * from devices where devindex is null and farm_id = %s) b where unix_timestamp(a.updated) > %s and a.nodeid = b.nodeid and a.deleted = 0 and a.farm_id = %s"
        self._dpcur.execute(query, [farmid, updated, farmid])
        return self._dpcur.fetchall()

    def _schedquery(self, sched, query):
        if sched:
            return query + " and sched <> 0"
        else:
            return query + " and sched = 0"

    def getnumberofrules(self, sched, guide, farmid = None):
        if "disallows" in guide and len(guide["disallows"]) > 0:
            query = "select count(*) cnt from core_rule_applied where id not in %s and farm_id = %s and advanced >= 10"
            self._dpcur.execute(self._schedquery(sched, query), [guide["disallows"], farmid])
        elif "allows" in guide and len(guide["allows"]) > 0:
            query = "select count(*) cnt from core_rule_applied where id in %s and farm_id = %s and advanced >= 10"
            self._dpcur.execute(self._schedquery(sched, query), [guide["allows"], farmid])
        else:
            query = "select count(*) cnt from core_rule_applied where farm_id = %s and advanced >= 10"
            self._dpcur.execute(self._schedquery(sched, query), [farmid])
        rows = self._cur.fetchone()
        return rows["cnt"]

    def loadappliedrules(self, updated, sched, guide, farmid = None):
        if "disallows" in guide and len(guide["disallows"]) > 0:
            query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and id not in %s and farm_id = %s and advanced >= 10"
            self._dpcur.execute(self._schedquery(sched, query), [updated, guide["disallows"], farmid])
        elif "allows" in guide and len(guide["allows"]) > 0:
            query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and id in %s and farm_id = %s and advanced >= 10"
            self._dpcur.execute(self._schedquery(sched, query), [updated, guide["allows"], farmid])
        else:
            query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and farm_id = %s and advanced >= 10"
            self._dpcur.execute(self._schedquery(sched, query), [updated, farmid])
        return self._dpcur.fetchall()

    def _getalert(self, params):
        tmp = ""
        for idx, key in [(4, 'hwid'), (5, 'device_id'), (6, 'field_id')]:
            if params[idx] is None:
                tmp = tmp + " and " + key + " is NULL"
            else:
                tmp = tmp + " and " + key + " = " + str(params[idx])

        query = "select id, mute_time, (mute_time < now()) as expired from alert where farm_id = %s and alert_code = %s and grade = %s " + tmp + " order by occured_time desc limit 1"
        try:
            self._cur.execute(query, params[:3])
            row = self._cur.fetchone()
        except Exception as ex:
            self._logger.warn("Fail to check alert : " + str(params) + str(ex))
            return None
        return row

    def writealert(self, alertret):
        params = alertret.getwparams()
        alert = self._getalert(params)
        try:
            # 경보가 존재. 확인이 안되었거나 뮤트중이라면 카운트 증가.
            if alert and (alert["mute_time"] is None or alert["expired"] == 0):
                query = "update alert set occured_count = occured_count + 1 where id = %s"
                self._cur.execute(query, [alert["id"]])
            else:
                query = "insert into alert(farm_id, alert_code, grade, info, hwid, device_id, field_id) values (%s, %s, %s, %s, %s, %s, %s)"
                self._cur.execute(query, params)
        except Exception as ex:
            self._logger.warn("Fail to write alert : " + str(params) + str(ex))
        self._conn.commit()

if __name__ == '__main__':
    pass

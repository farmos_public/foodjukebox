from lib.mate import Logger
from .. import *

import paho.mqtt.client as mqtt
from collections import deque

from .. import *


def message_handler(mate:Mate):
    def msg_handler(client, obj, block):        
        parser = MessageParser(mate._logger)
        message = parser.generate_message(block)
        mate._writeobs(message)
    return msg_handler

class MessageHandler:
    def __init__(self, mate:Mate) -> None:
        self.mate :DSMate = mate
    
    def on_message(self, client, obj, block):
        parser = MessageParser(self.mate._logger)
        message = parser.generate_message(block)
        self.mate._writeobs(message)

class Client():    
    def __init__(self, option, logger) -> None:
        self.conn = option["conn"]
        self.svc = option["mqtt"]["svc"]
        self.host = self.conn["host"]
        self.port = self.conn["port"]
        self.keepalive = self.conn["keepalive"]
        self.client = mqtt.Client()
        self.logger:Logger = logger        
    
    def connect(self):
        try:
            self.client.connect(self.host, self.port, self.keepalive)
            self.client.loop_start()
            self.isconnected = True
        except Exception as e:
            self.logger.warn("fail to connect mqttserver : " + str(e))
            self.isconnected = False
        return self.isconnected
    
    def subscribe(self, topic, qos=2):
        self.client.subscribe(topic, qos=qos)
        self.client.loop_start()
        
    def on_message(self, message_handler):
        self.client.on_message = message_handler
    
    def close(self):
        self.client.disconnect()
        
class MessageParser:
    def __init__(self, logger) -> None:
        self.logger = logger

    def generate_message(self, block):
        msg :Observation = MBlock.load(block.payload)
        topic_elements = block.topic.split('/')
        TYPE_INDEX = 3
        GW_ID_INDEX = 1
        
        if msg is None:
            self.logger.info("Fail to parse a message. " + str(block.payload))
            return msg
        
        if topic_elements[TYPE_INDEX] == "obs" and BlkType.isobservation(msg.gettype()):
            msg.setextra("gateway-id", topic_elements[GW_ID_INDEX])
            return msg
        
        self.logger.info("Topic is not matched. Check [3]. " + str(topic_elements))
        return None    

class Subscriber():
    def __init__(self, client, logger) -> None:
        self.client = client                
        self.logger = logger
        
    def subscribe_topics(self, topics:list):        
        for topic in topics:
            try:
                self.client.subscribe(topic=topic, qos=2) # qos = 2
                self.logger.info("subscribe : " + topic)
            except Exception as e:
                self.logger.error("subscribing error : " + e)
                
class TopicGenerator():
    @staticmethod
    def generate_topic(service_name, message_type, device_info):
        topic_list = []
        for root in device_info:
            assert root["dt"] == "gw" and "children" in root and "id" in root
            gateway_id = root["id"]
            couple_id = root["dk"]
            for nd in root['children']:
                node_id = nd["id"]
                topic = service_name + "/" + couple_id + "/"+ str(gateway_id) + "/" + message_type + "/" + str(node_id) 
                topic_list.append(topic)
        return topic_list
        

class MqttForwardMate(DSMate):
    """
    Observation forwarder.

    Args:
        DSMate ([type]): [description]
    """
    def __init__(self, option, devinfo, coupleid, quelist):
        super(MqttForwardMate, self).__init__(option, devinfo, coupleid, quelist)
        self._msgq = deque()
        

    def connect(self):
        super(MqttForwardMate, self).connect()
        self.client = Client(self._option, self._logger)
        self.client.on_message(message_handler(self))
        self.client.connect()

        topics = TopicGenerator.generate_topic("cvtgate", "obs", self._devinfo)
        subscriber = Subscriber(self.client, self._logger)  
        subscriber.subscribe_topics(topics)                  
        
    def close(self):
        super(MqttForwardMate, self).close()
        
    def matestart(self):
        super(MqttForwardMate, self).matestart()
        self.connect()

    def matestop(self):
        super(MqttForwardMate, self).matestop()
        self.client.close()

if __name__ == "__main__":
    pass 

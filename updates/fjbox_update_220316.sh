#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

mysql -u farmos -pfarmosv2@ farmos -e "update core_rule_applied set outputs = '{\"data\":[{\"name\":\"총캐노피면적\",\"outputs\":\"#area\",\"unit\":\"㎠\",\"outcode\":71},{\"name\":\"총생중량\",\"outputs\":\"#weight\",\"unit\":\"g\",\"outcode\":72},{\"name\":\"색상\",\"outputs\":\"#h\",\"unit\":\"\",\"outcode\":73},{\"name\":\"채도\",\"outputs\":\"#s\",\"unit\":\"\",\"outcode\":74},{\"name\":\"명도\",\"outputs\":\"#v\",\"unit\":\"\",\"outcode\":75}]}' where id = 18;"
mysql -u farmos -pfarmosv2@ farmos -e "update core_rule_applied set constraints = '{\"target\":\"field\",\"devices\":[{\"class\":\"actuator\",\"type\":\"camera/level0\",\"desc\":\"카메라를 선택해주세요.\",\"inputs\":{\"key\":\"#cam\",\"codes\":[0,10],\"names\":[\"stat\",\"img\"]},\"name\":\"카메라\",\"deviceid\":18}],\"data\":[{\"key\":\"#planttype\",\"name\":\"작물종류\",\"desc\":\"작물종류를 선택해주세요.\",\"idfmt\":\"[0-9][0-9][0-9][0-9]03\",\"dataid\":100003},{\"key\":\"#numofplant\",\"name\":\"주수\",\"desc\":\"정식한 작물의 수량을 선택해주세요.\",\"idfmt\":\"[0-9][0-9][0-9][0-9]08\",\"dataid\":100008}]}' where id = 18;"
mysql -u farmos -pfarmosv2@ farmos -e "update core_rule_applied set inputs = '[{\"key\":\"#cam.stat\",\"dataid\":10001800,\"count\":1},{\"key\":\"#cam.img\",\"dataid\":10001810,\"count\":1},{\"key\":\"#planttype\",\"dataid\":100003,\"count\":1},{\"key\":\"#numofplant\",\"dataid\":100008,\"count\":1}]' where id = 18;"

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function

from builtins import str
from builtins import object
from past.utils import old_div

import json

from datetime import datetime, timedelta
from collections import deque

from code import RetCode, VarCode

class Variable(object):
    """
        TSVariable can not have previous values.
    """
    def __init__(self, value=None, vid=None, size=1, vcode=VarCode.NORM):
        self._nvalue = value
        self._meta = {}
        self._critical = None

        if vcode == VarCode.LIST or vcode == VarCode.TSPAN:
            if isinstance(value, list):
                #A list variable can not have previous values.
                size = 1
            else:
                print ("WARN::value is not a list.")

        if vcode == VarCode.DICT:
            if isinstance(value, dict):
                #A dictionary variable can not have previous values.
                size = 1
            else:
                print ("WARN::value is not a dict.")

        if vcode == VarCode.HIST and size < 2:
            #A historical variable can not have previous values.
            size = 1
            print ("INFO::size is not a bigger number. " + str(size))

        now = datetime.now()
        self._modified = now
        self._observed = now
        self._updated = False
        self._id = vid
        self._size = size
        self._vcode = vcode

        # latest is on left side
        if vcode == VarCode.HIST or vcode == VarCode.IMG:
            self._nvalues = deque(maxlen=size)
            self._nvalues.append(value)
            self._observeds = deque(maxlen=size)
            self._observeds.append(now)
        else:
            self._nvalues = None
            self._observeds = None
   
    def __repr__(self):
        return "{}{}({},{})".format(self.__class__.__name__, str(self._id), str(self._nvalue), str(self._size))

    def getid(self):
        return self._id

    def setid(self, newid):
        self._id = newid

    def getsize(self):
        return self._size

    def setvalue(self, value, observed=datetime.now()):
        if self._vcode == VarCode.IMG: 
            # image data could not be updated
            if 'sftp' in self._nvalue:
                self.putimage()
            return

        #if value is None:
        #    return

        if value != self._nvalue:
            self._nvalue = value
            self._modified = observed

        self._observed = observed
        self._updated = True

        if self._vcode == VarCode.HIST:
            self._nvalues.appendleft(value)
            self._observeds.appendleft(observed)

    def setfromdb(self, row):
        """
         should be used when it loaded from db
        """
        if self._observed is not None and self._nvalue == row['nvalue'] and (row['obs_time'] - self._observed).total_seconds() < 60:
            # No need to update
            return

        self._id = row['data_id']
        self._nvalue = row['nvalue']
        self._modified = row['modified_time']
        self._observed = row['obs_time']
        self._updated = False

        if self._vcode == VarCode.HIST or self._vcode == VarCode.IMG:
            self._nvalues.appendleft(self._nvalue)
            self._observeds.appendleft(self._observed)

    def setimagesfromdb(self, rows, option=None):
        """
         Rows should be ordered by obs_time descend
         The latest row is the first.
         DP has option. Local does not have option.
        """
        if self._vcode != VarCode.IMG:
            return False

        if self._size < len(rows):
            self._size = len(rows)
            self._nvalues = deque(maxlen=len(rows))
            self._observeds = deque(maxlen=len(rows))

        for row in rows: 
            val = row['nvalue']
            if option and 'remote' in option:
                val['rbase'] = option['remote']['path']
                val['sftp'] = option['sftp']
            self._nvalues.append(val)
            self._observeds.append(row['obs_time'])

        self._nvalue = self._nvalues[0]
        self._modified = self._observeds[0]
        self._observed = self._observeds[0]

        if option and 'remote' in option:
            self.getimage()

        return True

    def getimage(self):
        fpath = self._nvalue['rbase'] + "/" + self._nvalue['path']
        self._nvalue['sftp'].get(fpath, self._nvalue['path'])

    def putimage(self):
        tpath = self._nvalue['rbase'] + "/" + self._nvalue['path']
        self._nvalue['sftp'].put(self._nvalue['path'], tpath)

    def setmeta(self, meta):
        self._meta = meta
        self._critical = None
        if "critical_info" in meta and meta["critical_info"] is not None:
            try:
                self._critical = json.loads(meta["critical_info"])
            except Exception as ex:
                print ("Exception", ex)
                return False 
        return True

    def getmeta(self, key):
        if key in self._meta:
            return self._meta[key]
        return None

    def getcriticalinfo(self):
        return self._critical

    def setpreviousfromdb(self, rows):
        """
         Rows should be ordered by obs_time descend
         The latest row is the first.
        """
        print ("setprevious", self._vcode, rows)
        if self._vcode == VarCode.NORM:
            self._vcode = VarCode.HIST
            self._nvalues = deque(maxlen=self._size)
            self._observeds = deque(maxlen=self._size)
        elif self._vcode == VarCode.IMG:
            return False
        elif self._vcode != VarCode.HIST:
            return False

        if self._size < len(rows):
            self._size = len(rows)
            self._nvalues = deque(maxlen=len(rows))
            self._observeds = deque(maxlen=len(rows))

        for row in rows: 
            print ("setprevious row", row)
            self._nvalues.append(row['nvalue'])
            self._observeds.append(row['obs_time'])

        return True

    def getvalue(self, tsidx = 0):
        if self._vcode == VarCode.TSPAN and isinstance(self._nvalue, list):
            return self._nvalue[tsidx]
        if self._size > 1:
            return list(self._nvalues)
        else:
            return self._nvalue

    def getlastestvalue(self, tsidx = 0):
        if self._vcode == VarCode.TSPAN and isinstance(self._nvalue, list):
            return self._nvalue[tsidx]
        if self._size > 1:
            return self._nvalues[0]
        else:
            return self._nvalue

    def inc(self):
        self._nvalue = self._nvalue + 1
        if self._size > 1:
            self._nvalues = map(lambda x: x + 1, self._nvalues) 

    def getlastvalue(self, tsidx = 0):
        if self._vcode == VarCode.TSPAN and isinstance(self._nvalue, list):
            return self._nvalue[tsidx]
        else:
            return self._nvalue

    def getobserved(self):
        return self._observed

    def getmodified(self):
        return self._modified

    def isupdatedtoday(self):
        today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        return self._observed >= today

    def getdelta(self, idx=0, n=1):
        if self._vcode == VarCode.HIST:
            if self._size > idx + n and idx + n < len(self._nvalues):
                return self._nvalues[idx] - self._nvalues[idx+n]
            elif self._size > idx + 1 and idx + 1 < len(self._nvalues):
                return self._nvalues[idx] - self._nvalues[-1]
        return 0

    def gettimedelta(self, idx=0, n=1):
        if self._vcode == VarCode.HIST:
            if self._size > idx + n and idx + n < len(self._observeds):
                print ("timedelta", idx, idx+n,  self._observeds[idx] - self._observeds[idx+n])
                return self._observeds[idx] - self._observeds[idx+n]
            elif self._size > idx + 1 and idx + 1 < len(self._observeds):
                print ("timedelta", idx, -1,  self._observeds[idx] - self._observeds[-1])
                return self._observeds[idx] - self._observeds[-1]
        return timedelta(minutes=1)

    def getdifferential(self, n=1):
        dt = self.gettimedelta(n=n).total_seconds()
        return old_div(self.getdelta(n=n), dt if dt != 0 else 60)

    def getintegration(self, n=1):
        integ = 0
        for i in range(n):
            integ = integ + self.getdelta(i)
        return integ

    def isupdated(self):
        return self._updated
        
    def applied(self):
        self._updated = False

    def getaverage(self):
        if self._vcode == VarCode.HIST and self._size > 1:
            temp = list(self._nvalues)
            return old_div(float(sum(temp)), len(temp))
        return self._nvalue

    def gethisotricalvalues(self):
        if self._vcode == VarCode.HIST or self._vcode == VarCode.IMG:
            return list(self._nvalues)
        return None

if __name__ == '__main__':
    import time
    import json

    v = Variable()
    row = {'data_id': 100, 'nvalue' : 3, 'obs_time' : datetime.now(), 'modified_time' : datetime.now()}
    v.setfromdb(row)
    print("isupdatedtoday", v.isupdatedtoday())
    print("getvalue", v.getvalue())
    rows = [{'data_id': 100, 'nvalue' : 3, 'obs_time' : datetime.now(), 'modified_time' : datetime.now()}]
    time.sleep(5)
    rows.insert(0, {'data_id': 100, 'nvalue' : 8, 'obs_time' : datetime.now(), 'modified_time' : datetime.now()})
    v.setpreviousfromdb(rows)
    print("values", v.getaverage())
    print("timedelta, delta, diff, integration ", v.gettimedelta(), v.getdelta(), v.getdifferential(), v.getintegration())

    rows = [{'data_id': 110, 'nvalue': {'path' : '47/1582083075016.png', 'mata' : json.loads('{}')}, 'obs_time' : datetime.now()}]
    time.sleep(5)
    rows.insert(0, {'data_id': 110, 'nvalue' : {'path' : '47/1582084558873.png', 'mata' : json.loads('{}')}, 'obs_time' : datetime.now()})
    v = Variable(vcode=VarCode.IMG)
    v.setimagesfromdb(rows)
    print("getvalue", v.getvalue())


#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

import sys
import time
import json
import pymysql
import traceback
import argparse
from lib.mblock import Notice, NotiCode
from datetime import date, timedelta

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

import statsutil

class DailyStats:
    def __init__(self, fname):
        self._util = statsutil.StatsUtil(fname)
        self._cur = self._util.getcursor()

    def execute(self, datestr, dataids=None):
        if datestr is None:
            datestr = (date.today()-timedelta(days=1)).isoformat() 
            isupdate = True
        else:
            isupdate = False

        if dataids is None:
            dataids = self._util.loaddataid()

        stats = self.getstats(datestr, dataids)
        if stats is None:
            # Send some warning
            print ("Fail to get stats.")
            return

        print("stats", stats)
        self._util.writestats(stats, isupdate)
           
    def getstats(self, datestr, dataids):
        diff = self.getdiffstats(datestr, dataids)
        daily = self.getdailystats(datestr, dataids)
        if diff and daily:
            return diff + daily
        return None

    def getdiffstats(self, datestr, dataids):
        qry = "select stddev(diff) ds from ( SELECT COALESCE( ( SELECT nvalue FROM observations mi "\
              "WHERE mi.obs_time > m.obs_time and mi.data_id = %s and mi.obs_time between %s and %s "\
              "ORDER BY obs_time limit 1), nvalue) - nvalue AS diff "\
              "FROM observations m where data_id = %s and obs_time between %s and %s ORDER BY obs_time) d"

        sd = datestr + ' 00:00:00'
        ed = datestr + ' 23:59:59'
        stats = [] 
        for did in dataids:
            params = [did, sd, ed, did, sd, ed] 
            print ("diffparam", params)
            try:
                self._cur.execute(qry, params)
                for row in self._cur.fetchall(): 
                    stats.append ([did, datestr, row['ds'], 26])
            except Exception as ex:
                print ("Fail to make diff statistics : " + str(ex))
                print (traceback.format_exc())
        return stats

    def getdailystats(self, datestr, dataids):
        qry = "SELECT data_id, count(*) vc, avg(nvalue) va, max(nvalue) vx, min(nvalue) vn, std(nvalue) vs "\
              "from observations "\
              "WHERE data_id in %s and obs_time between %s and %s group by data_id"
        try:
            print ("params", datestr, dataids)
            self._cur.execute(qry, [dataids, datestr + ' 00:00:00', datestr + ' 23:59:59'])
            stats = [] 
            for row in self._cur.fetchall():
                stats.append ([row['data_id'], datestr, row['vc'], 21])
                stats.append ([row['data_id'], datestr, row['va'], 22])
                stats.append ([row['data_id'], datestr, row['vx'], 23])
                stats.append ([row['data_id'], datestr, row['vn'], 24])
                stats.append ([row['data_id'], datestr, row['vs'], 25])
            return stats
        except Exception as ex:
            print ("Fail to make data statistics : " + str(ex))
            print (traceback.format_exc())
            return None

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='make statistics for sensor observations daily.')
    parser.add_argument('datestr', nargs='?', default=None, help='date to make statistics YYYY-MM-DD')
    parser.add_argument('--ids', metavar='DATA_ID', type=int, nargs='*', help='dataids (default: None)')

    args = parser.parse_args()
    print (args)
    dstats = DailyStats("conf/util.conf")
    dstats.execute(args.datestr, args.ids)

#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

echo -e '\n\n 0. update'
apt-get update --allow-releaseinfo-change

echo -e '\n\n 1. Make AP'

echo -e '\n\n 1-1. git clone rpi'
cd /home/pi

if [ -d rpi-ipset ] ; then
    echo -e "CHECK UPDATE"
else
    git clone https://gitlab.com/farmos_public/rpi-ipset.git
    npm install --prefix ./rpi-ipset --unsafe-perm
fi

apt install hostapd -y
systemctl unmask hostapd
systemctl enable hostapd
apt install dnsmasq -y

cat /dev/null > /home/pi/farmosv2-script/updates/log/temp

chmod 777 /etc/dnsmasq.conf
chmod 777 /etc/dhcpcd.conf

if [ -e /etc/dhcpcd.conf.orig -o -e dnsmasq.conf.orig ] ; then
    echo -e 'CHECK ORIGIN FILE'
else
    cp -r /etc/dhcpcd.conf /etc/dhcpcd.conf.orig
    cp -r /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
fi

if [ ! -d /etc/hostapd ] ; then
    mkdir /etc/hostapd
fi

cat << "EOF" > "/etc/hostapd/hostapd.conf"
country_code=GB
interface=wlan0
EOF

statustxt=$(awk '/FJDevice Serial/' /home/pi/farmosv2-script/cvtgate/fjbox/status.txt)
fjboxserial=${statustxt:18}

echo "ssid=FoodJukeBox${fjboxserial}" >> "/etc/hostapd/hostapd.conf"

cat << "EOF" >> "/etc/hostapd/hostapd.conf"

hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=fjb0000#
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP

EOF


cat << "EOF" > "/etc/dhcpcd.conf"
hostname
clientid
persistent
option rapid_commit
option domain_name_servers, domain_name, domain_search, host_name
option classless_static_routes
option interface_mtu
require dhcp_server_identifier
slaac private
interface wlan0
    static ip_address=10.2.0.1/24
    nohook wpa_supplicant
EOF


cat << "EOF" > "/etc/dnsmasq.conf"
interface=wlan0
dhcp-range=10.2.0.2,10.2.0.20,255.255.255.0,24h
domain=wlan
address=/gw.wlan/10.2.0.1
EOF

echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' >> "/etc/default/hostapd"

chmod 777 "/etc/hostapd/hostapd.conf"



echo -e '\n\n 2. UPDATE PUMP RULE \n'

#mysql -u farmos -pfarmosv2@ farmos -e "update core_rule_applied set deleted = 1 where name = '시간지정 관수제어'; update core_rule_applied set deleted = 1 where name = '주기지정 관수제어';"

#mysql -u farmos -pfarmosv2@ farmos -e "insert core_rule_applied(name, updated, field_id, used, constraints, configurations, inputs, controllers, outputs, sched, groupname , template_id) values('주기지정 관수제어', now(), 1, 1, '{\"target\":\"field\",\"devices\":[{\"class\":\"actuator\",\"type\":\"switch/level1\",\"inputs\":{\"key\":\"#inpump\",\"codes\":[0,90,91]},\"outputs\":\"#pump\",\"desc\":\"분무 펌프를 선택해주세요.\",\"deviceid\":10,\"name\":\"펌프\"}],\"data\":[]}', '{\"basic\":[{\"key\":\"#workperiod\",\"name\":\"분무주기(분)\",\"value\":[120,30,30,30,30,30,120],\"type\":\"ts_float\",\"description\":\"분무 주기(분)을 설정합니다.\"},{\"key\":\"#worktime\",\"name\":\"분무시간(초)\",\"value\":[20,20,20,20,20,20,20],\"type\":\"ts_float\",\"description\":\"분무 시간(초)을 설정합니다.\"}],\"advanced\":[{\"key\":\"priority\",\"name\":\"우선순위\",\"value\":2,\"minmax\":[0,5],\"description\":\"룰의 우선순위\"},{\"key\":\"period\",\"name\":\"기간\",\"value\":60,\"description\":\"룰의 작동주기\"}],\"timespan\":{\"id\":4,\"used\":[true,true,true,true,true,true,true]}}', '[{\"key\":\"#inpump0\",\"dataid\":10001000,\"count\":1},{\"key\":\"#inpump90\",\"dataid\":10001090,\"count\":1},{\"key\":\"#inpump91\",\"dataid\":10001091,\"count\":1}] ','{\"processors\":[{\"type\":\"mod\",\"mod\":\"fjbox.periodicspray\",\"outputs\":[\"#cmd\",\"#flow\",\"#accflow\"]}]}', '{\"req\":[{\"cmd\":\"#cmd\",\"pnames\":[\"hold-time\"],\"params\":[\"#worktime\"],\"targets\":[\"#pump\"]}],\"dev\":[{\"name\":\"최근분무량\",\"outputs\":\"#flow\",\"outcode\":90,\"targets\":\"#pump\",\"unit\":\"mL\"},{\"name\":\"일간누적분무량\",\"outputs\":\"#accflow\",\"outcode\":91,\"targets\":\"#pump\",\"unit\":\"mL\"}]}' , 0, '스위치제어' ,66);"
#mysql -u farmos -pfarmosv2@ farmos -e "insert core_rule_applied(name, updated, field_id, used, constraints, configurations, inputs, controllers, outputs, sched, groupname , template_id) values('시간지정 관수제어', now(), 1, 0, '{\"target\":\"field\",\"devices\":[{\"class\":\"actuator\",\"type\":\"switch/level1\",\"inputs\":{\"key\":\"#inpump\",\"codes\":[0,90,91]},\"outputs\":\"#pump\",\"desc\":\"분무 펌프를 선택해주세요.\",\"deviceid\":10,\"name\":\"펌프\"}],\"data\":[]}', '{\"basic\":[{\"key\":\"#table\",\"name\":\"스케쥴\",\"values\":{\"time\":[29460],\"cmd\":[202],\"worktime\":[30]},\"type\":\"table\",\"columns\":[{\"type\":\"time\",\"name\":\"시간\",\"key\":\"time\"},{\"type\":\"cmd\",\"name\":\"명령\",\"key\":\"cmd\"},{\"type\":\"float\",\"name\":\"작동시간\",\"key\":\"worktime\"}],\"description\":\"스케쥴 제어를 위한 설정을 입력합니다.\"}],\"advanced\":[{\"key\":\"priority\",\"name\":\"우선순위\",\"value\":2,\"minmax\":[0,5],\"description\":\"룰의 우선순위\"},{\"key\":\"period\",\"name\":\"기간\",\"value\":60,\"description\":\"룰의 작동주기\"}],\"timespan\":{\"id\":0,\"used\":[true]}}', '[{\"key\":\"#inpump0\",\"dataid\":10001000,\"count\":1},{\"key\":\"#inpump90\",\"dataid\":10001090,\"count\":1},{\"key\":\"#inpump91\",\"dataid\":10001091,\"count\":1}]','{\"processors\":[{\"type\":\"mod\",\"mod\":\"fjbox.schedulespray\",\"outputs\":[\"#cmd\",\"#worktime\",\"#flow\",\"#accflow\"]}]}','{\"req\":[{\"cmd\":\"#cmd\",\"pnames\":[\"hold-time\"],\"params\":[\"#worktime\"],\"targets\":[\"#pump\"]}],\"dev\":[{\"name\":\"최근분무량\",\"outputs\":\"#flow\",\"outcode\":90,\"targets\":\"#pump\",\"unit\":\"mL\"},{\"name\":\"일간누적분무량\",\"outputs\":\"#accflow\",\"outcode\":91,\"targets\":\"#pump\",\"unit\":\"mL\"}]}' , 1, '스위치제어' ,37);"

#mysql -u farmos -pfarmosv2@ farmos -e "insert dataindexes(id, rule_id, name, unit, sigdigit, field_id) select 30000000 + id * 10000 + 100, id,  concat(id ,'번 룰 1번 컨트롤 처리현황'), ' ', 0, 1 from core_rule_applied where name = '시간지정 관수제어' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert dataindexes(id, rule_id, name, unit, sigdigit, field_id) select 30000000 + id * 10000 + 100, id,  concat(id ,'번 룰 1번 컨트롤 처리현황'), ' ', 0, 1 from core_rule_applied where name = '주기지정 관수제어' and deleted = 0;"

#mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 30000000 + id * 10000 + 100, now(), NULL, now(), NULL  from core_rule_applied where name = '시간지정 관수제어' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 30000000 + id * 10000 + 100, now(), NULL, now(), NULL  from core_rule_applied where name = '주기지정 관수제어' and deleted = 0;"


echo -e '\n\n 3. UPDATE PWVC RULE \n'


#mysql -u farmos -pfarmosv2@ farmos -e "insert core_rule_applied(name, field_id, used, constraints, configurations, inputs, controllers, outputs, sched, groupname , template_id) values('수분부족분', 1, 1, '{ \"target \": \"field \", \"devices \":[{ \"class \": \"sensor \", \"type \": \"temperature-sensor \", \"desc \": \"내부온도센서를 선택해주세요. \", \"inputs \":{ \"key \": \"#intemp \", \"codes \":[0,1]}, \"name \": \"내부온도 \", \"deviceid \":2},{ \"class \": \"sensor \", \"type \": \"humidity-sensor \", \"desc \": \"내부습도센서를 선택해주세요. \", \"inputs \":{ \"key \": \"#inhum \", \"codes \":[0,1]}, \"name \": \"내부습도 \", \"deviceid \":3}], \"data \":[]}', '{ \"basic \":[], \"advanced \":[{ \"key \": \"priority \", \"name \": \"우선순위 \", \"value \":2, \"minmax \":[0,5], \"description \": \"룰의 우선순위 \"},{ \"key \": \"period \", \"name \": \"기간 \", \"value \":60, \"description \": \"룰의 작동주기 \"}], \"timespan \":{ \"id \":0, \"used \":[true]}}', '[{ \"key \": \"#intemp0 \", \"dataid \":10000200, \"count \":1},{ \"key \": \"#intemp1 \", \"dataid \":10000201, \"count \":1},{ \"key \": \"#inhum0 \", \"dataid \":10000300, \"count \":1},{ \"key \": \"#inhum1 \", \"dataid \":10000301, \"count \":1}]','{ \"processors \":[{ \"type \": \"eq \", \"eq \": \"1322.8 / (intemp1 + 273.15) * exp(25.22 / (intemp1 + 273.15) * intemp1 - 5.31 * log (intemp1 / 273.15 + 1)) \", \"outputs \":[ \"#PWVC \"]},{ \"type \": \"eq \", \"eq \": \"PWVC - ( inhum1 * PWVC / 100 ) \", \"outputs \":[ \"#HD \"]}]}', '{ \"data \":[{ \"name \": \"PWVC \", \"outputs \": \"#PWVC \", \"unit \": \"g/㎥ \", \"outcode \":22},{ \"name \": \"수분부족분 \", \"outputs \": \"#HD \", \"unit \": \"g/㎥ \", \"outcode \":23}]}' , 0, '특수지표' ,75); "
#mysql -u farmos -pfarmosv2@ farmos -e "insert dataindexes(id, rule_id, name, unit, sigdigit, field_id) select 30000000 + id * 10000 + 22, id, 'PWVC', 'g/㎥', 0, 1 from core_rule_applied where name = '수분부족분' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert dataindexes(id, rule_id, name, unit, sigdigit, field_id) select 30000000 + id * 10000 + 23, id, '수분부족분', 'g/㎥', 0, 1 from core_rule_applied where name = '수분부족분' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert dataindexes(id, rule_id, name, unit, sigdigit, field_id) select 30000000 + id * 10000 + 100, id, concat(id ,'번 룰 1번 컨트롤 처리현황'), ' ', 0, 1 from core_rule_applied where name = '수분부족분' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert dataindexes(id, rule_id, name, unit, sigdigit, field_id) select 30000000 + id * 10000 + 200, id, concat(id ,'번 룰 2번 컨트롤 처리현황'), '', 0, 1 from core_rule_applied where name = '수분부족분' and deleted = 0;"

#mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 30000000 + id * 10000 + 22, now(), NULL, now(), NULL  from core_rule_applied where name = '수분부족분' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 30000000 + id * 10000 + 23, now(), NULL, now(), NULL  from core_rule_applied where name = '수분부족분' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 30000000 + id * 10000 + 100, now(), NULL, now(), NULL  from core_rule_applied where name = '수분부족분' and deleted = 0;"
#mysql -u farmos -pfarmosv2@ farmos -e "insert into current_observations(data_id, obs_time, nvalue, modified_time, source_id) select 30000000 + id * 10000 + 200, now(), NULL, now(), NULL  from core_rule_applied where name = '수분부족분' and deleted = 0;"


echo -e '\n\n 4. CHAMGE UNITS CROP \n'

mysql -u farmos -pfarmosv2@ farmos -e "\
update units set nvalue = 6001 where unit = 'crop' and nvalue = 1;\
update units set nvalue = 6002 where unit = 'crop' and nvalue = 2;\
update units set nvalue = 6003 where unit = 'crop' and nvalue = 3;\
update units set nvalue = 6004 where unit = 'crop' and nvalue = 4;\
update units set nvalue = 6005 where unit = 'crop' and nvalue = 5;\
update units set nvalue = 6006 where unit = 'crop' and nvalue = 6;\
update units set nvalue = 6007 where unit = 'crop' and nvalue = 7;\
update units set nvalue = 7001 where unit = 'crop' and nvalue = 8;\
update units set nvalue = 7002 where unit = 'crop' and nvalue = 9;\
update units set nvalue = 7003 where unit = 'crop' and nvalue = 10;\
update units set nvalue = 7004 where unit = 'crop' and nvalue = 11;\
"

plantValue=$(mysql -u farmos -pfarmosv2@ farmos -e "select nvalue from current_observations where data_id = 100003;")
plantValue=${plantValue:7}
echo -e $plantValue

if [ ${plantValue} != 'NULL' ]; then
    if [ ${plantValue} -ge 1 -a ${plantValue} -le 7 ];then
        mysql -u farmos -pfarmosv2@ farmos -e "update current_observations set nvalue = (select nvalue from current_observations where data_id = 100003) + 6000 where data_id = 100003;"
    elif [ ${plantValue} -ge 8 -a ${plantValue} -le 11 ];then
        mysql -u farmos -pfarmosv2@ farmos -e "update current_observations set nvalue = (select nvalue from current_observations where data_id = 100003) + 7000 where data_id = 100003;"
    fi
fi

echo -e '\n\n 5. UPDATE CRON'
su - pi -c "
echo -e '*/5 * * * * cd /home/pi/farmosv2-script/scripts; ./checkautossh.sh' | crontab
"


echo -e '\n\n 5. DELETE OLD DIRECTORY \n'

cd /home/pi/

if [ ! -d olddirectory ] ; then
    echo -e "already remove"
else
    rm -rf olddirectory temp
fi

sudo /home/pi/rpi-ipset/ipset.sh


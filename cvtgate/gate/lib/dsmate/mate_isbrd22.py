#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
import os
import serial

from datetime import datetime
from enum import IntEnum
from threading import Thread, Lock
from collections import deque

from .. import *

# input
# [['C', 'C', 'O', 'O', 'S', 'S'],['f', 'f', 'f', 'f', 'o', 'o']]
# ['T', 'F']

AUTO = 'A'
MANUAL = 'M'

OPEN = 'O'
CLOSE = 'C'
STOP = 'S'

ON = 'o'
OFF = 'f'

TRUE = 'T'
FALSE = 'F'

TOGGLE = 'T'
SWITCH = 'S'

class FACNStateManager(object):
    def __init__(self, option, logger):
        self._dev = None
        self._option = option
        self._logger = logger
        self._force = False
        opt = option["boards"]
        self._ctrl = [AUTO, TOGGLE, str(opt["T"]), SWITCH, str(opt["S"])] 
        self._ctrl = self._ctrl + [STOP] * opt["T"] * 6 + [OFF] * opt["S"] * 6
        print (self._ctrl)

    # message from INGSYS board
    def setmessage(self, msg):
        ctrl = chr(msg[0])
        if ctrl == AUTO or ctrl == MANUAL:
            self._dev = msg
            if self._force is False:
                self._ctrl[0] = ctrl
        else:
            self._logger.info ("The message is not correct : " + str(msg))

    def getmode(self):
        return self._ctrl[0]

    def setmode(self, control):
        self._ctrl[0] = control
        self._force = True
        print ("new mode is", control)

    def setctrlstate(self, brdidx, chidx, state):
        idx = 5 + brdidx * 6 + chidx
        self._ctrl[idx] = state

    def getctrlstate(self, brdidx, chidx):
        idx = 5 + brdidx * 6 + chidx
        return chr(self._dev[idx])

    def getcommand(self):
        if self._ctrl[0] == AUTO:
            ret = ''.join(self._ctrl)
        else:
            ret = ''.join(self._ctrl[:5])
        print ("command", ret)
        self._force = False
        return ret.encode()

    def getinput(self, idx):
        if self._dev:
            if self._dev[0] == AUTO:
                return self._dev[5 + idx]
            else:
                return self._dev[len(self._ctrl) + idx]

class ISDevice(object):
    def __init__(self, devid, dk, fsmng, logger):
        self._devid = devid
        self._dk = dk
        self._status = StatCode.READY
        self._logger = logger
        self._fsmng = fsmng

    def getid(self):
        return self._devid

    def getstatus(self):
        return self._status

    def setstatus(self, status):
        self._status = status

    def update(self, lapse):
        pass

class ISSensor(ISDevice):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISSensor, self).__init__(devid, dk, fsmng, logger)
        self._obs = None

    def update(self, lapse):
        self._obs = 1 if (self._fsmng.getinput(self._dk) == ord('T')) else 0

    def getvalue(self):
        return self._obs

class ISActuator(ISDevice):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISActuator, self).__init__(devid, dk, fsmng, logger)
        self._opid = 0
        self._lastcmd = None
        self._stable = {StatCode.READY : OFF, StatCode.WORKING : ON, StatCode.MANUAL_WORKING : OFF}
        self._itable = {OFF : StatCode.READY, ON : StatCode.MANUAL_WORKING}

    def getopid(self):
        return self._opid

    def setopid(self, opid):
        self._opid = opid

    def getcontent(self):
        content = {"status" : self.getstatus().value, "opid" : self.getopid()}
        return content

    def setctrlstate(self):
        self._fsmng.setctrlstate(self._dk[0], self._dk[1], self._stable[self.getstatus()])

    def getctrlstate(self):
        return self._fsmng.getctrlstate(self._dk[0], self._dk[1])

class ISNode(ISActuator):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISNode, self).__init__(devid, dk, fsmng, logger)
        # LOCAL 1, REMOTE 2, MANUAL 3
        self._ctable = [AUTO, AUTO, AUTO, MANUAL]

    def getcontrol(self):
        if self._fsmng.getmode() == AUTO:
            return 2
        else:
            return 3

    def getcontent(self):
        content = {"status" : self.getstatus().value, "opid" : self.getopid(), "control": self.getcontrol()}
        return content

    def processrequest(self, request):
        operation = request.getcommand()
        self.setopid(request.getopid())

        if operation == CmdCode.CHANGE_CONTROL:
            ctrl = request.getparams()['control']
            if 1 <= ctrl <= 3:
                self._fsmng.setmode(self._ctable[ctrl])
            else:
                return ResCode.FAIL_NOT_PROPER_PARAM
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND
        self._lastcmd = operation
        return ResCode.OK

class ISSwitch(ISActuator):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISSwitch, self).__init__(devid, dk, fsmng, logger)
        self._rtime = 0

    def getrtime(self):
        return self._rtime

    def getcontent(self):
        content = super(ISSwitch, self).getcontent()
        content["remain-time"] = self.getrtime()
        return content

    def update(self, lapse):
        if self._fsmng.getmode() == AUTO:
            if self._lastcmd == CmdCode.TIMED_ON:
                self._rtime = self._rtime - lapse
                if self._rtime <= 0:
                    self._status = StatCode.READY
                    self._rtime = 0
            self.setctrlstate()
        else:
            self._rtime = 0
            self.setstatus(self._itable[self.getctrlstate()])

    def processrequest(self, request):
        operation = request.getcommand()
        self.setopid(request.getopid())

        if operation == CmdCode.TIMED_ON:
            self._rtime = request.getparams()['hold-time']
            self._status = StatCode.WORKING
        elif operation == CmdCode.ON:
            self._rtime = 0
            self._status = StatCode.WORKING
        elif operation == CmdCode.OFF:
            self._rtime = 0
            self._status = StatCode.READY
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND
        self._lastcmd = operation
        return ResCode.OK

class ISRetractable(ISSwitch):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISRetractable, self).__init__(devid, dk, fsmng, logger)
        self._stable = {StatCode.READY : STOP, StatCode.OPENING : OPEN, StatCode.CLOSING : CLOSE, StatCode.MANUAL_OPENING : STOP, StatCode.MANUAL_CLOSING : STOP}
        self._itable = {STOP : StatCode.READY, OPEN : StatCode.MANUAL_OPENING, CLOSE : StatCode.MANUAL_CLOSING}

    def update(self, lapse):
        if self._fsmng.getmode() == AUTO:
            if self._lastcmd == CmdCode.TIMED_OPEN or self._lastcmd == CmdCode.TIMED_CLOSE:
                self._rtime = self._rtime - lapse
                if self._rtime <= 0:
                    self._status = StatCode.READY
                    self._rtime = 0
            self.setctrlstate()
        else:
            self._rtime = 0
            self.setstatus(self._itable[self.getctrlstate()])

    def processrequest(self, request):
        operation = request.getcommand()
        self.setopid(request.getopid())

        if operation == CmdCode.TIMED_OPEN:
            self._rtime = request.getparams()['time']
            self._status = StatCode.OPENING
        elif operation == CmdCode.TIMED_CLOSE:
            self._rtime = request.getparams()['time']
            self._status = StatCode.CLOSING

        elif operation == CmdCode.OPEN:
            self._status = StatCode.OPENING
            self._rtime = 0
        elif operation == CmdCode.CLOSE:
            self._status = StatCode.CLOSING
            self._rtime = 0

        elif operation == CmdCode.OFF:
            self._rtime = 0
            self._status = StatCode.READY
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND
        self._lastcmd = operation
        return ResCode.OK

class ISRetractable2(ISRetractable):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISRetractable2, self).__init__(devid, dk, fsmng, logger)
        self._opentime = dk[2][0]
        self._closetime = dk[2][1]
        self._pos = None
        if len(dk) == 4:
            self._omargin = dk[3][0]
            self._cmargin = dk[3][1]
        else:
            self._omargin = 3
            self._cmargin = 1

    def update(self, lapse):
        if self._pos is not None:
            if self._status == StatCode.OPENING or self._status == StatCode.MANUAL_OPENING:
                self._pos = self._pos + (lapse / self._opentime)
                if self._pos > 100:
                    self._pos = 100
   
            elif self._status == StatCode.CLOSING or self._status == StatCode.MANUAL_CLOSING:
                self._pos = self._pos - (lapse / self._closetime)
                if self._pos < 0:
                    self._pos = 0

        if self._fsmng.getmode() == AUTO:
            if self._lastcmd in [CmdCode.TIMED_OPEN, CmdCode.TIMED_CLOSE, CmdCode.POSITION]:
                self._rtime = self._rtime - lapse
                if self._rtime <= 0:
                    self._status = StatCode.READY
                    self._rtime = 0

            self.setctrlstate()
        else:
            self._rtime = 0
            self.setstatus(self._itable[self.getctrlstate()])

    def getcontent(self):
        content = super(ISRetractable2, self).getcontent()
        if self._pos:
            content["position"] = int(self._pos * 100)
        return content

    def processrequest(self, request):
        operation = request.getcommand()
        self.setopid(request.getopid())

        if operation == CmdCode.TIMED_OPEN:
            self._rtime = request.getparams()['time']
            self._status = StatCode.OPENING
        elif operation == CmdCode.TIMED_CLOSE:
            self._rtime = request.getparams()['time']
            self._status = StatCode.CLOSING

        elif operation == CmdCode.OPEN:
            self._rtime = 0
            self._status = StatCode.OPENING
        elif operation == CmdCode.CLOSE:
            self._rtime = 0
            self._status = StatCode.CLOSING

        elif operation == CmdCode.POSITION:
            tpos = request.getparams()['position'] / 100.0
            if self._pos is None:
                self._pos = tpos

            if self._pos > tpos: # close
                self._status = StatCode.CLOSING
                self._rtime = self._closetime * (self._pos - tpos) + self._cmargin

            elif self._pos < tpos: # open
                self._status = StatCode.OPENING
                self._rtime = self._opentime * (tpos - self._pos) + self._omargin

        elif operation == CmdCode.OFF:
            self._rtime = 0
            self._status = StatCode.READY
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND
        self._lastcmd = operation
        return ResCode.OK

class ISNutrientSupply2(ISActuator):
    def __init__(self, devid, dk, fsmng, logger):
        super(ISNutrientSupply2, self).__init__(devid, dk, fsmng, logger)
        self._remain = deque()    # [[0, 30]] remain-area-index, remain-time
        self._stable = {StatCode.READY : OFF, StatCode.SUPPLYING : ON, StatCode.MANUAL_SUPPLYING : OFF}
        self._itable = {OFF: StatCode.READY, ON : StatCode.MANUAL_SUPPLYING}
        self._count = self.loadcount() 
        self._manual = None

    def loadcount(self):
        fname = "/tmp/fos-ns-" + self.getid() 
        try:
            with open(fname) as fp:
                return int(fp.readline())
        except:
            return 0

    def increasecount(self):
        self._count = self._count + 1
        fname = "/tmp/fos-ns-" + self.getid() 
        try:
            with open(fname, "w") as fp:
                fp.write(str(self._count))
        except:
            pass

    def getcontent(self):
        content = super(ISNutrientSupply2, self).getcontent()
        if self._status == StatCode.SUPPLYING:
            content["area"] = self._remain[0][0] + 1
            content["remain-time"] = self._remain[0][1]
            content["irrigation-count"] = self._count
        elif self._status == StatCode.MANUAL_SUPPLYING:
            content["area"] = self._manual.index(ON) + 1
        return content

    def getctrlstate(self):
        ret = []
        for i in range(len(self._dk[1])):
            ret.append (self._fsmng.getctrlstate(self._dk[0], self._dk[1][i]))
        return ret

    def update(self, lapse):
        if self._fsmng.getmode() == AUTO:
            if self._status == StatCode.SUPPLYING:
                self._remain[0][1] = self._remain[0][1] - lapse
                if self._remain[0][1] <= 0:
                    self._remain.popleft()
                    self.increasecount()
                    if len(self._remain) == 0:
                        self._status = StatCode.READY

            self.setctrlstate()
        else:
            self._manual = self.getctrlstate()
            self._remain = deque()     # 자동으로 공급하려던 모든 계획을 지운다.
            if ON in self._manual:
                self._status = StatCode.MANUAL_SUPPLYING
            else:
                if self._status == StatCode.MANUAL_SUPPLYING:
                    self.increasecount()
                self._status = StatCode.READY

    def processrequest(self, request):
        operation = request.getcommand()
        self.setopid(request.getopid())

        if operation == CmdCode.SAREA_WATERING:
            rtime = request.getparams()['on-sec']
            abmap = request.getparams()['area-bitmap']
            for i in range(len(self._dk[1])):
                if abmap & (1 << i):
                    self._remain.append([i, rtime])
            if len(self._remain) > 0:
                self._status = StatCode.SUPPLYING

        elif operation == CmdCode.OFF:
            self._remain = deque()
            self._status = StatCode.READY
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND
        self._lastcmd = operation
        return ResCode.OK

    def setctrlstate(self):
        for i in range(len(self._dk[1])):
            if len(self._remain) > 0 and i == self._remain[0][0]:
                self._fsmng.setctrlstate(self._dk[0], self._dk[1][i], self._stable[self.getstatus()])
            else:
                self._fsmng.setctrlstate(self._dk[0], self._dk[1][i], OFF)

class ISBRD22Mate(DSMate):
    _SLEEP = 0.5
    _OBS_TIMEOUT = 50

    def __init__(self, option, devinfo, coupleid, quelist):
        super(ISBRD22Mate, self).__init__(option, devinfo, coupleid, quelist)
        self._fsmng = FACNStateManager(option, self._logger)
        self._devices = self.setupdevices()
        self._lasttime = time.time()
        self._devidsfornoti = []
        self._logger.info("ISBRD22Mate Started.")

    def setupdevices(self):
        devs = {"nd" : None, "sen" : [], "act" : []}
        for gw in self._devinfo:
            for nd in gw["children"]:
                devs["nd"] = ISNode(nd["id"], [], self._fsmng, self._logger)
                for dev in nd["children"]:
                    print (dev)
                    dk = json.loads(dev["dk"])
                    if dev["dt"] == "sen":
                        devs["sen"].append (ISSensor(dev["id"], dk, self._fsmng, self._logger))
                    elif dev["dt"] == "act/switch/level1":
                        devs["act"].append (ISSwitch(dev["id"], dk, self._fsmng, self._logger))
                    elif dev["dt"] == "act/retractable/level1":
                        devs["act"].append (ISRetractable(dev["id"], dk, self._fsmng, self._logger))
                    elif dev["dt"] == "act/retractable/level2":
                        devs["act"].append (ISRetractable2(dev["id"], dk, self._fsmng, self._logger))
                    elif dev["dt"] == "act/nutrient-supply/level2":
                        devs["act"].append (ISNutrientSupply2(dev["id"], dk, self._fsmng, self._logger))
                    else:
                        self._logger.info("Unmatched devices information : " + str(dev))
        return devs

    def connect(self):
        opt = self._option['conn']
        try:
            self._conn =  serial.Serial(opt['port'], opt['baudrate'], timeout=opt['timeout'])
            noti = Notice(None, NotiCode.RTU_CONNECTED) # detection is canceled
            ret = True
        except serial.SerialException as e:
            self._logger.warn("Fail to connect : " + str(e))
            self._logger.warn(str(traceback.format_exc()))
            noti = Notice(None, NotiCode.RTU_FAILED_CONNECTION) # detection is canceled
            ret = False

        self._writenoti(noti)

        super(ISBRD22Mate, self).connect()
        self.readfromingbrd(True)
        return ret

    def close(self):
        self._conn.close()
        super(ISBRD22Mate, self).close()

    def updatesensor(self):
        for sen in self._devices["sen"]:
            sen.update(0)

    def updateactuator(self):
        now = time.time()
        lapse = now - self._lasttime
        self._lasttime = now

        for act in self._devices["act"]:
            act.update(lapse)

    # read message from ingsys board
    def prepare(self):
        self.readfromingbrd(True)

    def readfromingbrd(self, force=False):
        try:
            while self._conn.readable():
                line = self._conn.readline()
                print ("read : ", line)
                if len(line) > 0:
                    self._fsmng.setmessage(line)
                    self.updatesensor()
                    break
                if not force:
                    break

        except Exception as e:
            self._logger.warn("Fail to read : " + str(e))
            self._logger.warn(str(traceback.format_exc()))
            self.close()

    # write command to ingsys board
    def doextra(self):
        self.writetoingbrd()

    def writetoingbrd(self):
        self.updateactuator()
        try:
            #if self._conn.writable():
            self._conn.write(self._fsmng.getcommand())
            self._conn.flush()
        except serial.SerialException as e:
            self._logger.warn("Fail to write : " + str(e))
            self._logger.warn(str(traceback.format_exc()))
            self.close()

    def isurgent(self):
        for sen in self._devices["sen"]:
            if sen.getvalue() != 0:
                return True
        return False

    def processobservations(self):
        urgent = self.isurgent()
        if urgent or self._timetocheck(Mate.OBSTYPE):
            nid = self._devices["nd"].getid()
            obsblk = Observation(nid)
            if urgent:
                obsblk.seturgent()
            obsblk.setobservation(nid, 0, self._devices["nd"].getstatus())

            for sen in self._devices["sen"]:
                obsblk.setobservation(sen.getid(), sen.getvalue(), sen.getstatus())

            self._writeobs(obsblk)
            self._updatetime(Mate.OBSTYPE)

    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE):
            nid = self._devices["nd"].getid()
            blk = Notice(nid, NotiCode.ACTUATOR_STATUS)
            blk.setcontent(nid, self._devices["nd"].getcontent())
            for act in self._devices["act"]:
                blk.setcontent(act.getid(), act.getcontent())
            self._writenoti(blk)
            self._updatetime(Mate.NOTITYPE)

        elif self._timetocheck("actnoti"):
            nid = self._devices["nd"].getid()
            blk = Notice(nid, NotiCode.ACTUATOR_STATUS)
            blk.setcontent(nid, self._devices["nd"].getcontent())
            shouldsend = False
            for act in self._devices["act"]:
                if act.getstatus() == StatCode.WORKING or act.getid() in self._devidsfornoti:
                    try:
                        self._devidsfornoti.remove(act.getid())
                    except:
                        pass
                    blk.setcontent(act.getid(), act.getcontent())
                    shouldsend = True
            if shouldsend:
                self._writenoti(blk)
            self._updatetime("actnoti")

    def findactuator(self, devid):
        for act in self._devices["act"]:
            if str(act.getid()) == str(devid):
                return act
        if str(self._devices["nd"].getid()) == str(devid):
            return self._devices["nd"]
        return None
            
    def processrequest(self, req):
        """ 하나의 요청을 처리한다. """
        print("received request", req.getdevid(), self._coupleid)
        if BlkType.isrequest(req.gettype()) is False:
            self._logger.warn("The message is not request. " + str(req.gettype()))
            return False

        if str(req.getnodeid()) != str(self._devices["nd"].getid()):
            self._logger.warn("Node id is not matched. " + str(req.getnodeid()))
            return False
         
        act = self.findactuator(req.getdevid())
        if act is None:
            code = ResCode.FAIL_NO_DEVICE
        elif isinstance(act, ISNode):
            code = act.processrequest(req)
        elif self._fsmng.getmode() == MANUAL:
            if req.isforce():
                self._fsmng.setmode(AUTO)
                code = act.processrequest(req)
            else:
                code = ResCode.NOT_CONTROLLABLE
        else:
            code = act.processrequest(req)

        response = Response(req)
        response.setresult(code)
        self._logger.info("write response: " + str(response))
        self._writeres(response)
        self._devidsfornoti.append(str(req.getdevid()))
        return True 

if __name__ == "__main__":
    from multiprocessing import Queue

    isnutri = False
    quelist = [Queue(), Queue(), Queue(), Queue()]
    opt = {
        'conn' : {
            'port': '/dev/ttyACM0',
            'baudrate' : 9600,
            'timeout': 0.5
        },
        'boards' : { 
            'T' : 4,
            'S' : 2
        }
    }
    
    devinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "11", "dk" : "", "dt": "nd", "children" : [
                {"id" : "101", "dk" : "[0, 0]", "dt": "act/retractable/level1"},
                {"id" : "102", "dk" : "[0, 1]", "dt": "act/retractable/level1"},
                {"id" : "103", "dk" : "[0, 2]", "dt": "act/retractable/level1"},
                {"id" : "104", "dk" : "[0, 3]", "dt": "act/retractable/level1"},
                {"id" : "105", "dk" : "[0, 4]", "dt": "act/retractable/level1"},
                {"id" : "106", "dk" : "[0, 5, [100, 150]]", "dt": "act/retractable/level2"},
                {"id" : "107", "dk" : "[4, 0]", "dt": "act/switch/level1"},
                {"id" : "108", "dk" : "[4, 1]", "dt": "act/switch/level1"},
                {"id" : "109", "dk" : "[4, 2]", "dt": "act/switch/level1"},
                {"id" : "110", "dk" : "[4, [3, 4, 5]]", "dt": "act/nutrient-supply/level3"},
                {"id" : "111", "dk" : "1", "dt": "sen"},
                {"id" : "112", "dk" : "2", "dt": "sen"}
            ]
        }]
    }]

    dsmate = ISBRD22Mate(opt, devinfo, "1", quelist)
    ssmate = SSMate ({}, [], "1", quelist)

    ssmate.start ()
    dsmate.start ()
    print("mate started")

    time.sleep(5)
    req = Request(11)
    req.setcommand(11, CmdCode.CHANGE_CONTROL, {"control":2})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(11)
    req.setcommand(107, CmdCode.TIMED_ON, {"hold-time":20})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(11)
    req.setcommand(108, CmdCode.ON, {})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(11)
    req.setcommand(101, CmdCode.TIMED_OPEN, {"time":20})
    ssmate._writereq(req)
    req = Request(11)
    req.setcommand(102, CmdCode.TIMED_OPEN, {"time":20})
    ssmate._writereq(req)
    req = Request(11)
    req.setcommand(103, CmdCode.TIMED_OPEN, {"time":20})
    ssmate._writereq(req)

    """
    time.sleep(5)
    req = Request(11)
    req.setcommand(11, CmdCode.CHANGE_CONTROL, {"control":2})
    ssmate._writereq(req)

    """
    time.sleep(5)
    req = Request(11)
    req.setcommand(108, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    time.sleep(5)
    req = Request(11)
    req.setcommand(107, CmdCode.OFF, {})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(11)
    req.setcommand(110, CmdCode.SAREA_WATERING, {"on-sec":5, "area-bitmap": 5})
    ssmate._writereq(req)

    """
    time.sleep(8)
    req = Request(11)
    req.setcommand(110, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    """
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()

    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()


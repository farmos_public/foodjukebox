#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#
# SOILNWATER driver for KIST
#

import time, struct
import traceback
from mate import Mate
from mate_fixedlen import FixedLengthSerialMate

'''
option : {
    "conn" : {
        "port" : "/dev/ttyUSB0",
        "baudrate": 9600,
    },
    "msgfmt" : {
        "STX" : [36, 36],
        "ETX" : None,
        "size" : 18,
        "ididx" : 3,
        "info" : [
            {"len" : 5, "scale" : 1},
            {"len" : 5, "scale" : 1}
        ]
    }
}

devinfo : [
    {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "sen"}
    ]}
]
'''
class SOILNWATERMate(FixedLengthSerialMate):
    def __init__(self, option, devinfo, coupleid, logger):
        option["msgfmt"]["STX"] = "".join(map(chr, option["msgfmt"]["STX"]))
        print("stx", option["msgfmt"]["STX"])
        super(SOILNWATERMate, self).__init__(option, devinfo, coupleid, logger)

    def parsemsg(self, buf):
        st = self._msgfmt["ididx"]
        ed = 0
        tm = time.time()
        obs = []
        print("message ", ord(buf[0]), ord(buf[1]))

        for info in self._msgfmt["info"]:
            ed = st + info["len"]

            print("each sensor", st, ord(buf[st]), ord(buf[st+1]))
            temp = ord(buf[st]) * 256 + ord(buf[st+1])
            if info["scale"] is None:
                obs.append(temp)
            else:
                obs.append(temp*info["scale"])
            st = ed

        print("obs", obs)
        return obs


if __name__ == "__main__":
    option = {
        "conn" : {
            "port" : "/dev/ttyUSB0",
            "baudrate": 9600
        },
        "msgfmt" : {
            "STX" : [36, 36],
            "ETX" : None,
            "size" : 151,
            "ididx" : 2,
            "info" : [
                {"len" : 2, "scale" : 1},
                {"len" : 2, "scale" : 1},
                {"len" : 2, "scale" : 1},
                {"len" : 2, "scale" : 1}
            ]
        },
        "calibration": {
            "3": { "type": "linear", "args": { "a": 0.1, "b":0 } },
            "4": { "type": "linear", "args": { "a": 0.1, "b":0 } },
            "5": { "type": "linear", "args": { "a": 0.1, "b":0 } },
            "6": { "type": "linear", "args": { "a": 0.1, "b":0 } }
        }
    }

    devinfo = [
        {"id" : "1", "dk" : "1", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "3", "dk" : "0", "dt": "sen"},
                {"id" : "4", "dk" : "1", "dt": "sen"},
                {"id" : "5", "dk" : "2", "dt": "sen"},
                {"id" : "6", "dk" : "3", "dt": "sen"}
            ]}
        ]}
    ]

    mate = SOILNWATERMate(option, devinfo, "1", None)
    mate2 = Mate({}, [], "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(30)
    #mate.readmsg()
    #mate.sendobs()
    mate.stop()
    print("mate stopped")


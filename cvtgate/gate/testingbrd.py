#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 FarmOS, Inc.
# All right reserved.
#

import sys
import time
import importlib
import json
import traceback
import requests

import queue 
from multiprocessing import Process, Queue

from lib import *
import lib
import lib.dsmate.mate_isbrd22

class TestManager(Runner):
    def __init__(self):
        opt = {
            'conn' : {
                'port': '/dev/ttyACM1',
                'baudrate' : 9600,
                'timeout': 0.5
            },
            'boards' : { 
                'T' : 4,
                'S' : 2
            }
        }
    
        devinfo = [{
            "id" : "1", "dk" : "", "dt": "gw", "children" : [{
                "id" : "11", "dk" : "", "dt": "nd", "children" : [
                    {"id" : "51", "dk" : "1", "dt": "sen"},
                    {"id" : "52", "dk" : "2", "dt": "sen"}
                ]
            }]
        }]

        self._t = opt['boards']['T']
        self._s = opt['boards']['S']

        for i in range(self._t):
            for j in range(6):
                temp = {"id" : str(self._makeid(i, 0, j)), "dk" : str([i, j]), "dt": "act/retractable/level1"}
                devinfo[0]["children"][0]["children"].append (temp)

        for k in range(self._s):
            for j in range(6):
                temp = {"id" : str(self._makeid(self._t, k, j)), "dk" : str([self._t + k, j]), "dt": "act/switch/level1"}
                devinfo[0]["children"][0]["children"].append (temp)

        print (devinfo)
        self._quelist = [Queue(), Queue(), Queue(), Queue()]
        self._ssmate = lib.mate.SSMate ({}, [], "1", self._quelist)
        self._dsmate = lib.dsmate.mate_isbrd22.ISBRD22Mate(opt, devinfo, "1", self._quelist)
        self._isrunning = False
        self._logfuncs = None

    def _makeid(self, t, s, idx):
        return 100 + t * 10 + s * 10 + idx + 1

    def setlogger(self, logger):
        super(TestManager, self).setlogger(logger)
        self._logfuncs = {
            "debug" : logger.debug,
            "info" : logger.info,
            "warn" : logger.warning,
            "error" : logger.error,
            "crit" : logger.critical
        }

    def getdname(self):
        return "testmng"

    def initialize(self):
        try:
            self._ssmate.start()
        except Exception as ex:
            self._logger.warning("fail to start ssmate : " + str(ex))
            return None

        try:
            self._dsmate.start()
        except Exception as ex:
            self._ssmate.stop()
            self._logger.warning("fail to start dsmate : " + str(ex))
            return None

    def finalize(self):
        for q in self._quelist[1:]:
            q.close()
            q.join_thread()
        self._ssmate.stop()
        self._dsmate.stop()
        self._quelist[0].close()

    def stop(self):
        self._logger.info("Test Manager tries to stop")
        self._isrunning = False

    def processlog(self):
        logq = self._quelist[0]
        try:
            while True:
                msg = logq.get(False)
                if msg[0] == "L":     # L means log
                    self._logfuncs[msg[1]](msg[2])
        except queue.Empty:
            pass
        except Exception as ex:
            self._logger.warning("There is an exception : " + str(ex))
            self._logger.warning(traceback.format_exc())

    def timedon(self, did, idx):
        req = Request(11)
        req.setcommand(did, CmdCode.TIMED_ON, {"hold-time":3})
        self._ssmate._writereq(req)

    def timedoc(self, did, idx):
        req = Request(11)
        req.setcommand(did, CmdCode.TIMED_OPEN if idx > 6 else CmdCode.TIMED_CLOSE, {"time":3})
        self._ssmate._writereq(req)

    def run(self, debug=False):
        self._isrunning = True
        i = 0 # seconds
        j = 0 # index
        while self._isrunning:
            self.processlog()
            if i < 5:
                i = i + 1
            else:
                # make command

                for k in range(self._t): 
                    self.timedoc(self._makeid(k, 0, j % 6), j)

                for k in range(self._s): 
                    self.timedon(self._makeid(self._t, k, j % 6), j)

                i = 0
                if j < 12:
                    j = j + 1
                else: 
                    j = 0

            if debug:
                break

            time.sleep(1)
        self._logger.info("Test Manager is stopping.")

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python testmng.py [start|stop|restart|run|single]")
        sys.exit(2)

    mode = sys.argv[1]

    runner = TestManager()

    dname = runner.getdname()
    adaemon = Daemon(dname, runner, None)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.dstop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    elif 'single' == mode:
        adaemon.run(True)
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)

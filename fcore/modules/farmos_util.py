#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
#from past.utils import old_div
import time
from datetime import datetime
from collections import deque

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

def ispropertime(inputs):
    nsec = inputs["#nsec"].getvalue()
    times = inputs["#table.time"].getvalue()
    for idx in range(len(times)):
        if times[idx] is None:
            break
        if times[idx] <= nsec <= times[idx] + 60:
            return idx
    return None

def isproperweekday(inputs, idx):
    wday = time.localtime().tm_wday
    if "#table.weekday" not in inputs:
        return True
    available = inputs["#table.weekday"].getvalue()[idx]
    if available and available & (1 << wday):
        return True
    return False

def scheduleon(inputs, pvalues, dbcur):
    """
    inputs (mandatory) : table.used, table.time, table.worktime (분)
    outputs : cmd, worktime
    """

    idx = ispropertime(inputs)
    if idx is not None and inputs["#table.used"].getvalue()[idx]:
        return (RetCode.OK, [CmdCode.TIMED_ON, inputs["#table.worktime"].getvalue()[idx] * 60])
    else:
        return (RetCode.NOT_PROPER_TIME, None)

def schedulewin(inputs, pvalues, dbcur):
    """
    inputs (mandatory) : table.used, table.time, table.cmd, table.worktime (초)
    outputs : cmd, worktime
    """
    idx = ispropertime(inputs)
    if idx is not None and inputs["#table.used"].getvalue()[idx]:
        return (RetCode.OK, [inputs["#table.cmd"].getvalue()[idx], inputs["#table.worktime"].getvalue()[idx]])
    else:
        return (RetCode.NOT_PROPER_TIME, None)

def scheduleareasupply(inputs, pvalues, dbcur):
    """
    inputs (mandatory) : table.used, table.weekday, table.time, table.cmd, table.worktime (분)
    outputs : cmd, worktime
    """
    if isproperweekday(inputs) is False:
        return (RetCode.NOT_PROPER_TIME, None)

    idx = ispropertime(inputs)
    if idx is not None and inputs["#table.used"].getvalue()[idx]:
        worktime = inputs["#table.worktime"].getvalue()[idx]
        areabit = inputs["#table.areabit"].getvalue()[idx]
        times = inputs["#table.time"].getvalue()
        if idx == len(times) - 1: # idx is the last index
            nextime = None
        else:
            nexttime = times[idx + 1]
        return (RetCode.OK, [CmdCode.SAREA_WATERING, worktime, areabit, nextime])
    else:
        return (RetCode.NOT_PROPER_TIME, None)


if __name__ == '__main__':
    wday = time.localtime().tm_wday
    print ("today : ", wday)

    inputs= {"#nsec": Variable(150), "#table.time" : Variable([10, 70, 120, 180]), 
        "#table.weekday" : Variable([1<<wday])}
    print("result 2, true ", ispropertime(inputs), isproperweekday(inputs))

    inputs= {"#nsec": Variable(0), "#table.time" : Variable([10, 70, 120, 180]), 
        "#table.weekday" : Variable([1<<(wday+1)])}
    print("result None, false ", ispropertime(inputs), isproperweekday(inputs))

    inputs= {"#nsec": Variable(0), "#table.time" : Variable([10, 70, 120, 180])} 
    print("result None, false ", ispropertime(inputs), isproperweekday(inputs))


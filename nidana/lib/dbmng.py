#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 Farmos, Inc.
# All right reserved.
#
# mate for interaction with farmos 

import json
import pymysql
import pandas
from datetime import datetime

'''
option : {
    "db" : {"host" : "localhost", "user" : "root",
              "password" : "pass", "db" : "db"}
}
'''

class DBManager:
    def __init__(self, option, logger):
        self._isconnected = False
        self._conn = None
        if "db" in option:
            self._option = option["db"]
        else:
            self._option = {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"}
        self._logger = logger
        self._retries = []
        self._cache = {}

    def connect(self):
        pass

    def close(self):
        pass

    def _select(self, query, params):
        pass

    def _execute(self, query, params):
        pass

    def _commit(self):
        pass

class MySQLManager(DBManager):
    def _connect(self, opt):
        conn = pymysql.connect(host=opt["host"], user=opt["user"],
                                     password=opt["password"], db=opt["db"])
        cur = conn.cursor()
        return (conn, cur)

    def connect(self):
        self._conn, self._cur = self._connect(self._option)
        self._isconnected = True
        self._logger.info("DB Connection initialized.")
        temp = self._retries
        self._retries = []
        for q, p in temp:
            self._execute(q, p)

    def close(self):
        self._cur.close()
        self._conn.close()
        self._isconnected = False
        self._logger.info("DB Connection closed.")

    def getcolumns(self):
        return [i[0] for i in self._cur.description]

    def getdesc(self):
        return self._cur.description

    def _select(self, query, params, skiperr=False):
        try:
            if self._isconnected is False:
                self.connect()
            self._conn.ping(True)
            self._cur.execute(query, params)
            rows = self._cur.fetchall()
            #print("_select", query, params, rows)
            return rows
        except pymysql.IntegrityError as ex:
            #self._logger.info("DB Integrity exception : " + str([ex, query, params]))
            pass
        except Exception as ex:
            print("DB other exception : " + str([ex, query, params]))
            self._logger.warn("DB other exception : " + str([ex, query, params]))
            self._logger.warn(self._cur._last_executed)
            if skiperr:
                return None
            self.close()
        return None

    def _execute(self, query, params, skiperr=False):
        try:
            if self._isconnected is False:
                self.connect()
            self._conn.ping(True)
            self._cur.execute(query, params)
            self._conn.commit()
            #print(self._cur._last_executed)
        except pymysql.IntegrityError as ex:
            #self._logger.info("DB Integrity exception : " + str([ex, query, params]))
            pass
        except Exception as ex:
            self._logger.warn("DB other exception : " + str([ex, query, params]))
            self._retries.append([query, params])
            self._logger.warn(self._cur._last_executed)
            if skiperr:
                return None
            self.close()

    def _commit(self):
        try:
            if self._isconnected is False:
                self.connect()
            self._conn.commit()
        except Exception as ex:
            self._logger.info("commit exception : " + str(ex))


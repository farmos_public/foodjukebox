#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime


import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from code import RetCode, VarCode
from variable import Variable
from modules.farmos_util import ispropertime, isproperweekday

def scheduleswitchsupply(inputs, pvalues, dbcur):
    """
    inputs (mandatory) : table.used, table.weekday, table.time, table.cmd, table.worktime (분)
    outputs : cmd, worktime
    """
    idx = ispropertime(inputs)
    if idx is None:
        return (RetCode.NOT_PROPER_TIME, None)

    if isproperweekday(inputs, idx) is False:
        return (RetCode.NOT_PROPER_TIME, None)

    if inputs["#table.used"].getvalue()[idx]:
        worktime = inputs["#table.worktime"].getvalue()[idx] * 60
        vnum = inputs["#table.vnum"].getvalue()[idx]

        return (RetCode.OK, [CmdCode.TIMED_ON, worktime, vnum], None)
    return (RetCode.NOT_USED, None)

def fancontrol(inputs, pvalues, dbcur):
    """
    inputs : stdtemp, stdhum, fused, fcmd, stime, etime, stoptemp
    outputs : cmd, worktime
    """
    temp = inputs["#temp1"].getvalue()
    hum = inputs["#hum1"].getvalue()
    nsec = inputs["#nsec"].getvalue()
    worktime = inputs["period"].getvalue() + 10

    stdtemp = inputs["#stdtemp"].getvalue()
    stdhum = inputs["#stdhum"].getvalue()
    fused = inputs["#fused"].getvalue()
    fcmd = inputs["#fcmd"].getvalue()
    stime = inputs["#stime"].getvalue()
    etime = inputs["#etime"].getvalue()
    stoptemp = inputs["#stoptemp"].getvalue()

    if stoptemp > temp:
        return (RetCode.OK, [CmdCode.OFF, 0])

    if fused and stime <= nsec <= etime:
        return (RetCode.OK, [fcmd, worktime])

    if temp >= stdtemp or hum >= stdhum:
        return (RetCode.OK, [CmdCode.TIMED_ON, worktime])

    return (RetCode.OK, [CmdCode.OFF, 0])

def windowcontrol(inputs, pvalues, dbcur):
    """
    inputs : opentemp, closetemp, opentime, closetime, waittime
    outputs : cmd, worktime
    """
    temp = inputs["#temp1"].getvalue()
    nsec = inputs["#nsec"].getvalue()
    tsidx = inputs["tsidx"].getvalue()

    opentemp = inputs["#opentemp"].getvalue(tsidx)
    closetemp = inputs["#closetemp"].getvalue(tsidx)
    opentime = inputs["#opentime"].getvalue(tsidx)
    closetime = inputs["#closetime"].getvalue(tsidx)
    waittime = inputs["#waittime"].getvalue(tsidx)

    if opentemp <= temp and closetemp >= temp:
        return (RetCode.OK, [CmdCode.OFF, 0, CmdCode.OFF, 0], waittime)
    elif opentemp <= temp:
        return (RetCode.OK, [CmdCode.TIMED_OPEN, opentime, CmdCode.TIMED_OPEN, opentime], waittime)
    elif closetemp >= temp:
        return (RetCode.OK, [CmdCode.TIMED_CLOSE, closetime, CmdCode.TIMED_CLOSE, closetime], waittime)
    return (RetCode.OK, [CmdCode.OFF, 0, CmdCode.OFF, 0], waittime)

def windowcontrol2(inputs, pvalues, dbcur):
    """
    inputs : rain1, hightemp1, temp1, opentemp, closetemp, opentime, closetime, waittime
    outputs : cmd, worktime
    """
    temp = inputs["#temp1"].getvalue()
    nsec = inputs["#nsec"].getvalue()
    tsidx = inputs["tsidx"].getvalue()

    opentemp = inputs["#opentemp"].getvalue(tsidx)
    closetemp = inputs["#closetemp"].getvalue(tsidx)
    opentime = inputs["#opentime"].getvalue(tsidx)
    closetime = inputs["#closetime"].getvalue(tsidx)
    waittime = inputs["#waittime"].getvalue(tsidx)

    if opentemp <= temp and closetemp >= temp:
        return (RetCode.OK, [CmdCode.OFF, 0], waittime)

    elif opentemp <= temp:
        return (RetCode.OK, [CmdCode.TIMED_OPEN, opentime], waittime)

    elif closetemp >= temp:
        return (RetCode.OK, [CmdCode.TIMED_CLOSE, closetime], waittime)

    return (RetCode.OK, [CmdCode.OFF, 0], waittime)
        
def curtaincontrol(inputs, pvalues, dbcur):
    """
    inputs : opentemp, closetemp, opentime, closetime, waittime, fused, fcmd
    outputs : cmd, worktime
    """
    temp = inputs["#temp1"].getvalue()
    nsec = inputs["#nsec"].getvalue()

    opentemp = inputs["#opentemp"].getvalue()
    closetemp = inputs["#closetemp"].getvalue()
    opentime = inputs["#opentime"].getvalue()
    closetime = inputs["#closetime"].getvalue()
    waittime = inputs["#waittime"].getvalue()

    fused = inputs["#fused"].getvalue()
    fcmd = inputs["#fcmd"].getvalue()
    stime = inputs["#stime"].getvalue()
    etime = inputs["#etime"].getvalue()

    if fused and stime <= nsec <= etime:
        if fcmd == CmdCode.TIMED_OPEN:
            return (RetCode.OK, [fcmd, opentime], waittime)
        elif fcmd == CmdCode.TIMED_CLOSE:
            return (RetCode.OK, [fcmd, closetime], waittime)

    if opentemp <= temp and closetemp >= temp:
        return (RetCode.OK, [CmdCode.OFF, 0], waittime)
    elif opentemp <= temp:
        return (RetCode.OK, [CmdCode.TIMED_OPEN, opentime], waittime)
    elif closetemp >= temp:
        return (RetCode.OK, [CmdCode.TIMED_CLOSE, closetime], waittime)
    return (RetCode.OK, [CmdCode.OFF, 0], waittime)
        
def screencontrol(inputs, pvalues, dbcur):
    """
    inputs : opentemp, closetemp, opentime, closetime, waittime, fused, fcmd
    outputs : cmd, worktime
    """
    temp = inputs["#temp1"].getvalue()
    nsec = inputs["#nsec"].getvalue()

    opentemp = inputs["#opentemp"].getvalue()
    closetemp = inputs["#closetemp"].getvalue()
    opentime = inputs["#opentime"].getvalue()
    closetime = inputs["#closetime"].getvalue()
    waittime = inputs["#waittime"].getvalue()

    fused = inputs["#fused"].getvalue()
    fcmd = inputs["#fcmd"].getvalue()
    stime = inputs["#stime"].getvalue()
    etime = inputs["#etime"].getvalue()

    if fused and stime <= nsec <= etime:
        if fcmd == CmdCode.TIMED_OPEN:
            return (RetCode.OK, [fcmd, opentime], waittime)
        elif fcmd == CmdCode.TIMED_CLOSE:
            return (RetCode.OK, [fcmd, closetime], waittime)

    if closetemp <= temp and opentemp >= temp:
        return (RetCode.OK, [CmdCode.OFF, 0], waittime)
    elif opentemp >= temp:
        return (RetCode.OK, [CmdCode.TIMED_OPEN, opentime], waittime)
    elif closetemp <= temp:
        return (RetCode.OK, [CmdCode.TIMED_CLOSE, closetime], waittime)
    return (RetCode.OK, [CmdCode.OFF, 0], waittime)

def soilmoistsupply(inputs, pvalues, dbcur):
    """
    inputs : areabit, stdmoi, worktime
    outputs : cmd, worktime, areabit, nexttime
    """
    moist = inputs["#moist1"].getvalue()

    stdmoi = inputs["#stdmoi"].getvalue()

    if stdmoi >= moist:
        areabit = inputs["#areabit"].getvalue()
        worktime = inputs["#worktime"].getvalue()
        return (RetCode.OK, [CmdCode.SAREA_WATERING, worktime, areabit, None])
    else:
        return (RetCode.OK, None)

def _resetload(value):
    if value > 100:
       return 100
    elif value < 0:
       return 0
    return value

def calculateload(terr, Kp, Kd, Ki):
    return Kp * terr.getlastestvalue() + Kd * terr.getdifferential(n=5) + Ki * terr.getintegration(n=5)

def getloads(tthd, tdiff, inputs):
    """
    inputs : tthd, tdif, and inputs 
    outputs : ventload, heatload
    """

    tsidx = inputs["tsidx"].getvalue()

    Kp = inputs["#KpH"].getvalue(tsidx)
    Kd = inputs["#KdH"].getvalue(tsidx) 
    Ki = inputs["#KiH"].getvalue(tsidx)
    hl = calculateload(tthd, Kp, Kd, Ki)

    Kp = inputs["#KpV"].getvalue(tsidx)
    Kd = inputs["#KdV"].getvalue(tsidx) 
    Ki = inputs["#KiV"].getvalue(tsidx)
    vl = calculateload(tthd, Kp, Kd, Ki)

    #hl = inputs["#KpH"].getvalue(tsidx) * htemp + inputs["#KdH"].getvalue(tsidx) * thd.getdifferential(dt) + inputs["#KiH"].getvalue(tsidx) * thd.getintegration(dt)
    #hl = hl + inputs["#KpO"].getvalue(tsidx) * inputs["#inoutdiff"].getvalue() + inputs["#KdO"].getvalue(tsidx) * inputs["#inoutdiff"].getdifferential(dt) + inputs["#KiO"].getvalue(tsidx) * inputs["#inoutdiff"].getintegration(dt)

    print("calculated loads(vl, hl)", vl, hl)

    return [_resetindex(vl), _resetindex(hl)]

def _resetindex(value):
    if value > 100:
       return 100
    elif value < -100:
       return -100
    return value

def fieldindex(inputs, pvalues, dbcur):
    """
    inputs : #temp1, #hum1, #period, #htemp, #ltemp, #hhum, #lhum, #tsidx, KpH, KdH, KpO, KpV, KdV, KiV, inoutdiff
    outputs : envindex, tempindex, humindex, hload, vload, tstd, tdiff, hstd, hdiff
    """

    htemp = inputs["#htemp"].getvalue()
    ltemp = inputs["#ltemp"].getvalue()
    tstd = (htemp + ltemp) / 2
    tdiff = (htemp - ltemp) / 2
    tidx = inputs["#temp1"].getvalue() - tstd

    tempindex = _resetindex(tidx * inputs["#tstd"].getvalue())

    hhum = inputs["#hhum"].getvalue()
    lhum = inputs["#lhum"].getvalue()
    hstd = (hhum + lhum) / 2
    hdiff = (hhum - lhum) / 2
    hidx = inputs["#hum1"].getvalue() - hstd

    humindex = _resetindex(hidx * inputs["#hstd"].getvalue())

    if pvalues is None or len(pvalues) == 0:
        tthd = Variable(tidx, size=10, vcode=VarCode.HIST)
        hthd = Variable(hidx, size=10, vcode=VarCode.HIST)
    else:
        tthd = pvalues[9]
        tthd.setvalue(tidx)
        hthd = pvalues[10]
        hthd.setvalue(hidx)

    vload, hload = getloads(tthd, tdiff, inputs)
    print("indexlized calculated loads(vl, hl)", vload, hload)

    #envindex = vload - hload

    return (RetCode.OK, [tempindex, tempindex, humindex, hload, vload, tstd, tdiff, hstd, hdiff, tthd, hthd])

def getstage(inputs, pvalues, dbcur):
    """
    inputs : table.time, plantdate
    outputs : days, stage
    """
    if ispropertime(inputs) is None:
        return (RetCode.NOT_PROPER_TIME, None)
    else:
        now = time.time()
        pdate = inputs["#plantdate"].getvalue()
        ndays = int((now - pdate) / 86400)

        if ndays < 40: # 생육촉진기
            stage = 31
        elif ndays < 70: # 생육기
            stage = 32
        else: # 수확기
            stage = 33
        return (RetCode.OK, [ndays, stage])

if __name__ == '__main__':
    pass

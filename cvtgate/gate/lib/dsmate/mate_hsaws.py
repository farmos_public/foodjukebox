from collections import defaultdict
import requests
import json
import struct
import time
import traceback
from datetime import datetime
from decimal import *
import datetime
from .. import *

'''
    option = {
        "key" : {
            "obsrBhfInnb":"001001"
        }
    }
'''
class HSWeatherStationMate(DSMate):

    def loaddata(self):
        copt = self._option["key"]
        url = "http://182.162.27.101/getWetherNowApi.json?obsrBhfInnb={}".format(copt["obsrBhfInnb"])
        response = requests.get(url=url)

        if response.status_code == 200:
            data = json.loads(response.text)
            return data
        else:
            self._logger.error("error loaddata" + response.status_code)
            return None

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return
        self._updatetime(Mate.OBSTYPE)
        copt = self._option["key"]
        data = self.loaddata()["result"]
        
        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])

        for dev in nd["children"]:
            if dev["dk"] in data:
                if data[dev["dk"]] is None or data[dev["dk"]] == '':
                    obs = None
                else:
                    obs = float(data[dev["dk"]])
                obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)

        self._writeobs(obsblk)


if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "key" : {
            "obsrBhfInnb":"001001"
        }
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id": "4", "dk": "tp", "dt": "sen"},
                {"id": "5", "dk": "hd", "dt": "sen"},
                {"id": "6", "dk": "ws", "dt": "sen"},
                {"id": "7", "dk": "wd", "dt": "sen"},
                {"id": "8", "dk": "rainAccmlt", "dt": "sen"},
                {"id": "9", "dk": "solradAccmlt", "dt": "sen"}

            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = HSWeatherStationMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

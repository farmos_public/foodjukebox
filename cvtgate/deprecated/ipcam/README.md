# IP Camera Interaction

## How to setup

* crontab 설정
crontab -e 라고 입력하고 아래의 내용을 추가한다.

 ```
@reboot autossh -N -f -R 1241:localhost:22 root@dev.jinong.co.kr
* * * * * cd /home/debian/cvtgate/ipcam; python runipcam.py 300
*/5 * * * * cd /home/debian/cvtgate/ipcam; python checkoption.py 300
0 11 * * * cd /home/debian/cvtgate/ipcam; ./clear.sh
 ```

* configuration - ipcam.conf
 ```
{"cameras": [{
  "ip": "192.168.0.10", 
  "id": "100", 
  "idpw": "admin:jinong30@",
  "saveTimes": ["21:37", "14:53"], 
  "option": "-q:v 9 -s 1920x1280 -vframes 1"
}]}
 ```

* timezone setting to Seoul
 ```
sudo dpkg-reconfigure tzdata
 ```

#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`


cd /home/fjbox/backup/image

deviceList=`ls`


for deviceSerial in $deviceList
do
    echo -e $deviceSerial
    cd /home/fjbox/backup/image/$deviceSerial/18

    nvalue=$`mysql -u farmos -pfarmosv2@ farmos -e "select nvalue from current_observations where data_id = '100004' and farm_id = $deviceSerial"`

    echo -e $nvalue

    plantingDay=${nvalue:7}
    today=`date "+%s"`

    echo -e $plantingDay, '\n\n\n'

    diff=`expr $today - $plantingDay`
    diffDay=`expr $diff / 84600`

    if [[ "$plantingDay" =~ "NULL" ]]; then
        convert -resize 25% -delay 30 -loop 0 `find -mtime -20 -type f | shuf -n 20 | sort` ../18_animation.gif
    else
        convert -resize 25% -delay 30 -loop 0 `find -mtime -$diffDay -type f | shuf -n 20 | sort` ../18_animation.gif
    fi

done




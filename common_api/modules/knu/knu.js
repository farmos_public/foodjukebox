/**
 * @fileoverview FARMOS_BETA Javascript API
 * @author joonyong.jinong@gmail.com
 * @version 1.0.0
 * @since 2017.07.04
 */

var knuApi = function () {
  // var _pool = pool // 상기 내용으로 인하여 golbal pool 사용 .. 추후 다른 방법 생각 (윈도우전용)
  var _pool = require('database')() // 소스 push 할때 사용(리눅스전용 commit할때는 이걸로 올려야함)
  var _query = ''

  /**
     * @method getKnuObservations
     * @description 경북대 수질관리(습도)정보를 조회한다
     */
  var getKnuObservations = async function (date) {
    let _query = 'select data_id, date_format(obs_time, "%Y-%m-%d %H:%i") as obs_time, round(nvalue,2) as nvalue '
    _query += 'from '
    _query += 'observations '
    _query += 'where data_id =  ?  '
    _query += 'and obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59") '
    _query += 'order by obs_time asc '

    const [rows1] = await _pool.query(_query, [10000301, date, date])
    const [rows2] = await _pool.query(_query, [10000601, date, date])
    const [rows3] = await _pool.query(_query, [10000701, date, date])
    const [rows4] = await _pool.query(_query, [10000801, date, date])
    const [rows5] = await _pool.query(_query, [10000901, date, date])

    const data = [
      rows1, rows2, rows3, rows4, rows5
    ]

    return data
  }

  /**
     * @method getKnuCurrentObservations1
     * @description 경북대 수질관리(습도) 최근 정보를 조회한다
     */
  var getKnuCurrentObservations1 = async function () {
    _query = 'SELECT round(nvalue,2) as nvalue FROM  current_observations where data_id =  "10000301" '
    const [rows] = await _pool.query(_query)
    return rows
  }

  /**
     * @method getKnuCurrentObservations2
     * @description 경북대 수질관리(pH) 최근 정보를 조회한다
     */
  var getKnuCurrentObservations2 = async function () {
    _query = 'SELECT round(avg(nvalue),2) as nvalue FROM current_observations where data_id IN ( 10000601,10000701,10000801,10000901 ) '
    const [rows] = await _pool.query(_query)
    return rows
  }

  /**
     * @method getKnuDownload
     * @description 경북대 수질관리 정보를 다운로드한다
     */
  var getKnuDownload = async function (date) {
    console.log(date)

    const _query = 'SELECT obs_time as "시간" ,round(value_a,2) AS "pH",round(value_b,2) AS "함수율10", round(value_c,2) AS "함수율20", round(value_d,2) AS "함수율30", round(value_e,2) AS "함수율40" FROM ( ' +
        ' SELECT date_format(obs_time,"%Y-%m-%d %H:%i:%s") AS obs_time FROM observations WHERE obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59") GROUP BY date_format(obs_time,"%Y-%m-%d %H:%i:%s")) time LEFT OUTER JOIN ( ' +
        ' SELECT date_format(obs_time,"%Y-%m-%d %H:%i:%s") AS obs_a,nvalue AS value_a FROM observations WHERE data_id = "10000301" AND obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59")) aa ON time.obs_time=aa.obs_a LEFT OUTER JOIN ( ' +
        ' SELECT date_format(obs_time,"%Y-%m-%d %H:%i:%s") AS obs_b,nvalue AS value_b FROM observations WHERE data_id = "10000601" AND obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59")) bb ON time.obs_time=bb.obs_b LEFT OUTER JOIN ( ' +
        ' SELECT date_format(obs_time,"%Y-%m-%d %H:%i:%s") AS obs_c,nvalue AS value_c FROM observations WHERE data_id = "10000701" AND obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59")) cc ON time.obs_time=cc.obs_c LEFT OUTER JOIN ( ' +
        ' SELECT date_format(obs_time,"%Y-%m-%d %H:%i:%s") AS obs_d,nvalue AS value_d FROM observations WHERE data_id = "10000801" AND obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59")) dd ON time.obs_time=dd.obs_d LEFT OUTER JOIN ( ' +
        ' SELECT date_format(obs_time,"%Y-%m-%d %H:%i:%s") AS obs_e,nvalue AS value_e FROM observations WHERE data_id = "10000901" AND obs_time between date_format(?, "%Y-%m-%d 00:00") and date_format(?, "%Y-%m-%d 23:59")) ee ON time.obs_time=ee.obs_e ' +
        ' order by time.obs_time desc '

    // console.log(_pool.format(_query, [date, date, date, date, date, date, date, date, date, date, date, date]))

    const [rows] = await _pool.query(_query, [date, date, date, date, date, date, date, date, date, date, date, date])
    return rows
  }

  return {
    getKnuObservations: getKnuObservations,
    getKnuCurrentObservations1: getKnuCurrentObservations1,
    getKnuCurrentObservations2: getKnuCurrentObservations2,
    getKnuDownload: getKnuDownload
  }
}

module.exports = knuApi()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import requests

from imports import *
from mate import Mate

'''
option : {
    "conn" : {"base" : "http://stroll.jinong.co.kr/hsict/v1/fndservice/"}
}

# devinfo의 경우 특별한 매핑없이 그대로 오는 값을 사용해도 됨.
devinfo : []
'''

class ShortModelAPIMate(Mate):
    def __init__(self, option, devinfo, coupleid, logger):
        self._base = option["conn"]["base"]
        super(ShortModelAPIMate, self).__init__(option, devinfo, coupleid, logger)

    def start(self, writecb):
        super(ShortModelAPIMate, self).start(writecb)

    def stop(self):
        super(ShortModelAPIMate, self).stop()

    def wrapblk(self, nid, content):
        content.pop("result", None)
        # temporary
        #content.pop("netid ", None)
        #content.pop("fnd2 ", None)
        #content["netid"] = 2
        #content["nodeid"] = 10
        #content["fnd2"] = 40.3
        return MBlock(nid, BlkType.REQUEST, content)

    def request(self, method, url, data):
        if method == "post":
            r = requests.post(url, json=data)
        elif method == "get":
            r = requests.get(url, json=data)
        else:
            self._logger.warn("no method available !!")
            return None

        if r.status_code == 200:
            ret = r.json()
            print("result", ret)
            if "result" in ret[0]:
                self._logger.warn(url + " failed !!")
                return None
            else:
                return ret[0]
        else:
            self._logger.info(url + " status code " + str(r.status_code))

        return None

    def writeblk(self, blk):
        #print blk.getcontent()

        if BlkType.isobservation(blk.gettype()):
            pass

        elif BlkType.isrequest(blk.gettype()):
            url = self._base + "register/" + blk.getnodeid()
            self._logger.info(url + " register " + str(blk.getnodeid()))
            ret = self.request("get", url, None)
            if ret is not None:
                self._writecb(MBlock(blk.getnodeid(), BlkType.RESPONSE, ResCode.OK))

                url = self._base + "fnddata/" + blk.getnodeid()
                ret = self.request("get", url, None)
                if ret is not None:
                    self._writecb(self.wrapblk(blk.getnodeid(), ret))
                else:
                    self._logger.warn("error to get fnddata!!")
            else:
                self._logger.warn("error to register : " + str(blk.getnodeid()))
        else:
            # error log
            self._logger.warn("wrong block type : " + str(blk.gettype()))
            return False

if __name__ == "__main__":
    option = {"conn" : {"base" : "http://stroll.jinong.co.kr/hsict/v1/fndservice/"}}
    devinfo = []
    pest = ShortModelAPIMate(option, devinfo, "1", None)
    mate = Mate({}, [], "1", None)
    pest.start(mate.writeblk)
    pest.writeblk(MBlock("JS0101", BlkType.REQUEST, None))
    time.sleep(1)
    print("after sleeping 1 sec")
    time.sleep(3)
    pest.stop()

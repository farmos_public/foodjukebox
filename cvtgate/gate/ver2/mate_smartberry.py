#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import pymysql
import datetime

from imports import *

from mate import Mate
from dinfo import DevInfo
from devtype import DevType

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class SmartBerryMate(Mate):
    _INS_QUERY = "insert into TB_OBSR_INSTL_EQPMN_MESURE_I(MESURE_DT, OID, LID, MESURE_VALUE) values(%s, %s, %s, %s)"
    def __init__(self, option, devinfo, coupleid, logger):
        self._conn = None
        self._cur = None
        super(SmartBerryMate, self).__init__(option, devinfo, coupleid, logger)

    def connect(self):
        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                                     password=copt["password"], db=copt["db"])
        self._cur = self._conn.cursor()
        super(SmartBerryMate, self).connect()
        return True

    def close(self):
        self._cur.close()
        self._conn.close()
        super(SmartBerryMate, self).close()

    def start(self, writecb):
        super(SmartBerryMate, self).start(writecb)
        self.connect()

    def stop(self):
        super(SmartBerryMate, self).stop()
        self.close()

    def writeblk(self, blk):
        print("smartberry writeblk", blk.getcontent())
        if BlkType.isobservation(blk.gettype()):
            print("write obs")
            nid = "IJ" + str(47000 + int(blk.getnodeid()))
            content = blk.getcontent()
            tm = content.pop('time', None)
            ret = 0
            for dk, values in content.items():
                devid = self._devinfo.findid(dk, DevType.SENSOR)
                if devid is None:
                    continue
                params = [tm, nid, str(devid) + "_0_0_0", values[0]]
                try:
                    print((SmartBerryMate._INS_QUERY, params))
                    self._cur.execute(SmartBerryMate._INS_QUERY, params)
                except pymysql.IntegrityError as ex:
                    self._logger.info("DB exception : " + str(ex))
                except Exception as ex:
                    self._logger.warn("DB exception : " + str(ex))
            try:
                self._conn.commit()
                self._logger.info("Observation Inserted." + str(content))
            except Exception as ex:
                self._logger.warn("DB exception : " + str(ex))
        return True

if __name__ == "__main__":
    option = {
        "db" : {"host": "220.90.133.21", "user": "smartberry", "password": "smartberry#0801!", "db": "smartberry"},
    }

    devinfo = [
        {"id" : "0", "dk" : "0", "dt": "gw", "children" : [
            {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "1", "dk" : "1", "dt": "sen"},
                {"id" : "2", "dk" : "2", "dt": "sen"}
            ]}
        ]}
    ]

    sbmate = SmartBerryMate(option, devinfo, "1", None)
    mate = Mate({}, [], "1", None)
    sbmate.start(mate.writeblk)

    obs = Observation(1)
    obs.setobservation(1, [28, None])
    obs.setobservation(2, [50, None])
    obs.setobservation(8, [450, None])
    obs.setobservation(13, [0, None])

    sbmate.writeblk(obs)

    time.sleep(5)
    sbmate.stop()

    print("local tested.")
